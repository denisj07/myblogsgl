F.COMPO Q&A 脇役編


[雅彦編](./q_masa.md) 	
[紫苑編](./q_shion.md) 	
[若苗家編](./q_waka.md)  
[脇役編](./q_waki.md)  
[その他](./q_etc.md) 	


アシスタント編  
助手編  

    Q1、 	それぞれの本名は？
            你的真实姓名是什么？
    A1、 	
        マコちゃん（山崎真琴）：不明
        ヒロリン（中村浩美）：中村光浩・・・33rd.day
        和ちゃん（新潟和子）：新潟和人・・・53rd.day
        進ちゃん：横田進・・・15th.day

    　
    Q2、 	アシスタントさんのモデル  
            您的助手的型号  
    A2、 	City Hunter 35巻の最後にSpecial Thanksとして約10名の名前が挙げられています。アシスタントさんの名前や奥さんと思われる人の名前・・・そのなかに、F.COMPOのアシさんのモデルとなった名前があるようです。ヒロリン・和ちゃん・進ちゃんはおんなじ名前があります。マコちゃんはないようですね。CH後のアシさんかな？  
            在《城市猎人》第35卷末尾，有大约10个名字被列为特别感谢。 在助理和他们妻子的名字中，似乎有F.COMPO的助理的模特的名字。 Hirolin、Kazu-chan和Shin-chan的名字相同。 真琴似乎不在致谢名单里，也许她在CH之后才成为助手的？  
            At the end of City Hunter Volume 35, there are about 10 names listed as Special Thanks. Among the names of the assistants and their supposed wives, there seems to be the name of the model of F.COMPO's assistant. Hirolin, Kazu-chan, and Shin-chan have the same names. Mako-chan doesn't seem to have the same name; maybe she was an assistant after CH?  
    　
    Q3、 	仕事場のパソコン・・・  
            工作中的电脑...  
    A3、 	
        - 58th.day で登場した「起動せんし」マッキントッシュ。九州以外の人にわかるでしょうか、「起動せんし」。～せん、というのは、九州の言葉で「～しない」という意味なんです。きっと、北条先生が実際に使っているマッキントッシュが「起動せんし」なんでしょう。そうすると、5th.day の表紙で紫苑のTシャツの文字に「Welcome to Macintosh」があったのもなんとなく理解できそうです。ところで、あの機種は何という機種ですか？隊長はWindowsマシンはわかりますが、Macintoshとなると、さっぱりです。  
        出现在第58天的Macintosh。 我不知道九州以外的人是否能理解它，"不要开始"。 ～在九州语言中是 "不做 "的意思。 我敢肯定，北条老师实际使用的Macintosh是 "Startup Senshi"。 然后，我想我可以理解为什么第5.day封面上Shion的T恤上有 "欢迎来到Macintosh"。 顺便问一下，那是什么机器？ 我了解Windows机器，但我对Macintosh一无所知。
  
            - PowerMacintosh8500です。  
            実際にはCPUクロックの速度によって  
             事实上，根据CPU时钟的速度，它是  
            PowerMacintosh8500/120  
            PowerMacintosh8500/132  
            PowerMacintosh8500/150  
            PowerMacintosh8500/180  
            というのが正式名称になります。  
            成为一个正式的名字。  
            ちなみに不思議な形のモニタはAppleVision1710AVだったと思います。（提供T.S.）  
            顺便说一下，我认为那个神秘形状的显示器是AppleVision1710AV。 (提供T.S.)    

        - 加えて、一緒に置いてあったTA。ISDNの文字、メーカーNECO、電子メール着信通知にあのデザイン・・・とくれば、モデルはNEC製Aterm！98年末の掲載だったことを考えると、Aterm IT65 EX の可能性が大です。電子メール着信通知はBIGLOBEでしか使えないので、プロバイダはBIGLOBEですね。もしかしたら、F.COMPO世界では「SMALLOBE」くらいになっているかもしれませんが。  
        此外，TA被放在一起，ISDN的特点，制造商NECO，电子邮件环通知，和那个设计......如果它来了，该模型是NEC制造的Aterm! 考虑到Aterm IT65 EX是在1998年底出版的，它很有可能是Aterm IT65 EX。 电子邮件通知只在BIGLOBE中可用，所以提供者是BIGLOBE。 也许是关于F.COMPO世界中的"SMALLOBE"，不过。


脇役編
配角編  

    Q1、 	葉子の誕生日は？  
            叶子的生日是什么时候？   
    A1、 	9月12日と思われる（55th.day, 56th.day）。55th.dayは「誕生日一週間きったでしょ。」や「日曜（あした）家に？」から土日。残り2～3日で「1週間きった」とは普通言わないので、誕生日はおそらく翌週の木・金・土曜日。56th.dayでは、「誕生日が明日に迫った」のと、その日の夜に翌日のデートの予定を立てていることから翌日は土曜日（日曜日も考えられるが「一週間きった」と矛盾してしまう）。私立大学は第2・第4土曜しか休みになっていないところもあると聞くし、授業外活動や集中講義などと考えてもつじつまは合う。55th.dayを9月第1週の98年9月5日土曜日と翌日、56th.dayを9月11日金曜日と翌日と考えれば、葉子の誕生日は9月12日と想像できる。  
            它被认为是9月12日（第55话，第56话）。第55话的意思是 "距离你的生日还不到一个星期"和"你星期天回家吗？"到周六和周日。 在第56话，她的生日明天就要到了，她已经计划好第二天晚上的约会，所以第二天是星期六（星期天也可以，但会与 "已经过了一个星期 "相矛盾）。 . 我听说有些私立大学只在每个月的第二和第四个星期六放假，把它看成是课外活动或集中授课等是有道理的。如果我们把第55话看成是1998年9月5日星期六，即9月的第一个星期，以及第二天，第56话是1998年9月11日星期五，以及第二天，我们可以想象，叶子的生日是9月12日。可以想象，洋子的生日是9月12日。 
    　      55th.day, 56th.day). 55th.day means "Your birthday is less than a week away. The 55th.day is a Saturday and Sunday from "It's been a week since your birthday," and "Will you be home Sunday? The 55th day is a Saturday or Sunday. In 56th.day, since her birthday is tomorrow and she made plans for the next day's date that night, the next day is Saturday (Sunday is also possible, but it would be inconsistent with "the week is up"). The next day is Saturday (or Sunday, but that would contradict "the week is up"). I heard that some private universities are closed only on the second and fourth Saturday of the month, and it makes sense to think of it as an out-of-class activity or an intensive lecture, etc. If we think of 55th. day as Saturday, September 5, 1998, the first week of September, and the next day, and 56th. day as Friday, September 11, 1998, and the next day, we can imagine that Yoko's birthday is September 12. I can imagine that Yoko's birthday is September 12.  


    Q2、 	薫の携帯の機種は？  
            🇨🇳薰的手机是什么型号？  
    A2、 	液晶のそばに「DoCoMo」らしき文字と間違いなく「DIGITAL」の文字が存在し、中央の卵形ボタンから、おそらくNTTDoCoMoのPシリーズ（52nd.day）。  
            🇨🇳可能是NTTDoCoMo的P系列（第52天），因为LCD附近的字母看起来像 "DoCoMo"，而且肯定是 "DIGITAL"，中间有一个蛋形按钮。  
    　
    Q3、 	辰巳さんの勤めている会社は？  
            辰巳先生为哪家公司工作？ 
    A3、 	「青丹興業」です。（60th.day）  
            是「青丹興業」。（60th.day）
    　
    Q4、 	森さんの携帯  
            森编辑的手机    
    A4、 	担当の森さんの携帯が出てきましたね。NTTDoCoMoのD206です。  
            负责人森编辑的手机已经出来了，是NTTDoCoMo D206。   
    　
    Q5、 	葉子の実家は？  
            叶子的父母的房子在哪里？   
    A6、 	Vol.13「88th.day近くて遠い東京」96ページ5コマ目で、葉子の実家の最寄り駅が示されました。「白玉」となっていますが、そのような駅は全国どこにも実在しません（PCソフトの「駅すぱあと」2000年8月版にて検索）。  
            在Vol.13"88th.day 似近似远的东京"中，第96页，第5分格，显示了离叶子父母家最近的车站。 该站的名称是"白玉"，但在日本任何地方都不存在这样一个站（根据2000年8月版的PC软件 "Eki Superato "的搜索结果）。   

    しかし、奥多摩であることはわかっているので、似たような駅名がないかどうか調べたところ、  
    F.COMPO設定： 	はとむね⇔しらたま⇔おくさま  
    実際（JR青梅線）： 	鳩ノ巣⇔白丸⇔奥多摩  
    然而，由于我知道它是在大口山，我检查了是否有一个类似的站名。  
    F.COMPO设定: はとむね⇔しらたま⇔おくさま  
    实际情况 (JR青梅線): 鳩ノ巣 ⇔ 白丸 ⇔ 奥多摩  

    という駅名がありました。したがって、おそらくJR青梅線白丸駅が最寄り駅だと思われます。それ以上の特定は難しいと思われますが、温泉旅館街でもあれば、そのあたりでしょう。  
    因此，JR青梅線白丸站可能是最近的车站。 很难进一步说明，但如果有一个温泉旅馆区，它就在那附近。   

    気になる旅館の名前は、「せせらぎ」。同じくVol.13の102ページに描いてある雅彦のはっぴに書いてあります。  
    我感兴趣的旅馆的名字是"せせらぎ"。 它写在雅彦的Happi上，也画在第13卷第102页。   
    　