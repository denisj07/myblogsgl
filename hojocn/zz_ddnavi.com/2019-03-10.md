source: https://ddnavi.com/review/524378/a/  


更多信息：  
https://ddnavi.com/person/6316  
https://ddnavi.com/person_tag/北条司/  

（借助在线翻译：[Google Translate](https://translate.google.cn)，[DeepL Translate](https://www.deepl.com/translator) ）


**『劇場版シティーハンター』がファンも驚く大ヒットになった要因とは？**  
是什么让《劇場版城市猎人》在粉丝中引起如此大的反响？

公開日：2019/3/10

![](https://images-na.ssl-images-amazon.com/images/I/51CX8JSh7WL._SX352_BO1,204,203,200_.jpg)  
『劇場版シティーハンター 〈新宿プライベート・アイズ〉：公式ノベライズ』（北条司：原作、加藤陽一：脚本、福井健太：著）
《劇場版城市猎人》：新宿私家侦探：官方小说"（北条司：原创故事，加藤陽一：剧本，福井健太：书）  

　2月8日に公開されたアニメ映画『劇場版シティーハンター 新宿プライベート・アイズ』が、ファンもびっくりするほどの大ヒットを記録している。このヒットを巡り、「なぜこんなに成功したのか？」という議論が飛び交うことになった。  
　2月8日上映的动漫电影《城市猎人电影：新宿私人侦探》已大热，令其粉丝感到惊讶。 围绕着这一命题有很多争论，"为什么它如此成功？"。 该片已在日本的影院上映。  

『劇場版シティーハンター』は公開初週の興行成績ランキングで、4位にランクイン。土日2日間で動員18万人、興行収入2億5700万円のロケットスタートを切った。その後も順調に数字を伸ばしていき、公開から17日でついに興収10億円を突破した。  
《劇場版城市猎人》在上映第一周的票房排名中位居第四。 它有一个火爆的开端，周六和周日有18万名观众和2.57亿日元的票房收入。 此后，数字继续稳步增长，上映后17天的票房终于超过了10亿日元。  

　昔からのファンがこの記録を支えている大きな一因になっているようだが、それだけ既存ファンを取り込めた理由に、Twitter上で「ラーメン屋でラーメン注文したらラーメン出てきたから」という解説がバズったこともあげられるだろう。  
　似乎曾经的粉丝是创造这一记录的主要原因之一，而该片能够吸引这么多现有粉丝的原因之一是Twitter上流行起来的"我在拉面店点了拉面，拉面就出来了 "的评论。  
  
　一見すると、当たり前過ぎることのように感じるが、確かに納得のいくたとえになっている。  
　乍一看，这似乎太明显了，但这确实是一个令人信服的比喻。  

　今回の映画では、主人公・冴羽リョウの声優として神谷明が起用された。神谷は80年代の地上波アニメ放送でも冴羽を演じているのだが、他にも、伊 倉一恵などお馴染みのキャストが約20年ぶりに再集合。年齢面から考えると、キャスト変更も十分にありえたわけだが、スタッフのこの決断に、ファンは「わ かってるじゃないか！」「やっぱり冴羽リョウは神谷明さんしかできない！」と感激したという。  
　在这部电影中，神谷明被选为主角冴羽獠的配音演员。 神谷在80年代播出的地面动画中也扮演过冴羽，其他熟悉的演员，如伊倉一恵，大约20年来首次重聚。 考虑到演员的年龄，更换演员是很有可能的，但主创人员的决定让粉丝们说："你知道我的意思！"， "毕竟，只有神谷明能扮演冴羽獠！"。    

　また、今の時代にはそぐわなそうなスケベでセクハラ気質の冴羽のキャラを、改変することなくそのままにしたことも賛同を得ている。原作者の北条司も、映画の完成披露あいさつで「今回は多少絵が現代的になったくらいで何も変わらない」と“変わらないこと”に太鼓判を押していた。  
　此外，冴羽的角色很猥琐，有性骚扰，似乎与这个时代格格不入，但他没有做任何改动就保持原样，这一点也得到了认可。 原作的作者北条司在影片完成时的讲话中也对影片的"不变 "给予了肯定，他说："这次没有什么变化，只是画面变得更现代了一些。"    

　作中でドローンやスマートフォンが登場するなど時代にあわせた部分はあるが、ノリやギャグもそのまま。極めつきは、エンディングテーマに「TM NETWORK」の「Get Wild」という憎い演出。まさに「ラーメン屋でラーメン注文したらラーメン出てきた」状態だ。  
　尽管影片中有些部分已经适应了时代的发展，例如无人机和智能手机在影片中的出现，但幽默和插科打诨的情节仍然没有改变。 最精彩的部分是结尾的主题曲，TM NETWORK的 "Get Wild"。 这就像 "我在一家拉面店点了拉面，拉面就出来了"。    

　ただ、この手法がどのような作品でも通用するかというとそうでもない。  
 然而，并不是说这种方法可以适用于任何种类的电影。    

　例えば、爆発的人気を引き起こしたアニメ『おそ松さん』は、赤塚不二夫の漫画『おそ松くん』を原作とし、大幅改変を施して成功したことが記憶に新 しい。2018年7月期のアニメ『深夜！ 天才バカボン』も現代風のネタを織り込みまくって成功した例だ。また、現在放送中のアニメ『ゲゲゲの鬼太郎』も、現代化が上手くハマることに。原作とはま るで違う美少女化した“ねこ娘”は、“大きなお友達”から大人気だ。  
　例如，爆红的动画片《大松哥》是根据赤冢不二夫的漫画《大松君》改编的，它以大刀阔斧的方式获得了成功，我们至今记忆犹新。2018年7月的动画片《深夜！ 天才バカボン》是另一个成功改编的例子，其中融入了大量的现代材料。 目前正在播出的动画片《ゲゲゲの鬼太郎》是另一个成功现代化的例子。 Neko Musume"，一个与原著完全不同的美少女，在她的 "大きなお友達"中非常受欢迎。   

　一方で、2018年12月14日に公開された映画『ドラゴンボール超 ブロリー』は、『劇場版シティーハンター』のように“そのまま路線”で大成功。90年代に放送されていた『ドラゴンボールZ』のような作画を再現している と話題になり、ファンからは「90年代のアニメのような線の粗さが素晴らしい」と歓喜の声が上がった。  
　另一方面，2018年12月14日上映的电影《龙珠超ブロリー》，因为走了《劇場版城市猎人》的 "老路"，被人说成是再现了90年代播出的《龙珠Z》那样的画风，获得了巨大的成功。 这成为一个话题，粉丝们表达了他们的喜悦之情，他们说："线条的粗糙度很好，就像90年代的动画一样。    

　ファンの好みを的確に捉えるマーケティング力には脱帽だ！  
　向他们的营销技巧致敬，因为他们准确地捕捉到了粉丝的口味!    

文＝桜木カキ

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


