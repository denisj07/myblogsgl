"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这2个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](./gesture_stand.md)  
    - 欢迎补充！欢迎指出错误！  



# FC的画风-动作、姿态

----------------------------------------------------------

- 对比站姿    
    FC里的站姿：  
    ![](../fc_drawing_style__body_proportion/masahiko/masahiko-00-2.jpg) 
    ![](../fc_drawing_style__body_proportion/masahiko/masahiko-02.jpg) 
    ![](../fc_drawing_style__body_proportion/shion/04_085_0__.jpg) 
    ![](../fc_drawing_style__body_proportion/shion/shion-04.jpg) 
    ![](../fc_drawing_style__body_proportion/shion/shion-05.jpg) 
    ![](../fc_drawing_style__body_proportion/shion/shion-07.jpg) 
    ![](img/02_121_5__mod.jpg) 
    ![](img/03_158_3__mod.jpg) 
    ![](img/03_161_0__mod.jpg) 
    ![](img/03_164_0__mod.jpg) 
    ![](img/03_190_4__mod.jpg)  
    ![](img/04_081_2__.jpg) 
    ![](img/04_082_3__.jpg) 

    CH里的站姿：  
    ![](../ch_drawing__gesture/img/ch_32_067_4.jpg) 
    ![](../ch_drawing__gesture/img/ch_32_067_4_r.jpg) 
    ![](../ch_drawing__gesture/img/ch_32_123_5_mod.jpg) 
    ![](../ch_drawing__gesture/img/ch_32_123_5_mod_r.jpg) 
    
    我感觉，相比CH，FC的站姿里的小腿(向前下方)的倾斜度小。  


- FC里人物站立时腿部的动作多变，腿部常常不是直线。事实上，人放松时，小腿常常弯曲：    
![](img/03_072_5__mod.jpg) 


- # 紫苑  
    - 腿部笔直：  
    ![](img/01_177_4__mod.jpg) 
    ![](img/03_069_5__mod.jpg) 
    ![](img/04_121_3__mod.jpg) 
    ![](img/05_194_1__mod.jpg) 
    ![](img/05_015_5__mod.jpg) 
    生气时: ![](img/06_091_0__mod.jpg) 
    生气时: ![](img/02_041_0__mod.jpg) 
    ![](img/06_182_2__mod.jpg) 
    ![](img/07_016_1__.jpg) 
    ![](img/07_067_2__.jpg) 
    ![](img/08_095_6.jpg) 
    ![](img/08_010_5.jpg) 

    - 腿部稍弯曲：  
    ![](img/04_137_3__mod.jpg) 
    ![](img/04_157_1__mod.jpg) 
    发呆时: ![](img/06_055_0__mod.jpg) 
    ![](img/06_093_2__mod.jpg) 
    ![](img/06_186_4__mod.jpg) 
    ![](img/07_068_0__.jpg) 
    ![](img/08_120_5.jpg) 

    - 对比下面两个图，左为站立，右(应该)为走路(右前臂离跨部稍远，应该是在摆臂)：  
    ![](img/07_011_5__.jpg) 
    ![](img/07_010_2__.jpg) 

    - 其他：  
    ![](img/01_125_0__crop.jpg) 
    ![](img/01_150_1__mod.jpg) 
    ![](img/06_085_0__mod.jpg) 
    ![](img/01_139_4__mod.jpg) 
    ![](img/03_025_4__mod.jpg) 
    ![](img/03_041_4__mod.jpg) 
    ![](img/03_042_0__mod.jpg) 
    ![](img/04_008_0__mod.jpg) 
    ![](img/04_092_7__mod.jpg) 
    ![](img/04_163_1__mod.jpg) 
    ![](img/05_041_0__mod.jpg) 
    ![](img/05_129_6__mod.jpg) 
    ![](img/05_133_6__mod.jpg) 
    ![](img/05_138_0__mod.jpg) 
    ![](img/05_142_0__mod.jpg) 
    ![](img/06_069_3__mod.jpg) 
    ![](img/06_076_1__mod.jpg) 
    ![](img/06_094_1__mod.jpg) 
    ![](img/06_177_0__mod.jpg) 
    ![](img/07_046_4__.jpg) 
    ![](img/07_047_6__.jpg) 
    ![](img/07_097_4__.jpg) 
    ![](img/07_111_2__.jpg) 
    ![](img/07_127_7__.jpg) 
    ![](img/07_151_4__.jpg) 
    ![](img/08_078_2.jpg) 
    ![](img/08_178_4.jpg) 


- # 雅美  
    - .  
    ![](img/03_041_4__mod.jpg) 
    ![](img/03_042_0__mod.jpg) 
    ![](img/03_054_1__mod.jpg) 
    ![](img/04_161_2__mod.jpg) 
    ![](img/04_162_2__mod.jpg) 
    ![](img/04_163_1__mod.jpg) 
    ![](img/06_076_1__mod.jpg) 
    ![](img/07_127_2__.jpg) 
    ![](img/07_129_1__.jpg) 
    ![](img/07_130_1__.jpg) 
    ![](img/07_133_2__.jpg) 
    ![](img/07_134_0__.jpg) 
    ![](img/07_137_0__.jpg) 
    ![](img/07_144_0__.jpg) 
    ![](img/07_149_3__.jpg) 
    ![](img/07_160_5__.jpg) 
    ![](img/08_190_6.jpg) 

- # 雅彦  
    - .   
    ![](img/02_016_2__.jpg) 
    ![](img/04_070_1__mod.jpg) 
    ![](img/04_108_3__mod.jpg) 
    ![](img/04_114_2__mod.jpg) 
    ![](img/04_174_2__mod.jpg) 
    ![](img/05_040_2__mod.jpg) 
    ![](img/05_041_0__mod.jpg) 
    ![](img/05_133_6__mod.jpg) 
    ![](img/05_138_0__mod.jpg) 
    ![](img/05_142_0__mod.jpg) 
    ![](img/05_194_1__mod.jpg) 
    ![](img/06_026_0__mod.jpg) 
    ![](img/06_090_2__mod.jpg) 
    ![](img/06_093_2__mod.jpg) 
    ![](img/06_182_2__mod.jpg) 
    ![](img/07_010_6__.jpg) 
    ![](img/07_054_5__.jpg) 
    ![](img/07_056_3__.jpg) 
    ![](img/07_056_4__.jpg) 
    ![](img/07_067_2__.jpg) 
    ![](img/07_068_0__.jpg) 
    ![](img/07_068_5__.jpg) 
    ![](img/07_081_0__.jpg) 
    ![](img/07_110_2__1.jpg) 
    ![](img/07_111_2__.jpg) 
    ![](img/08_029_0.jpg) 
    ![](img/08_073_2.jpg) 
    ![](img/08_078_2.jpg) 
    ![](img/08_090_4.jpg) 
    ![](img/08_095_6.jpg) 
    ![](img/08_100_4.jpg) 
    ![](img/08_116_4.jpg) 
    ![](img/08_137_1.jpg) 
    ![](img/08_138_4.jpg) 
    ![](img/08_154_1.jpg) 
    ![](img/08_157_2_2.jpg) 
    ![](img/08_157_6.jpg) 
    ![](img/08_159_7.jpg) 
    ![](img/08_162_0.jpg) 
    ![](img/08_174_2.jpg) 
    ![](img/08_177_6.jpg) 
    ![](img/08_178_0.jpg) 
    ![](img/08_180_4.jpg) 
    ![](img/08_180_7.jpg) 
    ![](img/08_183_0.jpg) 

- # 紫  
    - .   
    ![](img/04_070_1__mod.jpg) 
    ![](img/05_031_0__mod.jpg) 
    ![](img/08_060_2.jpg) 


- # 空  
    - 站立时腿部常常(几乎)呈直线：  
    ![](img/01_079_6__.jpg) 
    ![](img/05_055_3__mod.jpg) 
    ![](img/07_047_1__.jpg) 
    ![](img/08_015_4.jpg) 
    ![](img/08_046_1.jpg) 
    ![](img/08_120_5.jpg) 

    - 其他：  
    ![](img/01_121_5__.jpg) 
    ![](img/04_024_0__mod.jpg) 
    ![](img/04_025_5__mod.jpg) 
    ![](img/04_032_5__mod.jpg) 
    ![](img/04_033_4__mod.jpg) 
    ![](img/04_037_1__mod.jpg) 
    ![](img/04_038_0__mod.jpg) 
    ![](img/04_050_0__mod.jpg) 
    ![](img/04_057_0__mod.jpg) 
    ![](img/04_070_1__mod.jpg) 
    ![](img/06_190_0__mod.jpg) 
    ![](img/08_043_6.jpg) 
    ![](img/08_052_2.jpg) 
    ![](img/08_077_0.jpg) 
    ![](img/08_078_2.jpg) 
    ![](img/08_119_3.jpg) 

- # 叶子  
    - .       
    ![](img/02_064_4__mod.jpg) 
    ![](img/02_064_6__mod.jpg) 
    ![](img/03_041_4__mod.jpg) 
    ![](img/03_042_0__mod.jpg) 
    ![](img/03_054_1__mod.jpg) 
    ![](img/04_136_2__mod.jpg) 
    ![](img/04_161_2__mod.jpg) 
    ![](img/04_163_1__mod.jpg) 
    ![](img/05_023_4__mod.jpg) 
    ![](img/05_142_0__mod.jpg) 
    ![](img/07_122_0__.jpg) 
    ![](img/08_154_1.jpg) 
    ![](img/08_157_0.jpg) 
    ![](img/08_162_3.jpg) 
    ![](img/08_163_1.jpg) 
    ![](img/08_186_1.jpg) 
    ![](img/08_186_3.jpg) 
    ![](img/08_190_6.jpg) 

- 助手们  
    - .   
    ![](img/05_129_6__mod.jpg) 
    ![](img/07_068_0__.jpg) 
    ![](img/07_097_4__.jpg) 
    ![](img/07_105_6__.jpg) 
    ![](img/08_089_3.jpg) 
    ![](img/08_090_4.jpg) 

- # 熏  
    - .   
    ![](img/07_143_0__.jpg) 
    ![](img/07_144_0__.jpg) 
    ![](img/07_164_5__.jpg) 
    ![](img/07_179_5__.jpg) 
    ![](img/07_180_2__.jpg) 
    ![](img/08_037_6.jpg) 
    ![](img/08_077_0.jpg) 
    ![](img/08_081_5.jpg) 
    ![](img/08_090_4.jpg) 
    ![](img/08_127_1.jpg) 
    ![](img/08_151_2.jpg) 
    ![](img/08_183_3.jpg) 

- # 辰巳  
    - .   
    ![](img/04_145_6__mod.jpg) 
    ![](img/06_026_0__mod.jpg) 
    ![](img/06_026_4__mod.jpg) 
    ![](img/06_065_4__mod.jpg) 
    ![](img/06_076_1__mod.jpg) 
    ![](img/06_080_0_mod.jpg) 
    ![](img/07_184_1__.jpg) 


- # 顺子  
    - .   
    ![](img/04_038_0__mod.jpg) 
    ![](img/04_040_0__mod.jpg) 

- # 宪司  
    - .   
    ![](img/04_024_0__mod.jpg) 
    ![](img/04_025_5__mod.jpg) 
    ![](img/04_032_5__mod.jpg) 
    ![](img/04_033_4__mod.jpg) 
    ![](img/04_035_5__mod.jpg)
    ![](img/04_040_0__mod.jpg) 
    ![](img/04_050_0__mod.jpg) 

- # 爷爷
    - .     
    ![](img/04_057_0__mod.jpg) 


- # 江岛  
    - .   
    ![](img/08_080_2.jpg) 


- # 导演  
    - .   
    ![](img/03_035_1__mod.jpg) 
    ![](img/07_097_4__.jpg) 
    ![](img/07_127_7__.jpg) 
    ![](img/07_151_4__.jpg) 

  

- 八不为找妻子京子而贸然闯入若苗家。此时，他自知理亏、尴尬，不好意思露面。动作很生动--拖着步子、身体左右摇晃：  
![](img/02_192_2__mod.jpg)  
下图重心可能在右脚，也可能不是(而是视角产生的错觉)：    
![](img/02_192_4__hanzu.jpg)  


- 八不与京子和好。两人相依而站，京子两手相握于腰前、腿没有站直--像是传统日本女性穿和服的那种姿态：  
![](img/02_192_4__hanzu.jpg) 
![](img/02_192_4__kyoko.jpg)  


- 06_065_4，夜店老板的站姿有点娘（因为他的左膝盖向内撇）：     
![](img/06_065_4__mod.jpg) 
![](img/06_065_4__mod_fl.jpg) 


- # 其他

![](img/04_168_0__mod.jpg) 
![](img/05_012_6__mod.jpg) 
![](img/05_019_4__mod.jpg) 
![](img/07_010_6__.jpg) 
![](img/07_151_4__.jpg) 
![](img/07_160_5__.jpg) 
![](img/08_070_6.jpg) 
![](img/08_075_5.jpg) 
![](img/08_113_1.jpg) 
![](img/08_137_1.jpg) 









**参考链接**：  
1. []()  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



