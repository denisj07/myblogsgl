"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这2个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
    - 欢迎补充！欢迎指出错误！  



# FC的画风-动作、姿态

----------------------------------------------------------

- 站姿。  
![](../fc_drawing_style__body_proportion/masahiko/masahiko-00-2.jpg) 
![](../fc_drawing_style__body_proportion/masahiko/masahiko-02.jpg) 
![](../fc_drawing_style__body_proportion/shion/04_085_0__.jpg) 
![](../fc_drawing_style__body_proportion/shion/shion-04.jpg) 
![](../fc_drawing_style__body_proportion/shion/shion-05.jpg) 
![](../fc_drawing_style__body_proportion/shion/shion-07.jpg) 
![](img/02_041_0__mod.jpg) 
![](img/02_121_5__mod.jpg) 
![](img/02_192_4__hanzu.jpg) 
![](img/02_192_4__kyoko.jpg) 
![](img/03_072_5__mod.jpg) 
![](img/03_158_3__mod.jpg) 
![](img/03_161_0__mod.jpg) 
![](img/03_164_0__mod.jpg) 
![](img/03_190_4__mod.jpg) 
![](img/04_035_5__mod.jpg) 
![](img/04_037_1__mod.jpg) 
![](img/04_081_2__.jpg) 
![](img/04_082_3__.jpg) 
![](img/04_114_2__mod.jpg) 
![](img/04_137_3__mod.jpg) 
![](img/04_157_1__mod.jpg) 

对比CH里的站姿：  
![](../ch_drawing__gesture/img/ch_32_067_4.jpg) 
![](../ch_drawing__gesture/img/ch_32_067_4_r.jpg) 
![](../ch_drawing__gesture/img/ch_32_123_5_mod.jpg) 
![](../ch_drawing__gesture/img/ch_32_123_5_mod_r.jpg) 

我感觉，相比CH，FC的站姿里的小腿(向前下方)的倾斜度小。  


- 01_006_3, 角色身上的蓝线是角色站立时的透视线(绘制方法参见[1])，绿线表示角色的动态。绿线和蓝线相比，可以看出角色的身体倾斜程度。  
![](img/01_006_3__mod_mark.jpg)  


- 01_077_1, 10_033_2, 13_053_2, 13_067_4. 紫苑跑步的姿态(如果一只脚落地的话，这只脚落在身体中线上（或落在中线另一侧）):  
![](img/01_077_1__mod.jpg) 
![](img/01_094_0__mod.jpg) 
![](img/10_033_2__.jpg) 
![](img/13_053_2__.jpg) 
![](img/13_067_4__.jpg)  

- 14_205，紫苑走路的姿势，重心（蓝色竖线）在右脚，但又不在右脚中央，所以呈现出一种动态，同时还有种走路时(胯部/臀部)左右摇摆的感觉：

![](../fc_details/img/14_205_0__.jpg) 
![](../fc_details/img/14_205_0__mod.jpg)  


- 01_084_2, 鞋底弯曲程度越大，越能显示出力度：  
![](img/01_084_2__mod.jpg)  
但10_072_2的弯曲程度很小，不知为何：  
![](img/10_072_2__mod.jpg)  


- 01_125_0, 动作曲线很流畅。  
![](img/01_125_0__crop.jpg) 
![](img/01_125_0__mod.jpg)  


- 01_096_3, 眼睛向下看。  
![](img/01_096_3__mod.jpg) 
![](img/01_098_0__crop.jpg)  

- 01_097_4, (手里的热米饭让雅彦感到家的温暖，雅彦因此而流泪。紫和空很惊讶。)紫手部的动作辅助表情，显得更生动。去掉手，对比一下：   
![](img/01_097_4__mod.jpg)  
![](img/01_097_4__mod_no-hand.jpg)  


- 01_139_4, 腿部的动作曲线很流畅(腿部实际应该无交叉，但动作曲线是否可以画成交叉?)。  
![](img/01_139_4__mod.jpg) 
![](img/01_139_4__mod_force-line.jpg)  


- 01_139_4, 可能的动作曲线：  
![](img/01_147_2__mod.jpg) 
![](img/01_147_2__mod_force-line1.jpg) 
![](img/01_147_2__mod_force-line2.jpg) 
![](img/01_147_2__mod_force-line0.jpg)  


- 01_150_1, 动作曲线(重心在左脚)：  
![](img/01_150_1__mod.jpg) 
![](img/01_150_1__mod_force-line.jpg)  


- 01_164_8, 人的脖子的转角只能小于90度，该镜头里应该是大于90度（如果脖子要转这个角度，那么躯干肯定要相应旋转）：  
![](img/01_164_8__.jpg)  


- 01_177_4, 动作曲线：  
![](img/01_177_4__mod.jpg) 
![](img/01_177_4__mod_force-line.jpg)  


- 01_179_0, 动作曲线：  
![](img/01_179_0__mod.jpg) 
![](img/01_179_0__mod_force-line.jpg)  


- 01_190_0, 动作曲线：  
![](img/01_190_0__mod.jpg) 
![](img/01_190_0__mod_force-line.jpg)  


- 01_202_1, 动作曲线：  
![](img/01_202_1__mod.jpg) 
![](img/01_202_1__mod_force-line.jpg)  


- 01_079_6, 01_121_5, 紫不是单单的站立，而是腿上有小动作。这样似乎更生动。都说看北条司的漫画像是看电影，我觉得画面里角色的这些小动作对此起了一定作用。    
![](img/01_079_6__.jpg) 
![](img/01_121_5__.jpg) 


- 02_016_2, 看到雅彦珍爱的mark杯掉落，众人慌张。动作很生动。    
![](img/02_016_2__.jpg)  


- 02_018_4，02_018_7，一边生气、一边吃饭。  
![](img/02_018_4__mod.jpg) 
![](img/02_018_7__mod.jpg)  


- 02_040_5，紫苑一边避开紫和空、一边打算和雅彦说悄悄话。  
![](img/02_040_5__mod.jpg)  


- 02_050_1， 紫苑的坐姿很美（紫苑翘二郎腿）。雅彦搭在腿上的右手的手指很生动。  
![](img/02_050_1__.jpg)  


- 02_051_2，02_054_0，紫苑的坐姿很美。  
![](img/02_051_2__mod.jpg) 
![](img/02_054_0__mod.jpg)  


- 02_057_3，雅彦翘二郎腿。  
![](img/02_057_3__.jpg)  


- 02_064_4，02_064_6，动作很生动。  
![](img/02_064_4__mod.jpg) 
![](img/02_064_6__mod.jpg)  


- 02_082_3，紫苑翘二郎腿，空翘二郎腿，两人动作很相似。  
![](img/02_082_3__sora.jpg) 
![](img/02_082_3__shion.jpg)  


- 02_112_2__mod，02_113_3__mod，动作很生动。转身挥手时，换手扶背包。  
![](img/02_112_2__mod.jpg) 
![](img/02_113_3__mod.jpg)  


- 02_116_0,  
![](img/02_116_0__mod.jpg) 
![](img/02_116_0__mod_force-line.jpg)  


- 02_146_4, 走路时，紫可能习惯左手翘起。  
![](img/02_146_4__mod.jpg) 
![](img/02_175_0__.jpg)  



- 02_150_4,  
![](img/02_150_4__mod.jpg) 
![](img/02_150_4__mod_force-line.jpg) 


- 02_164_5, 欢快地跑。右脚尖上扬、右臂的动作很生动。  
![](img/02_164_5__mod.jpg)  
对比叶子(伤心地跑开)03_083_0：  
![](img/03_083_0__mod.jpg)  


- 02_192_2，02_192_4， 八不为找妻子京子而贸然闯入若苗家。此时，他自知理亏、尴尬，不好意思露面。动作很生动--拖着步子、身体左右摇晃。  
![](img/02_192_2__mod.jpg)  
下图重心可能在右脚，也可能不是(而是视角产生的错觉)：    
![](img/02_192_4__hanzu.jpg)  


- 02_192_4，八不与京子和好。两人相依而站，京子两手相握于腰前、腿没有站直--像是传统日本女性穿和服的那种姿态。  
![](img/02_192_4__hanzu.jpg) 
![](img/02_192_4__kyoko.jpg)  


- 02_195_4，紫依靠着空：  
![](img/02_195_4__mod.jpg) 
![](img/02_195_4__mod_force-line.jpg)  




- 03_020_3，横田进手拿烟的姿势很生动。  
![](img/03_020_3__.jpg)  


- 03_023_2,  
![](img/03_023_2__.jpg) 
![](img/03_023_2__mod_force-line.jpg)  


- 03_025_4,  
![](img/03_025_4__mod.jpg) 
![](img/03_025_4__mod_force-line.jpg)  


- 03_028_4,  
![](img/03_028_4__mod.jpg) 
![](img/03_028_4__mod_force-line.jpg)  


- 03_035_1,  
![](img/03_035_1__mod.jpg) 
![](img/03_035_1__mod_force-line.jpg)  


- 03_041_0,  
![](img/03_041_0__mod.jpg) 
![](img/03_041_0__mod_force-line.jpg)  


- 03_041_4, 03_042_0, 站姿各不相同。  
![](img/03_041_4__mod.jpg) 
![](img/03_042_0__mod.jpg)  


- 03_045_4，  
![](img/03_045_4__mod.jpg) 



- 03_049_1,  
![](img/03_049_1__mod.jpg) 
![](img/03_049_1__mod_force-line.jpg)  


- 03_053_6,  
![](img/03_053_6__mod.jpg) 
![](img/03_053_6__mod_force-line.jpg)  


- 03_054_6,  
![](img/03_054_1__mod.jpg) 
![](img/03_054_1__mod_force-line.jpg)  


- 03_068_5, 03_083_0, 03_083_4, 叶子跑步的姿态。  
![](img/03_068_5__mod.jpg) 
![](img/03_083_0__mod.jpg) 
![](img/03_083_4__mod.jpg)  


- 03_069_5,  
![](img/03_069_5__mod.jpg) 
![](img/03_069_5__mod_force-line.jpg)  



- 03_109_1, 紫苑、雅美、横田进跑步的姿态。横田进的手臂左右摇摆。紫苑和雅美的摆臂相同，但前后脚相反，说明有一个人是“一顺腿”(是亚美)。    
![](img/03_109_1__mod.jpg) 


- 03_144_4, 紫苑手的姿势：   
![](img/03_144_4__mod.jpg)  


- 03_144_5,  
![](img/03_144_5__mod.jpg) 
![](img/03_144_5__mod_force-line.jpg)  


- 03_150_3, 同一个画面画了2个动作：   
![](img/03_150_3__mod.jpg)  


- 03_179_3,  
![](img/03_179_3__mod.jpg) 
![](img/03_179_3__mod_force-line.jpg)  


- 03_194_1,  
![](img/03_194_1__mod.jpg) 
![](img/03_194_1__mod_force-line.jpg)  


- 04_005_3,  
![](img/04_005_3__mod.jpg) 
![](img/04_005_3__mod_force-line.jpg)  


- 04_008_0,  
![](img/04_008_0__mod.jpg) 
![](img/04_008_0__mod_force-line.jpg)  


- 04_024_0,  
![](img/04_024_0__mod.jpg) 
![](img/04_024_0__mod_force-line.jpg)  


- 04_025_2,  
![](img/04_025_2__mod.jpg) 
![](img/04_025_2__mod_force-line.jpg)  


- 04_025_5,  
![](img/04_025_5__mod.jpg) 
![](img/04_025_5__mod_force-line.jpg)  


- 04_029_0,  
![](img/04_029_0__mod.jpg) 
![](img/04_029_0__mod_force-line.jpg)  

 
- 04_031_0,  
![](img/04_031_0__mod.jpg) 
![](img/04_031_0__mod_force-line.jpg)  


- 04_031_5,  
![](img/04_031_5__mod.jpg) 
![](img/04_031_5__mod_force-line.jpg)  


- 04_032_5,  
![](img/04_032_5__mod.jpg) 
![](img/04_032_5__mod_force-line.jpg)   


- 04_033_4,  
![](img/04_033_4__mod.jpg) 
![](img/04_033_4__mod_force-line.jpg)   


- 04_034_6, 04_070_4, 空跑步。  
![](img/04_034_6__mod.jpg) 
![](img/04_070_4__mod.jpg) 
![](img/04_076_1__mod.jpg) 
![](img/04_076_5__mod.jpg) 



- 04_035_0,  
![](img/04_035_0__mod.jpg) 
![](img/04_035_0__mod_force-line.jpg)  


- 04_037_4,  
![](img/04_037_4__mod.jpg) 
![](img/04_037_4__mod_force-line.jpg)  


- 04_038_0,  
![](img/04_038_0__mod.jpg) 
![](img/04_038_0__mod_force-line.jpg)  


- 04_040_0,  
![](img/04_040_0__mod.jpg) 
![](img/04_040_0__mod_force-line.jpg)  


- 04_043_3,  
![](img/04_043_3__mod.jpg) 
![](img/04_043_3__mod_force-line.jpg)  


- 04_047_5, 04_049_4, 爷爷跑步。  
![](img/04_047_5__mod.jpg) 
![](img/04_047_5__mod_force-line.jpg)  

![](img/04_049_4__mod.jpg) 
![](img/04_049_4__mod_force-line.jpg)  


- 04_048_0,  
![](img/04_048_0__mod.jpg) 
![](img/04_048_0__mod_force-line.jpg)  


- 04_049_5,  
![](img/04_049_5__mod.jpg) 
![](img/04_049_5__mod_force-line.jpg)  


- 04_050_0,  
![](img/04_050_0__mod.jpg) 
![](img/04_050_0__mod_force-line.jpg)  


- 04_050_2，  
![](img/04_050_2__mod.jpg) 
![](img/04_050_2__mod_force-line.jpg)  


- 04_054_4，  
![](img/04_054_4__mod.jpg) 
![](img/04_054_4__mod_force-line.jpg)  


- 04_057_0，  
![](img/04_057_0__mod.jpg) 
![](img/04_057_0__mod_force-line.jpg)  


- 04_059_2，  
![](img/04_059_2__mod.jpg) 
![](img/04_059_2__mod_force-line.jpg)  


- 04_065_0，  
![](img/04_065_0__mod.jpg) 
![](img/04_065_0__mod_force-line.jpg)  


- 04_065_1，04_065_2，04_065_4，04_066_1，抹化妆品    
![](img/04_065_1__mod.jpg) 
![](img/04_065_2__mod.jpg) 
![](img/04_065_4__mod.jpg) 
![](img/04_066_1__mod.jpg) 

![](img/04_065_1__mod_force-line.jpg) 
![](img/04_065_2__mod_force-line.jpg) 
![](img/04_065_4__mod_force-line.jpg) 
![](img/04_066_1__mod_force-line.jpg) 


- 04_068_3__mod，  
![](img/04_068_3__mod.jpg) 
![](img/04_068_3__mod_force-line.jpg)  


- 04_070_1，  
![](img/04_070_1__mod.jpg) 
![](img/04_070_1__mod_force-line.jpg)  


- 04_071_0，紫跑步。     
![](img/04_071_0__mod.jpg) 
![](img/04_071_0__mod_force-line.jpg)  


- 04_087_1，     
![](img/04_087_1__mod.jpg) 
![](img/04_087_1__mod_force-line.jpg)  


- 04_092_7，     
![](img/04_092_7__mod.jpg) 
![](img/04_092_7__mod_force-line.jpg)  


- 04_105_5__mod，     
![](img/04_105_5__mod.jpg) 
![](img/04_105_5__mod_force-line.jpg)  


- 04_106_0，     
![](img/04_106_0__mod.jpg) 
![](img/04_106_0__mod_force-line.jpg)  


- 04_108_3，     
![](img/04_108_3__mod.jpg) 
![](img/04_108_3__mod_force-line.jpg)  


- 04_117_5，     
![](img/04_117_5__mod.jpg) 
![](img/04_117_5__mod_force-line.jpg)  


- 04_121_3，     
![](img/04_121_3__mod.jpg) 
![](img/04_121_3__mod_force-line.jpg)  


- 04_123_1，     
![](img/04_123_1__mod.jpg) 
![](img/04_123_1__mod_force-line.jpg)  


- 04_123_4，     
![](img/04_123_4__mod.jpg) 
![](img/04_123_4__mod_force-line.jpg)  


- 04_125_0，     
![](img/04_125_0__mod.jpg) 
![](img/04_125_0__mod_force-line.jpg)  


- 04_128_0，     
![](img/04_128_0__mod.jpg) 
![](img/04_128_0__mod_force-line.jpg)  


- 04_128_3，     
![](img/04_128_3__mod.jpg) 
![](img/04_128_3__mod_force-line.jpg)  


- 04_130_3，     
![](img/04_130_3__mod.jpg) 
![](img/04_130_3__mod_force-line.jpg)  


- 04_136_0，     
![](img/04_136_0__mod.jpg) 
![](img/04_136_0__mod_force-line.jpg)  


- 04_136_2，     
![](img/04_136_2__mod.jpg) 
![](img/04_136_2__mod_force-line.jpg)  


- 04_137_1，     
![](img/04_137_1__mod.jpg) 
![](img/04_137_1__mod_force-line.jpg)  


- 04_142_0，手抚摸头发的姿势：       
![](img/04_142_0__mod.jpg) 
![](img/04_142_0__mod_force-line.jpg)  


- 04_145_6，     
![](img/04_145_6__mod.jpg) 
![](img/04_145_6__mod_force-line.jpg)  


- 04_153_0，     
![](img/04_153_0__mod.jpg) 
![](img/04_153_0__mod_force-line.jpg)  


- 04_154_2，叶子跑步：        
![](img/04_154_2__mod.jpg) 
![](img/04_154_2__mod_force-line.jpg)  


- 04_154_5，     
![](img/04_154_5__mod.jpg) 
![](img/04_154_5__mod_force-line.jpg)  


- 04_157_1，     
![](img/04_157_1__mod.jpg) 
![](img/04_157_1__mod_force-line.jpg)  


- 04_161_2，     
![](img/04_161_2__mod.jpg) 
![](img/04_161_2__mod_force-line.jpg)  


- 04_162_1，     
![](img/04_162_1__mod.jpg) 
![](img/04_162_1__mod_force-line.jpg)  


- 04_162_2，     
![](img/04_162_2__mod.jpg) 
![](img/04_162_2__mod_force-line.jpg)  


- 04_163_1，     
![](img/04_163_1__mod.jpg) 
![](img/04_163_1__mod_force-line.jpg)  


- 04_168_0，     
![](img/04_168_0__mod.jpg) 
![](img/04_168_0__mod_force-line.jpg)  


- 04_174_2，     
![](img/04_174_2__mod.jpg) 
![](img/04_174_2__mod_force-line.jpg)  


- 04_187_0__mod，     
![](img/04_187_0__mod.jpg) 
![](img/04_187_0__mod_force-line.jpg)  


-----------  


- 05_011_3，  
![](img/05_011_3__mod.jpg) 
![](img/05_011_3__mod_force-line.jpg) 


- 05_012_6，  
![](img/05_012_6__mod.jpg) 
![](img/05_012_6__mod_force-line.jpg) 

- 05_014_4，  
![](img/05_014_4__mod.jpg) 
![](img/05_014_4__mod_force-line.jpg) 


- 05_015_5，  
![](img/05_015_5__mod.jpg) 
![](img/05_015_5__mod_force-line.jpg) 

- 05_018_0，  
![](img/05_018_0__mod.jpg) 
![](img/05_018_0__mod_force-line.jpg) 

- 05_019_4，  
![](img/05_019_4__mod.jpg) 
![](img/05_019_4__mod_force-line.jpg) 

- 05_019_5，  
![](img/05_019_5__mod.jpg) 
![](img/05_019_5__mod_force-line.jpg) 


- 05_021_2，  
![](img/05_021_2__mod.jpg) 
![](img/05_021_2__mod_force-line.jpg) 

- 05_021_6，  
![](img/05_021_6__mod.jpg) 
![](img/05_021_6__mod_force-line.jpg) 

- 05_022_0，  
![](img/05_022_0__mod_0.jpg) 
![](img/05_022_0__mod_0_force-line.jpg) 

- 05_022_0，  
![](img/05_022_0__mod_1.jpg) 
![](img/05_022_0__mod_1_force-line.jpg) 

- 05_022_1，  
![](img/05_022_1__mod.jpg) 
![](img/05_022_1__mod_force-line.jpg) 

- 05_023_1，  
![](img/05_023_1__mod.jpg) 
![](img/05_023_1__mod_force-line.jpg) 


- 05_023_4，  
![](img/05_023_4__mod.jpg) 
![](img/05_023_4__mod_force-line.jpg) 

- 05_024_1，  
![](img/05_024_1__mod.jpg) 
![](img/05_024_1__mod_force-line.jpg) 

- 05_031_0，  
![](img/05_031_0__mod.jpg) 
![](img/05_031_0__mod_force-line.jpg) 

- 05_033_0，  
![](img/05_033_0__mod.jpg) 
![](img/05_033_0__mod_force-line.jpg) 

- 05_037_4，  
![](img/05_037_4__mod.jpg) 
![](img/05_037_4__mod_force-line.jpg) 


- 05_038_3，  
![](img/05_038_3__mod.jpg) 
![](img/05_038_3__mod_force-line.jpg) 


- 05_038_5，  
![](img/05_038_5__mod.jpg) 
![](img/05_038_5__mod_force-line.jpg) 


- 05_040_2，  
![](img/05_040_2__mod.jpg) 
![](img/05_040_2__mod_force-line.jpg) 

- 05_041_0，  
![](img/05_041_0__mod.jpg) 
![](img/05_041_0__mod_force-line.jpg) 

- 05_041_2，  
![](img/05_041_2__mod.jpg) 
![](img/05_041_2__mod_force-line.jpg) 

- 05_055_3，  
![](img/05_055_3__mod.jpg) 
![](img/05_055_3__mod_force-line.jpg) 


- 05_060_3，  
![](img/05_060_3__mod.jpg) 
![](img/05_060_3__mod_force-line.jpg) 


- 05_060_4，  
![](img/05_060_4__mod.jpg) 
![](img/05_060_4__mod_force-line.jpg) 

- 05_063_3，  
![](img/05_063_3__mod.jpg) 
![](img/05_063_3__mod_force-line.jpg) 

- 05_063_4，  
![](img/05_063_4__mod.jpg) 
![](img/05_063_4__mod_force-line.jpg) 


![](img/05_075_2__sora_side_surprise_fear.jpg) 

- 05_083_0，  
![](img/05_083_0__mod.jpg) 
![](img/05_083_0__mod_force-line.jpg) 

- 05_098_1，  
![](img/05_098_1__mod.jpg) 
![](img/05_098_1__mod_fl.jpg) 


- 05_099_3，  
![](img/05_099_3__mod.jpg) 
![](img/05_099_3__mod_fl.jpg) 

- 05_111_0，  
![](img/05_111_0__mod.jpg) 
![](img/05_111_0__mod_fl.jpg) 


- 05_117_4，  
![](img/05_117_4__mod.jpg) 
![](img/05_117_4__mod_fl.jpg) 

- 05_118_2，  
![](img/05_118_2___mod.jpg) 
![](img/05_118_2___mod_fl.jpg) 

- 05_119_2，  
![](img/05_119_2__.jpg) 
![](img/05_119_2__fl.jpg) 

- 05_126_5，  
![](img/05_126_5__mod.jpg) 
![](img/05_126_5__mod_fl.jpg) 

- 05_129_6，  
![](img/05_129_6__mod.jpg) 
![](img/05_129_6__mod_fl.jpg) 

- 05_133_6，  
![](img/05_133_6__mod.jpg) 
![](img/05_133_6__mod_fl.jpg) 

- 05_134_2，  
![](img/05_134_2__mod.jpg) 
![](img/05_134_2__mod_fl.jpg) 

- 05_138_0，  
![](img/05_138_0__mod.jpg) 
![](img/05_138_0__mod_fl.jpg) 

- 05_142_0，  
![](img/05_142_0__mod.jpg) 
![](img/05_142_0__mod_fl.jpg) 

- 05_155_0，  
![](img/05_155_0__mod.jpg) 
![](img/05_155_0__mod_fl.jpg) 

- 05_167_0，  
![](img/05_167_0__mod.jpg) 
![](img/05_167_0__mod_fl.jpg) 

- 05_193_3，  
![](img/05_193_3__mod.jpg) 
![](img/05_193_3__mod_fl.jpg) 

- 05_194_1，  
![](img/05_194_1__mod.jpg) 
![](img/05_194_1__mod_fl.jpg) 

---------------------------------------

- 06_004_3  
![](img/06_004_3__mod.jpg) 
![](img/06_004_3__mod_fl.jpg) 

- 06_005_5，  
![](img/06_005_5__mod.jpg) 
![](img/06_005_5__mod_fl.jpg) 

- 06_006_5，  
![](img/06_006_5__mod.jpg) 
![](img/06_006_5__mod_fl.jpg) 

- 06_008_3，  
![](img/06_008_3__mod.jpg) 
![](img/06_008_3__mod_fl.jpg) 

- 06_010_3，  
![](img/06_010_3__mod.jpg) 
![](img/06_010_3__mod_fl.jpg) 

- 06_018_4，  
![](img/06_018_4__mod.jpg) 
![](img/06_018_4__mod_fl.jpg) 

- 06_019_7，  
![](img/06_019_7__mod.jpg) 
![](img/06_019_7__mod_fl.jpg) 

- 06_023_4，弹烟灰的动作很生动：  
![](img/06_023_4__mod.jpg) 

- 06_023_5，  
![](img/06_023_5__mod.jpg) 
![](img/06_023_5__mod_fl.jpg) 

- 06_026_0，  
![](img/06_026_0__mod.jpg) 
![](img/06_026_0__mod_fl.jpg) 

- 06_026_4，  
![](img/06_026_4__mod.jpg) 
![](img/06_026_4__mod_fl.jpg) 

- 06_027_2，  
![](img/06_027_2__mod.jpg) 
![](img/06_027_2__mod_fl.jpg) 

- 06_042_0，  
![](img/06_042_0__mod.jpg) 
![](img/06_042_0__mod_fl.jpg) 

- 06_045_1，  
![](img/06_045_1__mod.jpg) 
![](img/06_045_1__mod_fl.jpg) 

- 06_052_0，  
![](img/06_052_0__mod.jpg) 
![](img/06_052_0__mod_fl.jpg) 

- 06_055_0，  
![](img/06_055_0__mod.jpg) 
![](img/06_055_0__mod_fl.jpg) 

- 06_058_2，  
![](img/06_058_2__mod.jpg) 
![](img/06_058_2__mod_fl.jpg) 

- 06_059_3，  
![](img/06_059_3__mod.jpg) 
![](img/06_059_3__mod_fl.jpg) 

- 06_062_1，  
![](img/06_062_1__mod.jpg) 
![](img/06_062_1__mod_fl.jpg) 

- 06_062_3，  
![](img/06_062_3__mod.jpg) 
![](img/06_062_3__mod_fl.jpg) 

- 06_062_5，  
![](img/06_062_5__mod.jpg) 
![](img/06_062_5__mod_fl.jpg) 

- 06_064_3，  
![](img/06_064_3__mod.jpg) 
![](img/06_064_3__mod_fl.jpg) 

- 06_065_4，夜店老板的站姿有点娘    
![](img/06_065_4__mod.jpg) 
![](img/06_065_4__mod_fl.jpg) 

- 06_069_3，  
![](img/06_069_3__mod.jpg) 
![](img/06_069_3__mod_fl.jpg) 

- 06_076_1，  
![](img/06_076_1__mod.jpg) 
![](img/06_076_1__mod_fl.jpg) 

- 06_079_1，  
![](img/06_079_1__mod.jpg) 
![](img/06_079_1__mod_fl.jpg) 

- 06_079_4，  
![](img/06_079_4__mod.jpg) 
![](img/06_079_4__mod_fl.jpg) 

- 06_080_0，  
![](img/06_080_0_mod.jpg) 
![](img/06_080_0_mod_fl.jpg) 

- 06_085_0，  
![](img/06_085_0__mod.jpg) 
![](img/06_085_0__mod_fl.jpg) 

- 06_086_0，  
![](img/06_086_0__mod.jpg) 
![](img/06_086_0__mod_fl.jpg) 

- 06_088_2，  
![](img/06_088_2__mod.jpg) 
![](img/06_088_2__mod_fl.jpg) 

- 06_089_0，  
![](img/06_089_0__mod.jpg) 
![](img/06_089_0__mod_fl.jpg) 

- 06_089_4，  
![](img/06_089_4__mod.jpg) 
![](img/06_089_4__mod_fl.jpg) 

- 06_090_2，  
![](img/06_090_2__mod.jpg) 
![](img/06_090_2__mod_fl.jpg) 

- 06_090_3，  
![](img/06_090_3__mod.jpg) 
![](img/06_090_3__mod_fl.jpg) 

- 06_090_5，  
![](img/06_090_5__mod.jpg) 
![](img/06_090_5__mod_fl.jpg) 

- 06_091_0，  
![](img/06_091_0__mod.jpg) 
![](img/06_091_0__mod_fl.jpg) 

- 06_093_2，  
![](img/06_093_2__mod.jpg) 
![](img/06_093_2__mod_fl.jpg) 

- 06_094_1，  
![](img/06_094_1__mod.jpg) 
![](img/06_094_1__mod_fl.jpg) 

- 06_116_5，06_117_4，这个角度和下颌处的画法似乎在猫眼里多见：  
![](img/06_116_5__.jpg) 
![](img/06_117_4__mod.jpg) 

- 06_120_2，  
![](img/06_120_2__mod.jpg) 
![](img/06_120_2__mod_fl.jpg) 

- 06_120_5，  
![](img/06_120_5__mod.jpg) 
![](img/06_120_5__mod_fl.jpg) 

- 06_126_0，  
![](img/06_126_0__mod.jpg) 
![](img/06_126_0__mod_fl.jpg) 

- 06_128_6，  
![](img/06_128_6__mod.jpg) 
![](img/06_128_6__mod_fl.jpg) 

- 06_137_0，   
![](img/06_137_0__mod.jpg) 
![](img/06_137_0__mod_fl.jpg) 

- 06_144，06_163。茜是一个怎样的女孩子，刻画得很生动了：内八字，跑步时手臂左右摇摆  
![](../fc_details/img/06_144_2__.jpg) 
![](../fc_details/img/06_163_5__.jpg) 
![](../fc_details/img/06_163_6__.jpg)  

- 06_146_3，  
![](img/06_146_3__mod.jpg) 
![](img/06_146_3__mod_fl.jpg) 

- 06_151_1，  
![](img/06_151_1__mod.jpg) 
![](img/06_151_1__mod_fl.jpg) 

- 06_163_1，  
![](img/06_163_1__mod.jpg) 
![](img/06_163_1__mod_fl.jpg) 

- 06_166_2，  
![](img/06_166_2__mod.jpg) 
![](img/06_166_2__mod_fl.jpg) 

- 06_172_1，  
![](img/06_172_1__mod.jpg) 
![](img/06_172_1__mod_fl.jpg) 

- 06_174_1，  
![](img/06_174_1__mod.jpg) 
![](img/06_174_1__mod_fl.jpg) 

- 06_177_0，  
![](img/06_177_0__mod.jpg) 
![](img/06_177_0__mod_fl.jpg) 

- 06_177_3，  
![](img/06_177_3__mod.jpg) 
![](img/06_177_3__mod_fl.jpg) 

- 06_182_2，  
![](img/06_182_2__mod.jpg) 
![](img/06_182_2__mod_fl.jpg) 

- 06_186_4，  
![](img/06_186_4__mod.jpg) 
![](img/06_186_4__mod_fl.jpg) 

- 06_190_0，  
![](img/06_190_0__mod.jpg) 
![](img/06_190_0__mod_fl.jpg) 

---------------------------------------
- 07_006_6  
![](img/07_006_6__.jpg) 
![](img/07_006_6__fl.jpg)  


- 07_010_2  
![](img/07_010_2__.jpg) 
![](img/07_010_2__fl.jpg)  


- 07_010_6  
![](img/07_010_6__.jpg) 
![](img/07_010_6__fl.jpg)  


- 07_011_5  
![](img/07_011_5__.jpg) 
![](img/07_011_5__fl.jpg)  


- 07_016_1  
![](img/07_016_1__.jpg) 
![](img/07_016_1__fl.jpg)  


- 07_016_3  
![](img/07_016_3__.jpg) 
![](img/07_016_3__fl.jpg)  


- 07_022_1  
![](img/07_022_1__.jpg) 
![](img/07_022_1__fl.jpg)  


- 07_022_3  
![](img/07_022_3__.jpg) 
![](img/07_022_3__fl.jpg)  


- 07_023_2, 紫苑是正在坐下还是站起身？剧情里是坐下。坐下时重心偏后、身体放松；起身时重心偏前。    
![](img/07_023_2__0.jpg) 
![](img/07_023_2__0_fl.jpg)  

![](img/07_023_2__1.jpg) 
![](img/07_023_2__1_fl.jpg)  


- 07_046_4  
![](img/07_046_4__.jpg) 
![](img/07_046_4__fl.jpg)  


- 07_047_1  
![](img/07_047_1__.jpg) 
![](img/07_047_1__fl.jpg)  


- 07_047_6  
![](img/07_047_6__.jpg) 
![](img/07_047_6__fl.jpg)  


- 07_054_5  
![](img/07_054_5__.jpg) 
![](img/07_054_5__fl.jpg)  


- 07_056_3  
![](img/07_056_3__.jpg) 
![](img/07_056_3__fl.jpg)  


- 07_056_4  
![](img/07_056_4__.jpg) 
![](img/07_056_4__fl.jpg)  


- 07_056_5  
![](img/07_056_5__.jpg) 
![](img/07_056_5__fl.jpg)  


- 07_067_2  
![](img/07_067_2__.jpg) 
![](img/07_067_2__fl.jpg)  


- 07_068_0  
![](img/07_068_0__.jpg) 
![](img/07_068_0__fl.jpg)  


- 07_068_5  
![](img/07_068_5__.jpg) 
![](img/07_068_5__fl.jpg)  


-  07_069_3   
![](img/07_069_3__.jpg) 
![](img/07_069_3__fl.jpg)  


- 07_071_3  
![](img/07_071_3__0.jpg) 
![](img/07_071_3__0_fl.jpg)  

![](img/07_071_3__1.jpg) 
![](img/07_071_3__1_fl.jpg)  


- 07_080_4  
![](img/07_080_4__.jpg) 
![](img/07_080_4__fl.jpg)  


- 07_080_5  
![](img/07_080_5__.jpg) 
![](img/07_080_5__fl.jpg)  


-  07_081_0  
![](img/07_081_0__.jpg) 
![](img/07_081_0__fl.jpg)  


- 07_083_2  
![](img/07_083_2__.jpg) 
![](img/07_083_2__fl.jpg)  


- 07_093_0  
![](img/07_093_0__.jpg) 
![](img/07_093_0__fl.jpg)  


-  07_093_6   
![](img/07_093_6__.jpg) 
![](img/07_093_6__fl.jpg)  


- 07_094_3  
![](img/07_094_3__.jpg) 
![](img/07_094_3__fl.jpg)  


- 07_094_4_  
![](img/07_094_4__.jpg) 
![](img/07_094_4__fl.jpg)  


- 07_095_6  
![](img/07_095_6__.jpg) 
![](img/07_095_6__fl.jpg)  


- 07_096_3  
![](img/07_096_3__.jpg) 
![](img/07_096_3__fl.jpg)  


- 07_097_4  
![](img/07_097_4__.jpg) 
![](img/07_097_4__fl.jpg)  


- 07_101_5  
![](img/07_101_5__.jpg) 
![](img/07_101_5__fl.jpg)  


- 07_105_6  
![](img/07_105_6__.jpg) 
![](img/07_105_6__fl.jpg)  


- 07_107_1,齐藤跑步的姿势：    
![](img/07_107_1__.jpg) 
![](img/07_107_1__fl.jpg)  


- 7_109_6  
![](img/07_109_6__.jpg) 
![](img/07_109_6__fl.jpg)  


- 07_110_2   
![](img/07_110_2__0.jpg) 
![](img/07_110_2__0_fl.jpg)  

![](img/07_110_2__1.jpg) 
![](img/07_110_2__1_fl.jpg)  


- 07_111_2  
![](img/07_111_2__.jpg) 
![](img/07_111_2__fl.jpg)  


-  07_113_7  
![](img/07_113_7__.jpg) 
![](img/07_113_7__fl.jpg)  


- 07_117_1  
![](img/07_117_1__.jpg) 
![](img/07_117_1__fl.jpg)  


- 07_120_7  
![](img/07_120_7__.jpg) 
![](img/07_120_7__fl.jpg)  


- 07_121_6  
![](img/07_121_6__.jpg) 
![](img/07_121_6__fl.jpg)  


- 7_122_0  
![](img/07_122_0__.jpg) 
![](img/07_122_0__fl.jpg)  


- 07_123_2  
![](img/07_123_2__.jpg) 
![](img/07_123_2__fl.jpg)  


- 07_123_5  
![](img/07_123_5__.jpg) 
![](img/07_123_5__fl.jpg)  


- 07_124_3  
![](img/07_124_3__.jpg) 
![](img/07_124_3__fl.jpg)  


- 07_124_6  
![](img/07_124_6__.jpg) 
![](img/07_124_6__fl.jpg)  


- 07_127_2  
![](img/07_127_2__.jpg) 
![](img/07_127_2__fl.jpg)  


- 07_127_7  
![](img/07_127_7__.jpg) 
![](img/07_127_7__fl.jpg)  


- 07_128_1  
![](img/07_128_1__.jpg) 
![](img/07_128_1__fl.jpg)  


- 7_129_1  
![](img/07_129_1__.jpg) 
![](img/07_129_1__fl.jpg)  


- 07_129_3  
![](img/07_129_3__.jpg) 
![](img/07_129_3__fl.jpg)  


- 07_130_0  
![](img/07_130_0__.jpg) 
![](img/07_130_0__fl.jpg)  


- 07_130_1  
![](img/07_130_1__.jpg) 
![](img/07_130_1__fl.jpg)  


- 07_132_0  
![](img/07_132_0__.jpg) 
![](img/07_132_0__fl.jpg)  


- 07_132_1  
![](img/07_132_1__.jpg) 
![](img/07_132_1__fl.jpg)  


- 07_132_2  
![](img/07_132_2__.jpg) 
![](img/07_132_2__fl.jpg)  


- 07_132_4  
![](img/07_132_4__.jpg) 
![](img/07_132_4__fl.jpg)  


- 07_132_5，背景里右侧的人跑步姿势好像是“一顺腿”：  
![](img/07_132_5__.jpg) 
![](img/07_132_5__fl.jpg)  


- 07_133_2    
![](img/07_133_2__.jpg) 
![](img/07_133_2__fl.jpg)  


- 07_134_0  
![](img/07_134_0__.jpg) 
![](img/07_134_0__fl.jpg)  


- 07_134_1  
![](img/07_134_1__.jpg) 
![](img/07_134_1__fl.jpg)  


-  07_134_2  
![](img/07_134_2__.jpg) 
![](img/07_134_2__fl.jpg)  


- 07_135_1  
![](img/07_135_1__.jpg) 
![](img/07_135_1__fl.jpg)  


- 07_137_0，熏走路的一个特点是脚尖不离地：    
![](img/07_137_0__.jpg) 
![](img/07_137_0__fl.jpg)  


- 07_143_0  
![](img/07_143_0__.jpg) 
![](img/07_143_0__fl.jpg)  


- 07_144_0  
![](img/07_144_0__.jpg) 
![](img/07_144_0__fl.jpg)  


- 07_146_1  
![](img/07_146_1__.jpg) 
![](img/07_146_1__fl.jpg)  


- 07_146_3，07_198_6， 熏跑步：  
这是正面还是背影？剧情里是背影。  
![](img/07_146_3__.jpg) 
![](img/07_146_3__fl.jpg)  
  
![](img/07_198_6__.jpg) 
![](img/07_198_6__fl.jpg)  


- 07_148_6    
![](img/07_148_6__.jpg) 
![](img/07_148_6__fl.jpg)  


- 07_149_3  
![](img/07_149_3__.jpg) 
![](img/07_149_3__fl.jpg)  


- 07_151_4  
![](img/07_151_4__.jpg) 
![](img/07_151_4__fl.jpg)  


- 07_155_6  
![](img/07_155_6__.jpg) 
![](img/07_155_6__fl.jpg)  


- 07_156_1  
![](img/07_156_1__.jpg) 
![](img/07_156_1__fl.jpg)  


- 07_157_1  
![](img/07_157_1__.jpg) 
![](img/07_157_1__fl.jpg)  


- 07_157_5  
![](img/07_157_5__.jpg) 
![](img/07_157_5__fl.jpg)  


- 07_159_2  
![](img/07_159_2__.jpg) 
![](img/07_159_2__fl.jpg)  


- 07_160_0  
![](img/07_160_0__.jpg) 
![](img/07_160_0__fl.jpg)  


- 07_160_5  
![](img/07_160_5__.jpg) 
![](img/07_160_5__fl.jpg)  


- 07_161_7  
![](img/07_161_7__.jpg) 
![](img/07_161_7__fl.jpg)  


- 07_164_1，这是接还是抛？是接东西。接东西时身体会后仰、视线会集中在物体上；抛东西时不会。    
![](img/07_164_1__.jpg) 
![](img/07_164_1__fl.jpg)  


- 07_164_2  
![](img/07_164_2__.jpg) 
![](img/07_164_2__fl.jpg)  


- 07_164_5  
![](img/07_164_5__.jpg) 
![](img/07_164_5__fl.jpg)  


- 07_166_0  
![](img/07_166_0__.jpg) 
![](img/07_166_0__fl.jpg)  


- 07_172_0  
![](img/07_172_0__.jpg) 
![](img/07_172_0__fl.jpg)  


- 07_175_3  
![](img/07_175_3__.jpg) 
![](img/07_175_3__fl.jpg)  


- 07_175_4  
![](img/07_175_4__.jpg) 
![](img/07_175_4__fl.jpg)  


- 07_176_0  
![](img/07_176_0__.jpg) 
![](img/07_176_0__fl.jpg)  


- 07_177_6，07_177_7，07_178_0，熏走路时的样子：    
![](img/07_177_6__.jpg) 
![](img/07_177_6__fl.jpg)  

![](img/07_177_7__.jpg) 
![](img/07_177_7__fl.jpg)  

![](img/07_178_0__.jpg) 
![](img/07_178_0__fl.jpg)  


- 07_178_1  
![](img/07_178_1__.jpg) 
![](img/07_178_1__fl.jpg)  


- 07_179_5  
![](img/07_179_5__.jpg) 
![](img/07_179_5__fl.jpg)  


- 07_180_2  
![](img/07_180_2__.jpg) 
![](img/07_180_2__fl.jpg)  


- 07_184_1  
![](img/07_184_1__.jpg) 
![](img/07_184_1__fl.jpg)  


- 07_186_0  
![](img/07_186_0__.jpg) 
![](img/07_186_0__fl.jpg)  


- 07_193_2  
![](img/07_193_2__.jpg) 
![](img/07_193_2__fl.jpg)  


- 07_195_0，熏翘二郎腿：    
![](img/07_195_0__.jpg) 
![](img/07_195_0__fl.jpg)  


- 07_195_5  
![](img/07_195_5__.jpg) 
![](img/07_195_5__fl.jpg)  


- 07_197_1  
![](img/07_197_1__.jpg) 
![](img/07_197_1__fl.jpg)  


- 07_197_2  
![](img/07_197_2__.jpg) 
![](img/07_197_2__fl.jpg)  


- 07_197_5  
![](img/07_197_5__.jpg) 
![](img/07_197_5__fl.jpg)  


---------------------------------------

- 08_003_0  
![](img/08_003_0.jpg) 
![](img/08_003_0__mod.jpg)  

- 08_010_5  
![](img/08_010_5.jpg) 
![](img/08_010_5__mod.jpg)  

- 08_013_6  
![](img/08_013_6.jpg) 
![](img/08_013_6__mod.jpg)   

- 08_015_4  
![](img/08_015_4.jpg) 
![](img/08_015_4__mod.jpg)  

- 08_018_6  
![](img/08_018_6.jpg) 
![](img/08_018_6__mod.jpg)  

- 08_021_6  
![](img/08_021_6.jpg) 
![](img/08_021_6__mod.jpg)  

- 08_026_4  
![](img/08_026_4.jpg) 
![](img/08_026_4__mod.jpg)  

- 08_029_0  
![](img/08_029_0.jpg) 
![](img/08_029_0__mod.jpg)  

- 08_035_3  
![](img/08_035_3.jpg) 
![](img/08_035_3__mod.jpg) 



- 08_037_6  
![](img/08_037_6.jpg) 
![](img/08_037_6__mod.jpg) 

- 08_041_2  
![](img/08_041_2.jpg) 
![](img/08_041_2__mod.jpg) 

- 08_041_4  
![](img/08_041_4.jpg) 
![](img/08_041_4__mod.jpg)  

- 08_042_3  
![](img/08_042_3.jpg) 
![](img/08_042_3__mod.jpg)  

- 08_043_0  
![](img/08_043_0.jpg) 
![](img/08_043_0__mod.jpg)  

- 08_043_6  
![](img/08_043_6.jpg) 
![](img/08_043_6__mod.jpg)  

- 08_046_1  
![](img/08_046_1.jpg) 
![](img/08_046_1__mod.jpg)  

- 08_046_4  
![](img/08_046_4.jpg) 
![](img/08_046_4__mod.jpg)  

- 08_047_2  
![](img/08_047_2.jpg) 
![](img/08_047_2__mod.jpg)  

- 08_052_2  
![](img/08_052_2.jpg) 
![](img/08_052_2__mod.jpg)  

- 08_052_5  
![](img/08_052_5.jpg)  

- 08_053_6  
![](img/08_053_6.jpg)  

- 08_054_1  
![](img/08_054_1.jpg) 
![](img/08_054_1__mod.jpg)  

- 08_054_4  
![](img/08_054_4.jpg) 
![](img/08_054_4__mod.jpg)  

- 08_054_6  
![](img/08_054_6.jpg) 
![](img/08_054_6__mod.jpg)  

- 08_057_3  
![](img/08_057_3.jpg) 
![](img/08_057_3__mod.jpg)  

- 08_060_1  
![](img/08_060_1.jpg) 

- 08_060_2  
![](img/08_060_2.jpg) 
![](img/08_060_2__mod.jpg)  

- 08_061_2  
![](img/08_061_2.jpg) 
![](img/08_061_2__mod.jpg)  

- 08_061_3  
![](img/08_061_3.jpg) 
![](img/08_061_3__mod.jpg)  

- 08_061_5  
![](img/08_061_5.jpg) 
![](img/08_061_5__mod.jpg)  

- 08_062_3  
![](img/08_062_3.jpg) 
![](img/08_062_3__mod.jpg)  

- 08_066_5  
![](img/08_066_5.jpg) 
![](img/08_066_5__mod.jpg)  

- 08_067_1  
![](img/08_067_1.jpg) 
![](img/08_067_1__mod.jpg)  

- 08_070_6  
![](img/08_070_6.jpg) 
![](img/08_070_6__mod.jpg)  

- 08_071_6  
![](img/08_071_6.jpg) 
![](img/08_071_6__mod.jpg)  

- 08_073_1  
![](img/08_073_1.jpg) 
![](img/08_073_1__mod.jpg)  

- 08_073_2  
![](img/08_073_2.jpg) 
![](img/08_073_2__mod.jpg)  

- 08_073_2  
![](img/08_073_2_2.jpg) 

- 08_075_5  
![](img/08_075_5.jpg) 
![](img/08_075_5__mod.jpg)  

- 08_076_1  
![](img/08_076_1.jpg) 
![](img/08_076_1__mod.jpg)  

- 08_077_0  
![](img/08_077_0.jpg) 
![](img/08_077_0__mod.jpg)  

- 08_078_2  
![](img/08_078_2.jpg) 
![](img/08_078_2__mod.jpg)  

- 08_079_0  
![](img/08_079_0.jpg) 
![](img/08_079_0__mod.jpg)  

- 08_079_5  
![](img/08_079_5.jpg) 
![](img/08_079_5__mod.jpg) 

- 08_080_2  
![](img/08_080_2.jpg) 
![](img/08_080_2__mod1.jpg) 
![](img/08_080_2__mod2.jpg) 
![](img/08_080_2__mod3.jpg) 
![](img/08_080_2__mod4.jpg)  

- 08_080_4  
![](img/08_080_4.jpg) 
![](img/08_080_4__mod.jpg)  

- 08_081_5  
![](img/08_081_5.jpg) 
![](img/08_081_5__mod.jpg)  

- 08_082_4  
![](img/08_082_4.jpg) 
![](img/08_082_4__mod.jpg)  

- 08_083_1  
![](img/08_083_1.jpg) 
![](img/08_083_1__mod.jpg)  

- 08_084_3  
![](img/08_084_3.jpg) 
![](img/08_084_3__mod.jpg)  

- 08_085_5  
![](img/08_085_5.jpg) 
![](img/08_085_5__mod.jpg)  

- 08_087_0  
![](img/08_087_0.jpg) 
![](img/08_087_0__mod.jpg)  

- 08_089_3  
![](img/08_089_3.jpg) 
![](img/08_089_3__mod.jpg)  

- 08_090_4  
![](img/08_090_4.jpg) 
![](img/08_090_4__mod.jpg)  

- 08_091_4  
![](img/08_091_4.jpg) 
![](img/08_091_4__mod.jpg)  

- 08_092_2  
![](img/08_092_2.jpg) 
![](img/08_092_2__mod.jpg)  

- 08_093_1  
![](img/08_093_1.jpg) 
![](img/08_093_1__mod.jpg)  

- 08_094_2  
![](img/08_094_2.jpg) 
![](img/08_094_2__mod.jpg)  

- 08_095_1  
![](img/08_095_1.jpg) 
![](img/08_095_1__mod.jpg)  

- 08_095_6  
![](img/08_095_6.jpg) 
![](img/08_095_6__mod.jpg)  

- 08_100_4  
![](img/08_100_4.jpg) 
![](img/08_100_4__mod.jpg)  

- 08_101_7  
![](img/08_101_7.jpg) 
![](img/08_101_7__mod.jpg)  

- 08_103_0  
![](img/08_103_0.jpg) 
![](img/08_103_0__mod.jpg)  

- 08_104_2  
![](img/08_104_2.jpg) 
![](img/08_104_2__mod.jpg)  

- 08_106_4  
![](img/08_106_4.jpg) 
![](img/08_106_4__mod.jpg)  

- 08_110_2  
![](img/08_110_2.jpg) 
![](img/08_110_2__mod.jpg)  

- 08_110_4  
![](img/08_110_4.jpg) 
![](img/08_110_4__mod.jpg)  

- 08_113_1  
![](img/08_113_1.jpg) 
![](img/08_113_1__mod.jpg)  

- 08_116_0  
![](img/08_116_0.jpg) 
![](img/08_116_0__mod.jpg)  

- 08_116_4  
![](img/08_116_4.jpg) 
![](img/08_116_4__mod.jpg)  

- 08_119_3  
![](img/08_119_3.jpg) 
![](img/08_119_3__mod.jpg)  

- 08_119_4  
![](img/08_119_4.jpg) 
![](img/08_119_4__mod.jpg)  

- 08_119_6  
![](img/08_119_6.jpg) 
![](img/08_119_6__mod.jpg)  

- 08_120_2  
![](img/08_120_2.jpg) 
![](img/08_120_2__mod.jpg)  

- 08_120_5  
![](img/08_120_5.jpg) 
![](img/08_120_5__mod.jpg)  

- 08_127_1  
![](img/08_127_1.jpg) 
![](img/08_127_1__mod.jpg)  

- 08_134_0  
![](img/08_134_0.jpg) 
![](img/08_134_0__mod.jpg)  

- 08_135_3  
![](img/08_135_3.jpg) 
![](img/08_135_3__mod.jpg)  

- 08_137_1  
![](img/08_137_1.jpg) 
![](img/08_137_1__mod.jpg)  

- 08_138_4  
![](img/08_138_4.jpg) 
![](img/08_138_4__mod.jpg)  

- 08_140_6  
![](img/08_140_6.jpg) 
![](img/08_140_6__mod.jpg)  

- 08_143_0  
![](img/08_143_0.jpg) 
![](img/08_143_0__mod.jpg)  

- 08_146_4  
![](img/08_146_4.jpg) 
![](img/08_146_4__mod.jpg)  

- 08_149_7  
![](img/08_149_7.jpg) 
![](img/08_149_7__mod.jpg)  

- 08_151_2  
![](img/08_151_2.jpg) 
![](img/08_151_2__mod.jpg)  

- 08_152_1  
![](img/08_152_1.jpg) 
![](img/08_152_1__mod.jpg)  

- 08_152_7  
![](img/08_152_7.jpg) 
![](img/08_152_7__mod.jpg)  

- 08_153_5  
![](img/08_153_5.jpg) 
![](img/08_153_5__mod.jpg)  

- 08_154_1  
![](img/08_154_1.jpg) 
![](img/08_154_1__mod.jpg)  

- 08_154_3  
![](img/08_154_3.jpg) 
![](img/08_154_3__mod.jpg)  

- 08_154_7  
![](img/08_154_7.jpg) 
![](img/08_154_7__mod.jpg)  

- 08_157_0  
![](img/08_157_0.jpg) 
![](img/08_157_0__mod.jpg)  

- 08_157_1  
![](img/08_157_1.jpg) 
![](img/08_157_1__mod.jpg)  

- 08_157_2  
![](img/08_157_2.jpg) 
![](img/08_157_2__mod.jpg)  

- 08_157_2_2  
![](img/08_157_2_2.jpg)  

- 08_157_6  
![](img/08_157_6.jpg) 
![](img/08_157_6__mod.jpg)  

- 08_159_0  
![](img/08_159_0.jpg) 
![](img/08_159_0__mod.jpg)  

- 08_159_3  
![](img/08_159_3.jpg) 
![](img/08_159_3__mod.jpg)  

- 08_159_7  
![](img/08_159_7.jpg) 
![](img/08_159_7__mod.jpg)  

- 08_160_5  
![](img/08_160_5.jpg) 
![](img/08_160_5__mod.jpg)  

- 08_162_0  
![](img/08_162_0.jpg) 
![](img/08_162_0__mod.jpg)  

- 08_162_3  
![](img/08_162_3.jpg) 
![](img/08_162_3__mod.jpg)  

- 08_162_5  
![](img/08_162_5.jpg) 
![](img/08_162_5__mod.jpg)  

- 08_163_1  
![](img/08_163_1.jpg) 
![](img/08_163_1__mod.jpg)  

- 08_164_5  
![](img/08_164_5.jpg) 
![](img/08_164_5__mod.jpg)  

- 08_166_5  
![](img/08_166_5.jpg) 
![](img/08_166_5__mod.jpg)  


- 08_169_1  
![](img/08_169_1.jpg) 
![](img/08_169_1__mod.jpg)  

- 08_169_4  
![](img/08_169_4.jpg) 
![](img/08_169_4__mod.jpg)  

- 08_174_2  
![](img/08_174_2.jpg) 
![](img/08_174_2__mod.jpg)  

- 08_174_6  
![](img/08_174_6.jpg) 
![](img/08_174_6__mod.jpg)  

- 08_175_2  
![](img/08_175_2.jpg) 
![](img/08_175_2__mod.jpg)  

- 08_175_5  
![](img/08_175_5.jpg) 
![](img/08_175_5__mod.jpg)  

- 08_176_0  
![](img/08_176_0.jpg) 
![](img/08_176_0__mod.jpg)  

- 08_176_5  
![](img/08_176_5.jpg) 
![](img/08_176_5__mod.jpg)  


- 08_177_4  
![](img/08_177_4.jpg) 
![](img/08_177_4__mod.jpg)  

- 08_177_6  
![](img/08_177_6.jpg) 
![](img/08_177_6__mod.jpg)  

- 08_178_0  
![](img/08_178_0.jpg) 
![](img/08_178_0__mod.jpg)  

- 08_178_4  
![](img/08_178_4.jpg) 
![](img/08_178_4__mod.jpg)  

- 08_179_3  
![](img/08_179_3.jpg) 
![](img/08_179_3__mod.jpg)  

- 08_179_6  
![](img/08_179_6.jpg) 
![](img/08_179_6__mod.jpg)  

- 08_180_0  
![](img/08_180_0.jpg) 
![](img/08_180_0__mod.jpg)  

- 08_180_4  
![](img/08_180_4.jpg) 
![](img/08_180_4__mod.jpg)  

- 08_180_7  
![](img/08_180_7.jpg) 
![](img/08_180_7__mod.jpg)  

- 08_181_1  
![](img/08_181_1.jpg) 
![](img/08_181_1__mod.jpg)  

- 08_181_5  
![](img/08_181_5.jpg) 
![](img/08_181_5__mod.jpg)  

- 08_182_1  
![](img/08_182_1.jpg) 
![](img/08_182_1__mod.jpg)  

- 08_183_0  
![](img/08_183_0.jpg) 
![](img/08_183_0__mod.jpg)  

- 08_183_3  
![](img/08_183_3.jpg) 
![](img/08_183_3__mod.jpg)  

- 08_183_5  
![](img/08_183_5.jpg) 
![](img/08_183_5__mod.jpg)  

- 08_184_0  
![](img/08_184_0.jpg) 
![](img/08_184_0__mod.jpg)  

- 08_186_1  
![](img/08_186_1.jpg) 
![](img/08_186_1__mod.jpg)  

- 08_186_3  
![](img/08_186_3.jpg) 
![](img/08_186_3__mod.jpg)  

- 08_187_0  
![](img/08_187_0.jpg) 
![](img/08_187_0__mod.jpg)  

- 08_188_1  
![](img/08_188_1.jpg) 
![](img/08_188_1__mod.jpg)  

- 08_188_5  
![](img/08_188_5.jpg) 
![](img/08_188_5__mod.jpg)  

- 08_189_1  
![](img/08_189_1.jpg) 
![](img/08_189_1__mod.jpg)  

- 08_190_6  
![](img/08_190_6.jpg) 
![](img/08_190_6__mod.jpg)  

- 08_191_2  
![](img/08_191_2.jpg) 
![](img/08_191_2__mod.jpg)  

- 08_192_1  
![](img/08_192_1.jpg) 
![](img/08_192_1__mod.jpg)  

- 08_197_0  
![](img/08_197_0.jpg) 
![](img/08_197_0__mod.jpg) 


- 吃  
![](img/08_037_3__eat.jpg) 
![](img/08_062_3__eat.jpg) 
![](img/08_063_1__eat.jpg)  

- 抽烟  
![](img/08_167_6.jpg)  




---------------------------------------


- 08_176_0, 紫苑坐在屋顶上。把臀部和屋顶的接触部分画的平一些，增加一些压力感，对比一下(感觉左小腿和臀部不在一个竖平面上，奇怪)：  
![](img/08_176_0__crop.jpg) 
![](img/08_176_0__crop_press.jpg)  


- 09_020_7, 雅彦躺着，因为重力，头发的形态和身体直立时的不同。图片旋转90度更易看出。  
![](img/09_020_7__.jpg) 
![](img/09_020_7__rotate.jpg)  


- 09_065_2, 紫苑在报纸上看到棒球新星竟然是矮子松下，很惊讶。紫苑的姿势和报纸平行(中间图里蓝线所示)。紫苑身体的弯曲，表示她低头看报纸的惊讶；报纸的弯曲程度，可以体现紫苑猛地抓过报纸时报纸的惯性。报纸越弯曲，体现出手抓的速度越快(右图所示)：    
![](img/09_065_2__mod.jpg) 
![](img/09_065_2__mod_mark.jpg) 
![](img/09_065_2__mod_bigger-surprise.jpg)   


- 09_073_3, 09_078_5, 左图是回忆里松下和紫苑的比试。右图是现在的比试，松下左腿几乎是直线，凸显力度；同时身体俯得更低，说明力度更大、更拼命，这符合他的心思--想战胜紫苑。  
![](img/09_073_3__mod.jpg) 
![](img/09_078_5__mod.jpg)  


- 09_073_5，紫苑击球的动作，身体的曲线、衣服褶皱的曲线很生动(如右图曲线示意):  
![](img/09_073_5__mod.jpg) 
![](img/09_073_5__mod_mark.jpg)  


- 10_019_2, 早纪手拿烟的姿势很形象。  
![](../fc_details/img/10_019_2__.jpg)  


- 13_064_2, 13_064_3. 空说起早年当漫画家的经历，紫说当时很担心。两人随后相视一笑：   
![](img/13_064_2__mod.jpg)  
![](img/13_064_3__.jpg)  
对比前后两个镜头里的动作(第一个镜头为红色)：  
![](img/13_064_2_3__mod.jpg)  


- 14_131_4, 14_131_6. 紫苑为了确认浅葱的性取向，让她吻自己。  
![](img/14_131_4__mod.jpg)  
![](img/14_131_6__.jpg)  
对比前后两个镜头里的动作(第一个镜头为红色)：  
![](img/14_131_4_6__mod.jpg)  


- 14_234_4, 14_234_6. (影研社拍戏时)雅美让浅葱吻自己。  
![](img/14_234_4__mod.jpg)  
![](img/14_234_6__.jpg)  
对比前后两个镜头里的动作(第一个镜头为红色)：  
![](img/14_234_4_6__mod.jpg)   






**参考链接**：  
1. [FC的绘画 - 透视](../fc_drawing_style__perspective/readme.md)  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



