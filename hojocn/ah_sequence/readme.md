
**AH的分镜**  

----------------------------------------------------------

- **Vol09	CH093	page077		比恋爱更深的感情**  		 

    - 獠在夏目芳树的个展看到了香的画像。这部分剧情的镜头统计：  
        [fc_shot_emotion_vol14_ch102.pdf](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/ah_sequence/shot_emotion/ah_shot_emotion_vol09_ch093.pdf)  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



