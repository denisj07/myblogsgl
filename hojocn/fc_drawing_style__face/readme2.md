
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)  


- **说明**：  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - [原链接](./readme2.md)  
    - 更多角色的比较，可参见[文件](project2/face_front_cmp_aps.xcf.7z)  
    - 温馨提示：**本文图片里人脸上有很多点，可能会令人不适，可能会产生密集恐惧**。  


# FC的画风-面部2(脸颊)  

- **动机**：  
在[0]里比较了FC里某些角色的面部。其中比较角色面部特征时，经常出现类似下列的描述："前者左下颌骨末端稍窄于后者", "腮帮子略微凸", "(脸部线条)更有棱角", "前者脸颊窄于后者"。本文尝试借助美容整形外科(Aesthetic Plastic Surgery，以下简称APS)领域的一些方法和术语来描述这些差异，试图让这些描述更精确。  

参考资料[1]里在人物面部正面和侧面定义了一些mark点，借助点、线之间的关系(距离、角度)来研究"美女脸的特征"。虽然本文只整理FC的角色的正面，但这里罗列了[1]里关于正面和侧面的mark点和术语，目的是方便查找和比较这些mark点。  

- **面部mark点-正面**：  
![](img2/DifferencesBetweenCaucasianAndAsianAttractiveFaces.Fig3.png)    
图1(FIGURE 3 in [1])  

图1中的mark点的注解和中文翻译:  
    - 1 and 2: pupil (p), （译注：瞳孔）  
    - 3: trichion (tr), （译注：发际中点）  
    - 4: glabella (g), （译注：眉间点）  
    - 5: sellion (se), （译注：鼻梁点）  
    - 6 and 7: alare (al), （译注：鼻翼点）  
    - 8: subnasale (sn), （译注：鼻下点）  
    - 9: labiale superius (ls), （译注：上唇中点）  
    - 10: stomion (sto), （译注：口裂点）  
    - 11: labiale inferius (li), （译注：下唇中点）  
    - 12 and 13: cheilion (ch), （译注：口角点）  
    - 14: menton (m), （译注：颏下点/颔下点） (gnathion (gn)[3])  
    - 15 and 16: endocanthion (en), （译注：眼内角点）  
    - 17 and 18: palpebrale superius (ps),  （译注：上眼睑点？）
    - 19 and 20: palpebrale inferius (pi),  （译注：下眼睑点？）
    - 21 and 22: exocanthion (ex), （译注：眼外角点？）   
    - 23 and 24: highest point of eyebrow (Hb), （译注：眉高点？）  
    - 25 and 26: most medial point of eyebrow (Mb), （译注：眉内侧点？）  
    - 27 and 28: zygion (zy), （译注：颧点）    
    - 29 and 30: points of mandible angle (ang)（译注：下颌骨角点）, that is, points of the outline of the mandible that meet with the horizontally extending line of the two cheilions（译注：口角）.("口角点的水平连线"与脸颊的交点)  
    - 31 and 32: lateral gonion (latgo)（译注：下颌骨后下点） tangential points with the outline of the mandible; the tangential line should be parallel to the ipsilateral（译注：同侧的） line of gn-­zy,  (详见[3] Figure 1 )(注：该点描述脸颊(腮帮子)的凸起程度，详见"使用的方法和遇到的困难"一节里"标记31/32点"。)  
    - 33 and 34: points of the outline of both cheeks (chk)（译注：脸颊）, which meet the transverse extending line of the subnasale, ("鼻下点的水平线"与脸颊的交点)  
    - 35 and 36: the points of the chin（译注：下巴/颏） contour line at the anterior-­lateral chin, which meet the vertical extending line from each pupil. (al-­chin);  ("瞳孔铅垂线"与脸颊的交点)  
    - Except for 27 and 28, the facial points designated on facial contour lines 23 to 36 were not usually defined as verified neoclassical canon; only I defined the landmarks. The arbitrary points help us to understand the actual differences in face shape between Caucasians and Asians  


- **面部mark点-侧面**：  
![](img2/DifferencesBetweenCaucasianAndAsianAttractiveFaces.Fig4.png)    
图2(FIGURE4 in [1])   

图2中的mark点的注解和中文翻译:  
    - Tragion (t) （译注：耳屏点）: the most anterior portion of the supratragal notch),  
    - glabella (g) （译注：眉间点）: the most prominent or anterior point of the forehead between the eyebrows,  
    - sellion (se) （译注：鼻梁点）: the most concave point in the tissue overlying the area of the frontonasal suture,  
    - pronasale (prn) （译注：鼻尖）: the most prominent or anterior projection point of the nose,  
    - columella breakpoint (c) （译注：(鼻)小柱断点） the highest point of the columella or breakpoint of Daniel,  
    - subnasale (sn) （译注：鼻下点）: the junctional point of the columella and the upper cutaneous lip, alar curvature point or alar  
    crest point (ac) （译注：波峰点）: the most lateral point in the curved base line of each ala, indicating the facial insertion of the nasal wing base, 鼻翼根与面部连接处的曲线的最外侧点  
    - labiale superius (ls) （译注：上唇中点）: the mucocutaneous junction and vermilion border of the upper lip,  
    - labiale inferius (li) （译注：下唇中点）: the mucocutaneous junction and vermilion border of the lower lip,  
    - cheilion (ch), （译注：口角点）  
    - pogonion (pg) （译注：颏前点）: the most anterior point of the soft-­tissue chin,  
    - distant chin (dc): the farthest point from the fiducial t. 下巴上距离t点最远的点。  
    - menton (m): the most inferior portion of the anterior chin. Points m1, m2, m3, and dc were not usually defined in articles; only I defined the landmarks.  （译注：颏下点/颔下点） (gnathion (gn)[3])。  
        The arbitrary points help us to understand the actual differences in the profile faces of Caucasians and Asians.  
        - m1: the point where the mandibular（译注：下颌骨） contour line meets the horizontal line that extends from the fiducial ls,  
        - m2: the intersecting line of the mandibular contour with the horizontally extended line from the fiducial li;  
        - m3: the intersecting line of the mandibular contour with the horizontally extended line from the fiducial pg;  
    

脸颊的形状一般指：从正面看，下颌骨外轮廓的形状和颧骨的形状。依照上面的mark点，正面脸颊的形状指"27/28, 29/30, 31/32, 33/34, 35/36, 14"这些点的位置。  


- **本文使用的方法、遇到的困难**  
    - 标记31/32点（右脸latgo/左脸latgo）。31/32点的定义是"latgo点处的切线与gn-­zy平行"。因为脸颊向外凸，所以该定义等同于"latgo点距离线段gn-­zy最远"，即用latgo点描述脸颊(腮帮子)的凸起程度。之所以用平行线的方式来定义latgo点，我猜，可能是因为用平行线的方法更容易找到latgo点。本文标记latgo点的方法如下(如下图动画所示)：  
        - 以右脸颊为例。初始时，取一直线(蓝色)经过gn点和zy点；  
        - 向脸颊外侧水平移动该直线，则该直线与脸颊的两个交点(绿色)之间的距离会越来越近；  
        - 当这两个绿色交点重合为一个点时，该点即为"(右脸颊)距离线段gn-­zy最远的点"，即为所求的latgo点。  
        ![](img2/face_front_cmp_aps_mark31-32-latgo.gif)  

    - 有多种方法对比不同角色的面部差异：  
        - 方法1. 动态渐变。使用gif动画，把一个角色的面部图片逐渐过渡到另一个角色的面部图片。详见[0];  
        - 方法2. 一个角色的面部图片做成红色半透明，另一个角色的面部图片做成蓝色半透明；两个图片叠在两个图层，作对比。详见[5];  
        - 方法3. 在某些面部特征的位置定义一些mark点。比较不同面部的mark点的差异。  
      本文使用方法3。方法3的优缺点：  
        - 优点：  
            - 能明显地展现出面部的差异;  
            - 因为定义了很多mark点，所以它们联合起来能反映出面部特征。  
        - 缺点：  
            - 由于需要标记mark点，所以，如果标记的位置不准确，则直接影响后续对比结果和结论。  

    - 尝试回答以下问题：  
        - 1.如果配角A和主角B的相貌很相似，则有理由认为作者依据B的人设相貌去设计A的相貌；  
        - 2.对于FC里有些角色的正面相，其左右脸明显不对称。这能说明什么问题？哪些角色是这样？能否概括出规律？  
        - 3.如果某两个角色有血缘关系，ta们面部是否相似？  

    - 对比的内容：  
        - 对于问题1，对比不同角色的面部差异。  
        - 对于问题2，对于某角色，对比其左右脸的差异。  
        - 对于问题3，如果某两个角色有血缘关系，对比ta们的面部差异。  

    - 遇到的困难：  
        - 标记mark点时，难免有误差。尤其是"31/32点（右脸latgo/左脸latgo）"。  
        - 对比角色A和角色B的面部特征时，要把A、B的头部放缩至同样大小，然后叠放在不同图层，最后对齐A和B的五官。难点在于：  
            - 1.角色A、B的头部放缩后，大小可能不一致；  
            - 2.对齐五官时，最先对齐哪里（即，以哪个位置为基准）？比如，首先对齐“左右眼的瞳孔”、或首先对齐“两侧脸颊最大宽度(颧点zy)”、或首先对齐“左右外眼角”，等等)。很常见的困难是，对齐某个五官后，发现另外几个五官对不齐，比如：1)水平方向对齐鼻子后，浩美front组的脸(相对于雅彦group2/紫苑group2)向图片右侧偏(详见"浩美"一节); 2)对齐瞳孔后，发现两侧脸颊对不齐；3)对齐两侧脸颊后，发现鼻子对不齐(左右脸不对称?)，等等;   

    - FC里的角色面部很少画鼻翼，所以，鼻翼处的mark点(6/7)可忽略。  


- **紫苑(Shion)-正面**  
搜集了FC里紫苑的正面图片。整理了其中一部分后，发现可以分为以下几组。这些组和[0]里的组一致，但依据重要性重新排列了顺序。这对于以下其他角色也是一样。  
    - Group2(front):  
        - 图片：03_094_2, 04_015_5, 06_065_0, 07_097_0, 07_188_0, 08_096_2, 08_165_4, 08_176_6, 09_083_2.  
        - 特点：正面图。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__shion-group2_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和雅彦Group2(红色mark点)的对比（紫苑嘴角窄(FC里女性的嘴角窄于男性); 31/32点比雅彦低，反映出紫苑腮帮子处的线条更圆滑）；  
          （下图左三）左一叠加左二；  
          （下图左四）雅彦Group2；  
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group2_vs_masahiko-group2.jpg) 
        ![](img2/face_front_cmp_aps__shion-group2-p_vs_masahiko-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、若苗空front(蓝色mark点)、若苗紫front(红色mark点)的对比(**血缘关系**)（紫苑嘴小（紫的嘴是按男性比例）；紫苑31/32点相对靠下，说明其腮帮子处的线条相对更圆滑（即，其父母(成年人)腮帮子处的线条相对更直））；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front；  
          （下图左五）若苗紫front；  
          （下图左六）若苗空front渐变对比；  
          （下图左七）若苗紫front渐变对比；  
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group2_vs_yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group2-p_vs_yukari-front_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_sora_front_to_shion_front.gif) 
        ![](img/face_front_yukari_front_to_shion_front.gif) 

    - Group0:  
        - 图片：01_042_0，02_001，01_047_0，04_064_6。  
        - 特点：相对其他组图片，这组图片脸稍短(或稍宽)、显得角色年龄小。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__shion-group0_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和Group2(红色mark点)的对比（本组颏下点/m/gn高，说明脸短，显得年龄小）；  
          （下图左三）左一叠加左二;  
          （下图左四）Group2;  
        ![](img/face_front_shion_group0-young.jpg) 
        ![](img2/face_front_cmp_aps__shion-group0_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__shion-group0-p_vs_shion-group2.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、若苗空front(蓝色mark点)、若苗紫front(红色mark点)的对比(血缘关系)；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front；  
          （下图左五）若苗紫front；  
        ![](img/face_front_shion_group0-young.jpg) 
        ![](img2/face_front_cmp_aps__shion-group0_vs_yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group0-p_vs_yukari-front_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

    - Group3:  
        - 图片：11_043_7, 11_133_2, 14_122_0, 14_280_6, 14_282_1.  
        - 特点：虽然是正面图，但左脸比右脸稍宽一点点（介于Group1和Group2之间）。下图对比左右脸mark点，可以反映出这一点。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)。不是严格的正面：  
        ![](img2/face_front_cmp_aps__shion-group3_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(红色mark点)和Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二;  
          （下图左四）Group2;  
        ![](img/face_front_shion_group3-slight-left.jpg) 
        ![](img2/face_front_cmp_aps__shion-group3_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__shion-group3-p_vs_shion-group2.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、若苗空front(蓝色mark点)、若苗紫front(红色mark点)的对比(血缘关系)；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front；  
          （下图左五）若苗紫front；  
          （下图左六）若苗空front渐变对比；  
          （下图左七）若苗紫front渐变对比；  
        ![](img/face_front_shion_group3-slight-left.jpg) 
        ![](img2/face_front_cmp_aps__shion-group3_vs_yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group3-p_vs_yukari-front_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_sora_front_to_shion_front-slight-left.gif)
        ![](img/face_front_yukari_front_to_shion_front-slight-left.gif)


- **雅彦(Masahiko)-正面**  
搜集了FC里雅彦的正面图片。整理了其中一部分后，发现可以分为几组。这些组和[0]里的组一致，但依据重要性重新排列了顺序。这对于以下其他角色也是一样。  
    - Group2:  
        - 图片：03_131_4，08_109_3，11_129_3，13_116_3，13_116_4，13_118_6，13_139_1，13_157_0，14_149_5，14_283_4。  
        - 特点：正面图。两侧腮帮子稍凸(婴儿肥?)。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__masahiko-group2_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和紫苑Group2(红色mark点)的对比（雅彦嘴角宽(FC里女性的嘴角窄于男性)）；  
          （下图左三）左一叠加左二：  
          （下图左四）紫苑Group2;  
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group2-p_vs_shion-group2.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和若苗紫front(红色mark点)的对比（**血缘关系**）（两人嘴横向、竖向长度一致(这说明**若苗紫的嘴宽符合男性比例**);两人右脸一致；若苗紫左脸宽；若苗紫眼睛更修长(成年人的特征)。两人脸颊有不少相似之处，符合血缘关系的设定。）；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗紫front；  
          （下图左五）若苗紫front渐变对比；  
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group2_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group2-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_yukari_front_to_masahiko_group2.gif) 

        - 导演front对比雅彦group2（见“导演front”一节）  
        - 早纪front对比雅彦group2（见“早纪front”一节）  
        - 横田进03_012_5和雅彦group2（见“横田进03_012_5”一节）  
        - 仁科对比雅彦group2，参见“仁科”一节。  
        - 文哉front和雅彦group2，参见“文哉”一节。  
        - 松下敏史09_077_5和雅彦group2，参见“松下敏史”一节。  
        - 古屋公弘09_051_6和雅彦group2，参见“古屋公弘”一节。  

    - Group0:  
        - 图片：01_023_4，01_081_5，02_063_4。  
        - 特点：相对其他组图片，这组图片脸稍短、显得角色年龄小。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__masahiko-group0_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和Group2(红色mark点)的对比（本组颏下点/m/gn高，说明脸短，显得年龄小；嘴角宽度窄，符合年龄小的设定）；  
          （下图左三）左一叠加左二：  
          （下图左四）动态渐变对比；  
        ![](img/face_front_masahiko_group0_young.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group0_vs_masahiko-group2.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group0-p_vs_masahiko-group2.jpg) 
        ![](img/face_front_masahiko_group0_to_group2.gif) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和若苗紫front(红色mark点)的对比（血缘关系）；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗紫front；  
          （下图左五）若苗紫front渐变对比；  
        ![](img/face_front_masahiko_group0_young.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group0_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group0-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_yukari_front_to_masahiko_group0.gif) 


- **塩谷/盐谷(Shionoya)(紫苑男装)-正面**  
搜集了FC里其正面图片。整理了其中一部分后，发现可以分为几组：  
    - psudo-front：  
        - 图片：13_167_1，13_174_0，14_010_5。  
        - 特点：虽然是正面图，但左脸比右脸稍宽一点点。（与紫苑Group3符合）  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__shya-psudo-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组脸颊、鼻子、嘴和紫苑一致，说明本组是女性的画法）；  
          （下图左三）左一叠加左二：  
          （下图左三）雅彦Group2；  
          （下图左三）紫苑Group2；  
        ![](img/face_front_shya_psudo-front.jpg) 
        ![](img2/face_front_cmp_aps__shya-psudo-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__shya-psudo-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、若苗空front(蓝色mark点)、若苗紫front(红色mark点)的对比(血缘关系)；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front；  
          （下图左五）若苗紫front；  
        ![](img/face_front_shya_psudo-front.jpg) 
        ![](img2/face_front_cmp_aps__shya-psudo-front_vs_yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__shya-psudo-front-p_vs_yukari-front_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

    - right-top：  
        - 图片：10_125_0，12_150_4，12_151_4，12_158_6，14_067_4。  
        - 特点：虽然是正面图，但右脸比左脸稍宽（角色稍微向左转头的效果）、略微低头、左嘴角上扬。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__shya-right-top_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(本组嘴角有笑容，所以嘴角宽度变大)；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_shya_right-top.jpg) 
        ![](img2/face_front_cmp_aps__shya-right-top_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__shya-right-top-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、若苗空front(蓝色mark点)、若苗紫front(红色mark点)的对比(**血缘关系**)；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front；  
          （下图左五）若苗紫front；  
        ![](img/face_front_shya_right-top.jpg) 
        ![](img2/face_front_cmp_aps__shya-right-top_vs_yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__shya-right-top-p_vs_yukari-front_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front.jpg) 


- **雅美(Masami)-正面**  
搜集了FC里雅美的正面图片。整理了其中一部分后，发现可以分为几类：  
    - Group0:  
        - 图片：03_027_1，03_027_3(镜中)，03_060_1。  
        - 特点：相对其他组图片，这组图片脸稍上扬、脸稍短。雅彦第一次异装。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__masami-group0_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴宽更接近男性。疑问:脸颊处的黑、蓝、红点间隔出现，这能说明什么问题？）；  
          （下图左三）左一叠加左二；  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_masami_group0_vol03.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和若苗紫front(红色mark点)的对比（**血缘关系**）；  
          （下图左三）左一叠加左二；  
          （下图左四）若苗紫front；  
        ![](img/face_front_masami_group0_vol03.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和group1(红色mark点)的对比(嘴大小一致)；  
          （下图左三）左一叠加左二；  
          （下图左四）Group1；  
          （下图左五）渐变对比Group1(脸型相似，Group0的鼻子稍上移(上扬))；  
        ![](img/face_front_masami_group0_vol03.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img/face_front_masami_group0_to_group1.gif) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和雅彦group0(红色mark点)的对比(两组的共同点是相貌年龄小)；  
          （下图左三）左一叠加左二；  
          （下图左四）雅彦group0的渐变对比；  
        ![](img/face_front_masami_group0_vol03.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0_vs_masahiko-group0.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0-p_vs_masahiko-group0.jpg) 
        ![](img/face_front_masahiko_group0_to_masami_group0.gif) 

    - Group1:  
        - 图片：03_114_0，07_048_0。  
        - 特点：正面。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)。**左右脸颊有几处差异**。  
        ![](img2/face_front_cmp_aps__masami-group1_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2的渐变对比(这说明后续雅彦的脸型偏瘦长、后续雅美的脸型偏短宽)；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img2/face_front_cmp_aps__masami-group1_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__masami-group1-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_to_masami_group1.gif) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和若苗紫front(红色mark点)的对比（**血缘关系**）；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗紫front；  
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img2/face_front_cmp_aps__masami-group1_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__masami-group1-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 


- **若苗空(Sora)-正面**  
搜集了FC里其正面图片。整理了其中一部分后，发现可以分为几类：  
    - front:  
        - 图片：01_00a_0，02_000a，04_015_0，04_039_0，05_059_0，06_135_2。  
        - 特点：正面。虽然左右脸不对称，但人中、嘴唇中点位于"两耳外侧间距的中线"上(如下图所示)，说明这是严格的正面。在本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节里归纳出了该画法的规律。  
        ![](img/face_asymmetry_sora-front_with_markline.jpg) 
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__sora-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
          （下图左六）紫苑Group2渐变对比；  
        ![](img/face_front_sora_front.jpg) 
        ![](img2/face_front_cmp_aps__sora-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__sora-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img/face_front_sora_front_to_shion_front.gif) 

        - 紫苑对比若苗空front（血缘关系），参见“紫苑group0”、“紫苑group2”、“紫苑group3”节。  
        - 盐谷对比若苗空front（血缘关系），参见“盐谷psudo-front”、“盐谷top-short”节。  
        - 若苗紫front对比若苗空front，参见“若苗紫front”一节。  
        - 森front对比空front，参见“森front”一节。  
        - 葵front对比若苗空front，参见“葵front”一节。  
 

- **若苗紫(Yukari)-正面**  
搜集了FC里其（包括雅彦妈妈）正面图片。整理了其中一部分后，发现可以分为几类：  
    - front:  
        - 图片：01_005_0，01_025_4，01_043_0，01_200_4，02_004_4，02_146_4，03_087_0，04_000a_0，04_180_1，05_042_1，05_092_2，05_183_6，06_092_0，06_092_6，06_135_2，07_059_3，08_017_5，09_099_2，09_155_5，09_155_6，10_020_4，10_035_3，10_055_4，10_057_0。
        - 特点：正面。相对于该角色的其他组，本组图片在数量上占绝对优势，说明若苗紫的正面画法稳定。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__yukari-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果（因彩图不易比较，故不包含彩图）；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）雅彦Group2渐变对比；  
          （下图左六）紫苑Group2；  
          （下图左七）紫苑Group2渐变对比；  
        ![](img/face_front_yukari_front.jpg) 
        ![](img2/face_front_cmp_aps__yukari-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__yukari-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_yukari_front_to_masahiko_group2.gif) 
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img/face_front_yukari_front_to_shion_front.gif) 

        - （下图左一）这些图片叠加后的效果（因彩图不易比较，故不包含彩图）；  
          （下图左二）本组(黑色mark点)、若苗空front(红色mark点)的对比(**脸颊几乎一致（夫妻相?）**。35/36点不一致，说明若苗空瞳距稍小；27/28点不一致，说明若苗紫颧骨稍宽)；   
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front；  
          （下图左五）若苗空front渐变对比；  
        ![](img/face_front_yukari_front.jpg) 
        ![](img2/face_front_cmp_aps__yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__yukari-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front_to_sora_front.gif) 

        - 紫苑对比若苗紫front（血缘关系），参见“紫苑group0”、“紫苑group2”、“紫苑group3”节。  
        - 盐谷对比若苗紫front（血缘关系），参见“盐谷psudo-front”、“盐谷top-short”节。  
        - 雅彦对比若苗紫front（血缘关系），参见“雅彦group0”、“雅彦group2”节。  
        - 雅美对比若苗紫front（血缘关系），参见“雅美group0”、“雅美group1”节。  
        - 早纪front对比紫front（参见“早纪front”一节）  


- **浅冈叶子(Yoko)-正面**  
分为几类：  
    - front(正面。叶子的腮帮子稍凸(婴儿肥?))：
        - 13_178_5，13_172_0，13_154_2，13_142_1，08_160_3，08_153_3，08_152_6，06_007_3_，05_160_2，05_142_1，05_097_5，04_178_0，04_136_3，03_041_2  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__yoko-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
          （下图左六）雅彦Group2渐变对比；  
          （下图左7）紫苑Group2渐变对比；  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img/face_front_masahiko_group2_front_to_yoko_front.gif) 
        ![](img/face_front_shion_group2-front_to_yoko_front.gif) 

        - 叶子和叶子母13_103_3的对比（血缘关系），参见“叶子母”一节。  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和**雅美Group0**(红色mark点)的对比(两者脸型很相似，叶子front下半脸(尤其是左脸)微瘦、下巴稍尖)；  
          （下图左三）左一叠加左二：  
          （下图左四）雅美Group0;  
          （下图左五）雅美Group0渐变对比;  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_masami-group0.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_masami-group0.jpg) 
        ![](img/face_front_masami_group0_vol03.jpg) 
        ![](img/face_front_masami_group0_vol03_to_yoko_front.gif) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和**盐谷psudo-front**(蓝色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）盐谷psudo-front;  
          （下图左五）盐谷psudo-front渐变对比;  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_shya-psudo-front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_shya-psudo-front.jpg) 
        ![](img/face_front_shya_psudo-front.jpg) 
        ![](img/face_front_shya_psudo-front_to_yoko_front.gif) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和**若苗空front**(蓝色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front;  
          （下图左五）若苗空front渐变对比;  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_sora_front_to_yoko_front.gif) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和**若苗紫front**(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗紫front;  
          （下图左五）若苗紫front渐变对比;  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_yukari_front_to_yoko_front.gif) 


- **浩美(Hiromi)-正面**  
    - front  
        - 图片：09_042_4，05_075_1，03_008_3，01_191_5，01_187_4。  
        - 这些图片叠加后的效果：  
        ![](img/face_front_hiromi_front.jpg)  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__hiromi-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（(对齐鼻子后)浩美的脸向图片右侧偏）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_hiromi_right.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - right  
        - 图片：09_047_4，07_106_1，05_130_2。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__hiromi-right_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_hiromi_right.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-right_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-right-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和**叶子front**(红色mark点)的对比(脸型很类似，叶子腮帮子稍凸)；  
          （下图左三）左一叠加左二：  
          （下图左四）叶子front；  
          （下图左五）叶子front的渐变对比；  
        ![](img/face_front_hiromi_right.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-right_vs_yoko-front.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-right-p_vs_yoko-front.jpg) 
        ![](img/face_front_yoko_front.jpg) 
        ![](img/face_front_yoko_front_vs_hiromi_right.gif) 

        - 早纪front对比浩美right，参见“早纪front”一节。  


- **熏(Kaoru)-正面** {#熏(Kaoru)-正面}  
    - front {#熏(Kaoru)-正面-front}  
        - 图片：08_013_4，07_193_5。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__kaoru-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_kaoru_front.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - 熏front和早纪front、早纪top-short的对比（血缘关系），见“早纪“一节。  

        - 熏front和辰巳front、辰巳left的对比（血缘关系），见“辰巳”一节。  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和**浩美right**(红色mark点)的对比(脸型类似，熏front脸颊更有棱角、右下颌骨末端偏下)；  
          （下图左三）左一叠加左二：  
          （下图左四）浩美right;  
          （下图左四）浩美right渐变对比；  
        ![](img/face_front_kaoru_front.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 
        ![](img/face_front_hiromi_front_vs_kaoru_front.gif) 

        - 江岛wide和熏front的脸型有些类似。两者的对比参见"江岛wide"一节。    

        - 麻衣front对比熏front，参见“麻衣front”一节。  


- **江岛(Ejima)-正面**  
    - wide(脸庞稍宽)  
        - 图片：14_025_1，05_012_0。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__ejima-wide_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（江岛的下颌窄（下颌骨末端稍高））；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_ejima_wide.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和**熏front**(红色mark点)的对比(脸型类似，江岛的下颌窄、下颌骨末端稍高)；  
          （下图左三）左一叠加左二：  
          （下图左四）熏front;  
        ![](img/face_front_ejima_wide.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)和**空front**(红色mark点)的对比（江岛的下颌窄）；  
          （下图左三）左一叠加左二：  
          （下图左四）空front;  
        ![](img/face_front_ejima_wide.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

        - 江岛wide和熏front的脸型有些类似，前者下颌骨末端稍高。对比发现，江岛(蓝色)的29/ang点比熏(红色)的点高：  
        ![](img2/face_front_ejima_wide_vs_kaoru_front.jpg)  


- **辰巳(Tatsumi)-正面**  
    - front：  
        - 图片：09_134_6，09_110_1，04_139_4，03_125_5。  
        - 特点：正面。虽然左右脸不对称，但人中、嘴唇中点位于"两耳外侧间距的中线"上(如下图所示)，说明这是严格的正面。在本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节里归纳出了该画法的规律。    
        ![](img/face_asymmetry_tatsumi-front_with_markline.jpg)  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__tatsumi-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_tatsumi_front.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、和**若苗空front**(红色mark点)的对比(**脸颊几乎一致**)；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front；  
          （下图左五）若苗空front渐变对比；  
        ![](img/face_front_tatsumi_front.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_sora_front_vs_tatsumi_front.gif)  
          辰巳front和空front脸颊几乎一致。我猜，之所以为辰巳加上胡子，可能是为了避免两人相貌太相似。  

        - 葵front对比辰巳front，参见“葵front”一节。  

    - left（脸向右转，左脸稍微偏大，右脸稍微偏小）：  
        - 图片：13_074_4，09_093_7，06_026_5。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__tatsumi-left_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_tatsumi_left.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-left_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-left-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、和空front(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front；  
        ![](img/face_front_tatsumi_left.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-left_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-left-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

        - 摄像师对比辰巳left，参见“摄像师open”一节。  

        - 八不02_183_7对比辰巳left，参见“八不”一节。  

    - （下图左一）这些图片叠加后的效果；  
      （下图左二）熏front(黑色mark点)、**辰巳front**(蓝色mark点)、**辰巳left**(绿色mark点)的对比（血缘关系）（和辰巳front相似度高）；  
      （下图左三）左一叠加左二：  
      （下图左四）辰巳front；  
      （下图左五）辰巳left；  
    ![](img/face_front_kaoru_front.jpg) 
    ![](img2/face_front_cmp_aps__kaoru-front_vs_tatsumi-left_vs_tatsumi-front.jpg) 
    ![](img2/face_front_cmp_aps__kaoru-front-p_vs_tatsumi-left_vs_tatsumi-front.jpg) 
    ![](img/face_front_tatsumi_front.jpg) 
    ![](img/face_front_tatsumi_left.jpg) 


- **导演-正面**   
    - 导演戴眼镜，所以无法定位眼睛的位置。只能大概估计外眼角水平位置约为眼镜腿的位置。  
    
    - 导演胖，五官比例不符合标准人像比例，但符合胖人的人像比例。以下显示了胖人的五官比例[1]：  
        ![](img/DrawingTheHeadHands.AndrewLoomis.page014.jpg)  

    - front：  
        - 图片：03_061_2, 03_099_1。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__director-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_director_front.jpg) 
        ![](img2/face_front_cmp_aps__director-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__director-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - 导演front对比雅彦group2。前者脸胖于后者。对比导演(蓝色)、雅彦(红色)脸颊上的点(27/zy，29/ang，31/latgo，33/chk，35/al-chin):   
        ![](img2/face_front_masahiko_group2_vs_director_front.jpg)  

        - 绫子front对比导演front（两人都胖），参见“绫子front”一节。  


- **早纪(Saki)-正面**  
    - front:  
        - 图片：10_042_5, 09_176_2, 09_175_7, 09_175_1, 09_174_4, 09_152_3。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__yukari-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（相比紫苑group3，早纪front下颌骨线条更直；两组的左脸很类似，紫苑group3的右脸占比例小(右转头)，而早纪front的右脸是正面）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
        
        - 早纪front类似雅彦group2，没有雅彦group2的腮帮子凸。对比早纪(蓝色)、雅彦(红色)脸颊上的点, 29/ang，31/latgo，35/al-chin几乎重合，说明两人脸形相似。27/zy，33/chk差异较大，说明雅彦腮帮子更凸:     
        ![](img2/face_front_masahiko_group2_vs_saki_front.jpg)  

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比(脸形很类似)；  
          （下图左三）左一叠加左二：  
          （下图左四）熏front；   
          （下图左五）熏front渐变对比；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg) 
        ![](img/face_front_kaoru_front_vs_saki_front.gif) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**紫front**(红色mark点)的对比(早纪的脸窄于紫的脸)；  
          （下图左三）左一叠加左二：  
          （下图左四）紫front；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

        - 早纪front对比紫front。早纪的脸窄于紫的脸。对比早纪(蓝色)、紫(红色)左脸颊上的点：    
        ![](img2/face_front_yukari_front_vs_saki_front.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**雅美group0**(红色mark点)的对比(早纪下巴比雅美下巴长一些(或许是因为雅美闭嘴，早纪稍张嘴)，早纪眼眶处的脸颊比雅美窄)；  
          （下图左三）左一叠加左二：  
          （下图左四）雅美group0；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_masami-group0.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_masami-group0.jpg) 
        ![](img/face_front_masami_group0_vol03.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比(早纪下巴比雅美下巴长一些(或许是因为雅美闭嘴，早纪稍张嘴)，早纪眼眶处的脸颊比雅美窄)；  
          （下图左三）左一叠加左二：  
          （下图左四）雅美group1；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**浩美right**(红色mark点)的对比(早纪front的脸更有棱角（腮帮子棱角：早纪 > 熏 > 浩美）)；  
          （下图左三）左一叠加左二：  
          （下图左四）浩美right；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 

        - 早纪front对比浩美right，早纪front的脸更有棱角。对比早纪(蓝色)、浩美(红色)的点：  
        ![](img2/face_front_hiromi_right_vs_saki_front.jpg) 

        - 顺子front对比早纪front，参见“顺子front”一节。  

    - top_short(稍微低头、下巴稍短):  
        - 图片：11_113_2，10_043_1。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__saki-top_short_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_saki_top_short.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**紫front**(红色mark点)的对比(早纪的脸窄于紫的脸)；  
          （下图左三）左一叠加左二：  
          （下图左四）紫front；  
        ![](img/face_front_saki_top_short.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**盐谷right-top**(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）盐谷right-top；  
        ![](img/face_front_saki_top_short.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short_vs_shya-right-top.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short-p_vs_shya-right-top.jpg) 
        ![](img/face_front_shya_right-top.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）熏front(黑色mark点)、**早纪front**(红色mark点)、**早纪top-short**(紫色mark点)的对比（血缘关系）（相似度高；比起辰巳，熏的脸颊更象早纪）；  
          （下图左三）左一叠加左二：  
          （下图左四）早纪front；  
          （下图左五）早纪top-short；  
        ![](img/face_front_kaoru_front.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front_vs_saki-front_vs_saki-top-short.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front-p_vs_saki-front_vs_saki-top-short.jpg) 
        ![](img/face_front_saki_front.jpg) 
        ![](img/face_front_saki_top_short.jpg) 

        - 叶子母13_103_3和早纪top_short，参见“叶子母”一节。  


- **真琴(Makoto)-正面**  
几乎都是长大嘴，多数是笑（这是否能反映她的性格？）。  
    - front(正面):  
        - 图片：03_021_2, 04_176_1, 06_005_6, 08_110_0, 07_075_5, 07_072_0。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__makoto-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二；  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_makoto_front.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比(脸颊相似)；  
          （下图左三）左一叠加左二；  
          （下图左四）雅美group1；  
          （下图左五）雅美group1渐变对比；  
        ![](img/face_front_makoto_front.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img/face_front_masami_group1_vs_makoto_front.gif) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**雅美group0**(红色mark点)的对比(脸颊相似)；  
          （下图左三）左一叠加左二：  
          （下图左四）雅美group0；  
        ![](img/face_front_makoto_front.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front_vs_masami-group0.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front-p_vs_masami-group0.jpg) 
        ![](img/face_front_masami_group0_vol03.jpg) 


- **摄像师-正面**  
几乎都是长大嘴，
    - open(正面、张嘴):   
        - 图片：14_171_4, 12_157_5, 12_148_0, 10_067_3, 03_070_4。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__cameraman-open_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_cameraman_open.jpg) 
        ![](img2/face_front_cmp_aps__cameraman-open_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__cameraman-open-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**辰巳left**(红色mark点)的对比(脸型有一点类似，摄像师棱角更明显)；  
          （下图左三）左一叠加左二：  
          （下图左四）辰巳left；  
        ![](img/face_front_cameraman_open.jpg) 
        ![](img2/face_front_cmp_aps__cameraman-open_vs_tatsumi-left.jpg) 
        ![](img2/face_front_cmp_aps__cameraman-open-p_vs_tatsumi-left.jpg) 
        ![](img/face_front_tatsumi_left.jpg) 


- **浅葱(Assagi)-正面**  
    - front(正面):   
        - 图片：14_250_1，14_197_0，14_196_1，14_193_1，14_185_2，14_169_5，14_090_1，14_057_0，14_039_0。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__assagi-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_assagi_front.jpg) 
        ![](img2/face_front_cmp_aps__assagi-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__assagi-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果；  
          （下图左二）本组(黑色mark点)、**紫front**(红色mark点)的对比(**脸颊几乎一致**；浅葱眉略高；浅葱嘴角略高，说明下嘴唇厚；紫的发际线略高(因为年龄大？))；  
          （下图左三）左一叠加左二：  
          （下图左四）紫front；  
          （下图左五）紫front渐变对比；  
        ![](img/face_front_assagi_front.jpg) 
        ![](img2/face_front_cmp_aps__assagi-front_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__assagi-front-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_yukari_front_vs_assagi_front.gif) 


- **和子(Kazuko)-正面**  
和子的面部比例大多不符合标准：    
    ![](img/01_052_2__mod.jpg) 
    ![](img/08_092_3__mod.jpg)  
和子是大块头，所以头应该大；所以让其脸宽符合参考图，使得头偏大。  

    - front_open(正面、稍张嘴)：  
        - 图片：08_120_3，08_091_5。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__kazuko-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（和子下颌骨明显大一圈）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_kazuko_front_open.jpg) 
        ![](img2/face_front_cmp_aps__kazuko-front-open_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kazuko-front-open-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - 和子(和人)的脸型和以上人物都不相似。  
    
    - 和子与宪司这两个大块头的对比，见“宪司front”一节。  


- **爷爷-正面**  
待完成  


- **横田进(Susumu)-正面**  
    - 03_021_2，横田进这个镜头不符合标准比例：  
        ![](img/03_021_2__mod.jpg)  

    - 03_012_5(第一次出场时的正面特写)：  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__susumu-03_012_5_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（横田进脸颊更有棱角、且腮帮子不凸）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_susumu_03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__susumu-03_012_5_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__susumu-03_012_5-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - 横田进03_012_5和雅彦group2的脸形很类似，前者更有棱角、且腮帮子不凸。对比横田进(蓝色)、雅彦(红色)脸颊上的点。29/ang, 31/latgo, 33/chk, 35/al-chin这些点的差异说明雅彦的腮帮子更凸：  
        ![](img2/face_front_susumu_03_012_5_vs_masahiko_group2.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**空front**(红色mark点)的对比（横田进脸颊稍窄）；  
          （下图左三）左一叠加左二：  
          （下图左四）空front；  
        ![](img/face_front_susumu_03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__susumu-03_012_5_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__susumu-03_012_5-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

        - 千夏12_141_0对比横田进03_012_5，参见“千夏”一节。  

    - open(正面，微张嘴)：  
        - 图片：03_094_1，03_012_5。  
        - 这些图片叠加后的效果：  
        ![](img/face_front_susumu_open.jpg)  

    - 横田进open和空front的脸形相似，前者脸颊窄于后者。   


- **宪司(Kenji)-正面**  
宪司是大块头，所以头应该大；所以让其脸宽符合参考图，使得头偏大。  
    - front:  
        - 图片：04_039_5, 04_035_1, 04_014_1。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__kenji-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_kenji_front.jpg) 
        ![](img2/face_front_cmp_aps__kenji-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kenji-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、和子(和人)front_open(红色mark点)的对比（宪司下巴更大，这符合设定--宪司开店需要经常装货卸货，比起画漫画的和子，宪司有更多体力活儿）；  
          （下图左三）左一叠加左二;  
          （下图左四）和子(和人)front_open;  
        ![](img/face_front_kenji_front.jpg) 
        ![](img2/face_front_cmp_aps__kenji-front_vs_kazuko-front-open.jpg) 
        ![](img2/face_front_cmp_aps__kenji-front-p_vs_kazuko-front-open.jpg) 
        ![](img/face_front_kazuko_front_open.jpg) 

        - 宪司front与和子(和人)front_open的脸型有些类似。宪司的下巴比和人的下巴宽大。对比宪司(蓝色)、和子(红色)脸颊上的点：  
        ![](img2/face_front_kazuko_front_vs_kenji_front.jpg)  


- **顺子(Yoriko)-正面**  
    - front:  
        - 图片：10_173_3，04_039_0，04_016_3，06_187_0，03_172_0。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__yoriko-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**若苗空front**(红色mark点)的对比（血缘关系）；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front;  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅美group1;  
          （下图左五）雅美group1渐变对比;  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img/face_front_masami_group1_vs_yoriko_front.gif) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比；  
          （下图左三）左一叠加左二;  
          （下图左四）熏front;  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**早纪front**(红色mark点)的对比(脸颊类似，**左脸几乎一致**)；  
          （下图左三）左一叠加左二;  
          （下图左四）早纪front;  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_vs_saki-front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front-p_vs_saki-front.jpg) 
        ![](img/face_front_saki_front.jpg) 

        - 顺子front和早纪front脸型类似，顺子的脸比早纪大一圈。对比顺子(蓝色)、早纪(红色)脸颊上的点，几乎一致。31/latgo, 35/al-chin点的差异，说明那里顺子的脸比早纪的凸：  
        ![](img2/face_front_yoriko_front_vs_saki_front.jpg)  


- **森(Mori)-正面**  
    - front:  
        - 图片：10_102_3，10_102_1。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__mori-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(左脸类似，但本组腮帮子微凸)；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_mori_front.jpg) 
        ![](img2/face_front_cmp_aps__mori-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__mori-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**若苗空front**(红色mark点)的对比(森front左下颌骨末端稍窄于若苗空)；  
          （下图左三）左一叠加左二：  
          （下图左四）若苗空front;  
        ![](img/face_front_mori_front.jpg) 
        ![](img2/face_front_cmp_aps__mori-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__mori-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

        - 森front和空front的脸很类似，前者左下颌骨末端稍窄于后者。对比森(蓝色)、空(红色)脸颊上的点：  
        ![](img2/face_front_sora_front_vs_mori_front.jpg)  


- **叶子母-正面**  
    - 13_103_3(第一次出场的正面)：  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__ykma-13_103_3_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
        
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）叶子front组(黑色mark点)和**叶子母13_103_3**(红色mark点)的对比（血缘关系）（叶子腮帮子似乎更凸，婴儿肥?）；  
          （下图左三）左一叠加左二：  
          （下图左四）叶子母13_103_3；  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_ykma-13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_ykma-13_103_3.jpg) 
        ![](img/face_front_ykma_13_103_3.jpg) 

        - 叶子母13_103_3和叶子front脸型类似，叶子腮帮子似乎更凸。对比叶子母(蓝色)、叶子(红色)脸颊上的点，右脸29/ang，31/latgo的差异说明前者腮帮子没有后者凸：  
        ![](img2/face_front_yoko_front_vs_ykma_13_103_3.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**浩美right**(红色mark点)的对比(脸型很类似)；  
          （下图左三）左一叠加左二：  
          （下图左四）浩美right;  
          （下图左五）浩美right渐变对比;  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 
        ![](img/face_front_hiromi_right_vs_ykma_13_103_3.gif)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）熏front;  
          （下图左五）熏front渐变对比;  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg) 
        ![](img/face_front_yoko_front_vs_ykma_13_103_3.gif)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**早纪front**(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）早纪front;  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_vs_saki-front.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3-p_vs_saki-front.jpg) 
        ![](img/face_front_saki_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**早纪top_short**(红色mark点)的对比（脸颊类似，叶子母下颌骨末端偏高）；  
          （下图左三）左一叠加左二：  
          （下图左四）早纪top_short;  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_vs_saki-top-short.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3-p_vs_saki-top-short.jpg) 
        ![](img/face_front_saki_top_short.jpg) 

        - 叶子母13_103_3和早纪top_short的脸型有些类似，前者下颌骨末端偏高。对比叶子母(蓝色)、早纪(红色)脸颊上的点，右脸29/ang，31/latgo的变化方向相反，说明前者的这部位没有后者棱角明显：       
        ![](img2/face_front_saki_top-short_vs_ykma_13_103_3.jpg)  


- **理沙(Risa)-正面**  
    - 12_025_0：  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__risa-12_025_0_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(**理沙脸宽**)；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_risa_12_025_0.jpg) 
        ![](img2/face_front_cmp_aps__risa-12_025_0_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__risa-12_025_0-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、紫苑group0(红色mark点)的对比（脸颊类似，**理沙脸宽**）；  
          （下图左三）左一叠加左二：  
          （下图左四）紫苑group0;  
        ![](img/face_front_risa_12_025_0.jpg) 
        ![](img2/face_front_cmp_aps__risa-12_025_0_vs_shion-group0.jpg) 
        ![](img2/face_front_cmp_aps__risa-12_025_0-p_vs_shion-group0.jpg) 
        ![](img/face_front_risa_12_025_0.jpg) 
        ![](img/face_front_shion_group0-young.jpg) 


- **美菜(Mika)-正面**  
美菜的脸最宽。  
    - front(正面):  
        - 图片：12_008_6，11_167_4。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__mika-front_0_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(**美菜脸宽**)；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_mika_front.jpg) 
        ![](img2/face_front_cmp_aps__mika-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__mika-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比(**美菜脸宽**)；  
          （下图左三）左一叠加左二：  
          （下图左四）雅美group1；  
        ![](img/face_front_mika_front.jpg) 
        ![](img2/face_front_cmp_aps__mika-front_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__mika-front-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg)  

        - 美菜front和雅美group1的脸型类似，前者(上半部分)脸宽。对比美菜(蓝色)、雅美(红色)脸颊上的点：  
        ![](img2/face_front_mika_front_vs_masami_group1.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**紫front**(红色mark点)的对比(**美菜脸略宽**)；  
          （下图左三）左一叠加左二：  
          （下图左四）紫front；  
        ![](img/face_front_mika_front.jpg) 
        ![](img2/face_front_cmp_aps__mika-front_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__mika-front-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

        - 美菜front和紫front的脸型类似，前者(上半部分)脸略宽。对比美菜(蓝色)、紫(红色)脸颊上的点：  
        ![](img2/face_front_mika_front_vs_yukari_front.jpg) 

        - 齐藤茜front和美菜front，参见“齐藤茜front”一节。  


- **齐藤茜(Akane)-正面**  
    - front(正面):  
        - 图片：06_145_7，06_144_5。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__akane-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_akane_front.jpg) 
        ![](img2/face_front_cmp_aps__akane-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__akane-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**美菜front**(红色mark点)的对比(脸型类似, 齐藤茜鬓角处稍窄)；  
          （下图左三）左一叠加左二；  
          （下图左四）美菜front；  
        ![](img/face_front_akane_front.jpg) 
        ![](img2/face_front_cmp_aps__akane-front_vs_mika-front.jpg) 
        ![](img2/face_front_cmp_aps__akane-front-p_vs_mika-front.jpg) 
        ![](img/face_front_mika_front.jpg) 

        - 齐藤茜front和美菜front脸型类似，前者鬓角处窄。对比齐藤茜(蓝色)、美菜(红色)脸颊上的点：  
        ![](img2/face_front_akane_front_vs_mika_front.jpg) 


- **仁科(Nishina)-正面**  
    - 14_014_5(只显示了右脸):  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__nishina-14_014_5_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(**仁科脸颊窄**)；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_nishina_14_014_5.jpg) 
        ![](img2/face_front_cmp_aps__nishina-14_014_5_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__nishina-14_014_5-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - 仁科14_014_5右腮帮子比大多数人窄. 对比仁科(蓝色)、雅彦group2(红色)脸颊上的点, 两人在29/ang，31/latgo两个点上的明显差异说明仁科右腮帮子比雅彦窄：  
        ![](img2/face_front_nishina_14_014_5_vs_masahiko_group2.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**横田进03_012_5**(红色mark点)的对比(仁科腮帮子稍窄)；  
          （下图左三）左一叠加左二：  
          （下图左四）横田进03_012_5；  
        ![](img/face_front_nishina_14_014_5.jpg) 
        ![](img2/face_front_cmp_aps__nishina-14_014_5_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__nishina-14_014_5-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg) 


- **葵(Aoi)-正面**  
    - 该角色的造型(下图左一)类似CH里獠的异装(下图左二)：  
        ![](../fc_dress/img/06_039_0__.jpg) 
        ![](../ch_make-up/img/ch_34_093__mod.jpg)  

    - front(正面):  
        - 图片：06_073_4，06_042_1。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__aoi-front_rl-cmp.jpg) 
        - 特点：正面。虽然左右脸不对称，但人中、嘴唇中点位于"两外眼角间距的中线"上(如下图所示)，说明这是严格的正面。在本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节里归纳出了该画法的规律。    
        ![](img/face_asymmetry_aoi-front_with_markline.jpg)  
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**若苗空front**(红色mark点)的对比(脸形很类似，若苗空脸颊的线条更直)；  
          （下图左三）左一叠加左二；  
          （下图左四）若苗空front;  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg)  
           **葵的左右脸不对称，若苗空的左右脸也不对称；但两人左脸类似，右脸也类似**。在本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节里归纳出了该画法的规律。      

        - 葵front和若苗空front的脸型很类似，前者脸颊的线条没有后者直。对比葵(蓝色)、空(红色)脸颊上的点,两人在29/ang，31/latgo，35/al-chin点的差异表明葵的线条比空的更弯曲。      
        ![](img2/face_front_aoi_front_vs_sora_front.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**辰巳front**(红色mark点)的对比(脸形类似，葵右下巴比辰巳窄)；  
          （下图左三）左一叠加左二；  
          （下图左四）辰巳front;  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_tatsumi-front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_tatsumi-front.jpg) 
        ![](img/face_front_tatsumi_front.jpg)  
           **葵的左右脸不对称，辰巳的左右脸也不对称；但两人左脸类似，右脸也类似**。在本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节里归纳出了该画法的规律。      

        - 葵front和辰巳front的脸形类似，前者右下巴比后者窄。对比葵(蓝色)、辰巳(红色)脸颊上的点,35/al-chin点的差异表明葵的右下巴窄：    
        ![](img2/face_front_aoi_front_vs_tatsumi_front.jpg)  


        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比(右脸类似，葵脸颊稍宽)；  
          （下图左三）左一叠加左二；  
          （下图左四）熏front：  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**江岛wide**(红色mark点)的对比(葵脸颊宽)；  
          （下图左三）左一叠加左二；  
          （下图左四）江岛wide：  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_ejima-wide.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_ejima-wide.jpg) 
        ![](img/face_front_ejima_wide.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**真琴front**(红色mark点)的对比(**脸颊处的红黑mark点几乎等距离地间隔排列，这说明什么？**)；  
          （下图左三）左一叠加左二；  
          （下图左四）真琴front：  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_makoto-front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_makoto-front.jpg) 
        ![](img/face_front_makoto_front.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**横田进13_012_5**(红色mark点)的对比(葵脸稍宽)；  
          （下图左三）左一叠加左二；  
          （下图左四）横田进13_012_5：  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg)  

 
- **奶奶-正面**  
待完成  


- **文哉(Fumiya)-正面**  
    - front(正面):  
        - 图片：12_012_3，12_010_3。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__fumiya-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（文哉腮帮子偏上、没有雅彦凸）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - 文哉front和雅彦group2的脸型类似，前者腮帮子偏上、且没有后者凸。对比文哉(蓝色)、雅彦(红色)脸颊上的点：29/ang点的差异表明文载微笑(嘴角上移)；31/latgo，33/chk, 35/al-chin点的差异与"文哉的腮帮子没有雅彦凸"有关：  
        ![](img2/face_front_fumiya_front_vs_masahiko_group2.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**叶子front**(红色mark点)的对比(叶子腮帮子凸（婴儿肥？）)；  
          （下图左三）左一叠加左二；  
          （下图左四）叶子front：  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_yoko-front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_yoko-front.jpg) 
        ![](img/face_front_yoko_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**浩美right**(红色mark点)的对比(脸型类似)；  
          （下图左三）左一叠加左二；  
          （下图左四）浩美right：  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**江岛wide**(红色mark点)的对比；  
          （下图左三）左一叠加左二；  
          （下图左四）江岛wide：  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_ejima-wide.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_ejima-wide.jpg) 
        ![](img/face_front_ejima_wide.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**横田进03_012_5**(红色mark点)的对比(脸型很类似)；  
          （下图左三）左一叠加左二；  
          （下图左四）横田进03_012_5；  
          （下图左五）横田进03_012_5渐变对比：  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5_vs_fumiya_front.gif) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**葵front**(红色mark点)的对比(本组脸稍窄)；  
          （下图左三）左一叠加左二；  
          （下图左四）葵front：  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_aoi-front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_aoi-front.jpg) 
        ![](img/face_front_aoi_front.jpg) 


- **齐藤玲子(Reiko)-正面**  
    - 01_166_0(左脸、正面):  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__reiko-01_166_0_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_reiko_01_166_0.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**紫苑group0**(红色mark点)的对比(两组的相貌看起来年龄小；**脸颊几乎一致**)；  
          （下图左三）左一叠加左二；  
          （下图左四）紫苑group0；  
          （下图左五）紫苑group0渐变对比;  
        ![](img/face_front_reiko_01_166_0.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0_vs_shion-group0.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0-p_vs_shion-group0.jpg) 
        ![](img/face_front_shion_group0-young.jpg) 
        ![](img/face_front_shion_group0_vs_reiko_01_166_0.gif) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**齐藤茜front**(红色mark点)的对比(本组脸颊比齐藤茜小(也可能是本组放缩不当?))；  
          （下图左三）左一叠加左二；  
          （下图左四）齐藤茜front；  
        ![](img/face_front_reiko_01_166_0.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0_vs_akane-front.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0-p_vs_akane-front.jpg) 
        ![](img/face_front_akane_front.jpg) 


- **松下敏史(Toshifumi)-正面**  
没有完全的正面镜头。09_077_5稍低头，09_076_5稍右转。  
    - 09_077_5:  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__matsu-09_077_5_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（雅彦腮帮子凸(婴儿肥?)）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - 松下敏史09_077_5和雅彦group2的脸型类似，前者没有后者腮帮子凸。对比松下敏史(蓝色)、雅彦(红色)脸颊上的点. 29/ang, 31/latgo，35/al-chin点的差异表明前者的腮帮子没有后者凸：  
        ![](img2/face_front_matsu_09_077_5_vs_masahiko_group2.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**空front**(红色mark点)的对比(左脸类似，本组右脸稍窄)；  
          （下图左三）左一叠加左二；  
          （下图左四）空front；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**叶子front**(红色mark点)的对比(叶子腮帮子稍凸(婴儿肥?))；  
          （下图左三）左一叠加左二；  
          （下图左四）叶子front；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_yoko-front.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_yoko-front.jpg) 
        ![](img/face_front_yoko_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**浩美front**(红色mark点)的对比；  
          （下图左三）左一叠加左二；  
          （下图左四）浩美front；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_hiromi-front.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_hiromi-front.jpg) 
        ![](img/face_front_hiromi_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**浩美right**(红色mark点)的对比；  
          （下图左三）左一叠加左二；  
          （下图左四）浩美right；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**辰巳front**(红色mark点)的对比；  
          （下图左三）左一叠加左二；  
          （下图左四）辰巳front；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_tatsumi-front.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_tatsumi-front.jpg) 
        ![](img/face_front_tatsumi_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**横田进03_012_5**(红色mark点)的对比(脸形类似)；  
          （下图左三）左一叠加左二；  
          （下图左四）横田进03_012_5；  
          （下图左五）动态渐变对比：  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5_vs_matu_09_077_5.gif) 


- **章子(Shoko)-正面**  
待完成


- **阿透(Tooru)-正面**  
没有符合参考图的镜头。  
    - open(正面、张嘴):  
        - 图片：12_046_1(张大嘴)，12_044_0(张大嘴)，12_038_3(张大嘴)，12_015_1(张嘴)。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__tooru-open_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_tooru_open.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**空front**(红色mark点)的对比；  
          （下图左三）左一叠加左二；  
          （下图左四）空front；  
        ![](img/face_front_tooru_open.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**仁科14_014_5**(红色mark点)的对比(本组腮帮子凸)；  
          （下图左三）左一叠加左二；  
          （下图左四）仁科14_014_5；  
        ![](img/face_front_tooru_open.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open_vs_nishina-14_014_5.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open-p_vs_nishina-14_014_5.jpg) 
        ![](img/face_front_sora_front.jpg) 


- **京子(Kyoko)-正面**  
    - 02_177_2(正面):    
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__kyoko-02_177_2_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_kyoko_02_177_2.jpg) 
        ![](img2/face_front_cmp_aps__kyoko-02_177_2_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kyoko-02_177_2-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**齐藤茜front**(红色mark点)的对比；  
          （下图左三）左一叠加左二；  
          （下图左四）齐藤茜front；  
          （下图左五）齐藤茜front渐变对比：  
        ![](img/face_front_kyoko_02_177_2.jpg) 
        ![](img2/face_front_cmp_aps__kyoko-02_177_2_vs_akane-front.jpg) 
        ![](img2/face_front_cmp_aps__kyoko-02_177_2-p_vs_akane-front.jpg) 
        ![](img/face_front_akane_front.jpg) 
        ![](img/face_front_akane_front_vs_kyoko_02_177_2.gif) 
        

- **一马(Kazuma)-正面**  
待完成  


- **一树(Kazuki)-正面**  
    - 12_012_3(正面):  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__kazuki-12_012_3_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组脸颊和雅彦类似）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
          （下图左六）雅彦Group2动态渐变对比：  
        ![](img/face_front_kazuki_12_012_3.jpg) 
        ![](img2/face_front_cmp_aps__kazuki-12_012_3_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kazuki-12_012_3-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img/face_front_masahiko_group2_vs_kazuki_12_012_3.gif)  

    - 一树12_012_3和雅彦group2叠加，可以看看雅彦戴眼镜的样子：  
        ![](img/face_front_kazuki_masahiko_glasses.jpg)   


- **千夏(Chinatsu)-正面**  
    - 12_141_0:  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group3(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**早纪front**(红色mark点)、**早纪top_short**(紫色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）早纪front；  
          （下图左五）早纪top_short；  
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_vs_saki-front_vs_saki-top-short.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0-p_vs_saki-front_vs_saki-top-short.jpg) 
        ![](img/face_front_saki_front.jpg) 
        ![](img/face_front_saki_top_short.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**横田进03_012_5**(红色mark点)的对比（千夏鬓角处稍宽、下巴尖处稍窄、左下颌骨末端圆滑；横田进下颌线条较直）；  
          （下图左三）左一叠加左二：  
          （下图左四）横田进03_012_5;  
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg) 

        - 千夏12_141_0和横田进03_012_5的脸型有些类似，前者鬓角处稍宽。对比千夏(蓝色)、横田进(红色)脸颊上的点. 千夏左脸27/zy处稍宽：  
        ![](img2/face_front_chinatsu_12_141_0_vs_susumu_03_012_5.jpg)  
        此外，mark点无法反映出"前者下巴尖左侧稍窄、左下颌骨末端圆滑"这两个特点:  
        ![](img2/face_front_chinatsu_12_141_0_vs_susumu_03_012_5.gif)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**顺子front**(红色mark点)的对比（千夏右脸稍窄）；  
          （下图左三）左一叠加左二;  
          （下图左四）顺子front;  
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_vs_yoriko-front.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141-p_0_vs_yoriko-front.jpg) 
        ![](img/face_front_yoriko_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**文哉front**(红色mark点)的对比（千夏下颌更尖）；  
          （下图左三）左一叠加左二;  
          （下图左四）文哉front; 
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_vs_fumiya-front.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0-p_vs_fumiya-front.jpg) 
        ![](img/face_front_fumiya_front.jpg) 

    - wide(脸颊宽):  
        - 图片：14_016_2，12_159_3。  
        - 这些图片叠加后的效果：  
        ![](img/face_front_chinatsu_wide.jpg) 

    - chinatsu wide和葵front的右脸类似。 前者左脸窄于后者。  

    - chinatsu wide和江岛wide左脸类似。前者右脸宽于后者。  


- **绫子(Aya)-正面**  
    - front(正面):  
        - 图片：02_189_4, 02_180_5。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__aya-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_aya_front.jpg) 
        ![](img2/face_front_cmp_aps__aya-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__aya-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**导演front**(红色mark点)的对比（导演更胖一些）；  
          （下图左三）左一叠加左二：  
          （下图左四）导演front;  
        ![](img/face_front_aya_front.jpg) 
        ![](img2/face_front_cmp_aps__aya-front_vs_director-front.jpg) 
        ![](img2/face_front_cmp_aps__aya-front-p_vs_director-front.jpg) 
        ![](img/face_front_director_front.jpg) 

        - 绫子front和导演front的脸型相似，前者腮帮子稍瘦于后者。对比绫子(蓝色)、导演(红色)脸颊上的点(导演更胖):  
        ![](img2/face_front_director_front_vs_aya_front.jpg)  


- **典子(Tenko)-正面**  
    - front(正面):  
        - 图片：02_188_4，02_180_6，02_178_7。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__tenko-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_tenko_front.jpg) 
        ![](img2/face_front_cmp_aps__tenko-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__tenko-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**横田进03_012_5**(红色mark点)的对比（典子脸小、腮帮子处更圆）；  
          （下图左三）左一叠加左二：  
          （下图左四）横田进03_012_5;  
        ![](img/face_front_tenko_front.jpg) 
        ![](img2/face_front_cmp_aps__tenko-front_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__tenko-front-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg) 

    - tenko front和辰巳left_wide的右脸相似，前者的棱角小于后者：   
        ![](img/face_front_tenko_front.jpg)  --
        ![](img/face_front_tatsumi_left-wide_vs_tenko_front.gif) --> 
        ![](img/face_front_tatsumi_left_wide.jpg)  


- **麻衣(Mai)-正面**  
    - front(正面):  
        - 图片：12_159_3，12_141_0。  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点), 左右脸不对称，左脸大：  
        ![](img2/face_front_cmp_aps__mai-front_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（对齐颧点，鼻子未对齐；**脸颊和紫苑几乎一致**）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_mai_front.jpg) 
        ![](img2/face_front_cmp_aps__mai-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__mai-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
          
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比（麻衣左腮帮子稍大、鬓角处稍窄）；  
          （下图左三）左一叠加左二：  
          （下图左四）熏front;  
        ![](img/face_front_mai_front.jpg) 
        ![](img2/face_front_cmp_aps__mai-front_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__mai-front-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg) 

        - 麻衣(Mai)front和熏front的右脸几乎一致，前者左腮帮子稍大于后者、鬓角处稍窄于后者。对比麻衣(蓝色)、熏(红色)脸颊上的点. 左脸29/ang, 31/latgo的差异表明前者腮帮子大于后者:   
        ![](img2/face_front_mai_front_vs_kaoru_front.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**千夏12_141_0**(红色mark点)的对比（对齐颧点，鼻子未对齐；**脸颊类似**）；  
          （下图左三）左一叠加左二；  
          （下图左四）千夏12_141_0；  
        ![](img/face_front_mai_front.jpg) 
        ![](img2/face_front_cmp_aps__mai-front_vs_chinatsu-12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__mai-front-p_vs_chinatsu-12_141_0.jpg) 
        ![](img/face_front_chinatsu_12_141_0.jpg) 


- **古屋公弘(Kimihiro)-正面**  
    - 09_051_6(稍低头):  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点), 左右脸不对称：  
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（对齐颧点，鼻子未对齐；**本组脸颊和雅彦类似**）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - 古屋公弘09_051_6和雅彦group2脸型类似。对比公弘(蓝色)、雅彦(红色)脸颊上的点(前者29/ang、31/latgo重叠). 右脸27/zy的差异说明前者脸颊稍宽；右脸31/latgo的差异表明前者此处瘦:  
        ![](img2/face_front_kimihiro_09_051_6_vs_masahiko_group2.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比（脸颊类似，本组右下颌骨末端稍靠上、左下颌骨末端稍凸）；  
          （下图左三）左一叠加左二：  
          （下图左四）熏front；  
          （下图左五）熏front渐变对比；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg) 
        ![](img/face_front_kaoru_front_vs_kimihiro_09_051_6.gif)  
    
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**早纪front**(红色mark点)的对比（脸颊类似，本组下颌骨末端稍靠上）；  
          （下图左三）左一叠加左二：  
          （下图左四）早纪front;  
          （下图左五）早纪front渐变对比;  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_saki-front.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_saki-front.jpg) 
        ![](img/face_front_saki_front.jpg) 
        ![](img/face_front_kimihiro_09_051_6_vs_saki_front.gif)    
    
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比（对齐鼻子;上半脸类似，本组下半脸长于后者）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅美group1；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**浩美right**(红色mark点)的对比（对齐鼻子;脸颊类似，本组更显棱角）；  
          （下图左三）左一叠加左二：  
          （下图左四）浩美right；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**江岛wide**(红色mark点)的对比（对齐鼻子）；  
          （下图左三）左一叠加左二：  
          （下图左四）江岛wide；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_ejima-wide.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_ejima-wide.jpg) 
        ![](img/face_front_ejima_wide.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**顺子front**(红色mark点)的对比（对齐鼻子）；  
          （下图左三）左一叠加左二：  
          （下图左四）顺子front；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_yoriko-front.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_yoriko-front.jpg) 
        ![](img/face_front_yoriko_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**千夏12_141_0**(红色mark点)的对比（对齐鼻子；本组右脸颊稍宽）；  
          （下图左三）左一叠加左二：  
          （下图左四）千夏12_141_0；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_chinatsu-12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_chinatsu-12_141_0.jpg) 
        ![](img/face_front_chinatsu_12_141_0.jpg) 

        - 古屋朋美09_047_1对比古屋公弘09_051_6脸型类似，参见“古屋朋美”一节。  


- **古屋朋美(Tomomi)-正面**  
    - 09_047_1(正面):  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（相比雅彦，本组脸颊稍胖；相比紫苑，本组左鬓角处稍宽）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**古屋公弘09_051_6**(红色mark点)的对比（**血缘关系**）（对齐鼻子。古屋公弘象雅彦、古屋朋美象雅美，所以两人必然相貌相似；所以把二人设定为兄妹是符合逻辑的）；  
          （下图左三）左一叠加左二：  
          （下图左四）古屋公弘09_051_6;  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_kimihiro-09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_kimihiro-09_051_6.jpg) 
        ![](img/face_front_kimihiro_09_051_6.jpg) 

        - 古屋朋美09_047_1和古屋公弘09_051_6脸型类似。对比朋美(蓝色)、公弘(红色)脸颊上的点。29/ang，31/latgo，33/chk，35/al-chin的差异表明前者右脸颊大于后者:  
        ![](img2/face_front_tomomi_09_047_1_vs_kimihiro_09_051_6.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**雅美group0**(红色mark点)的对比（脸颊类似，雅美嘴宽）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅美group0;  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_masami-group0.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_masami-group0.jpg) 
        ![](img/face_front_masami_group0_vol03.jpg)  

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比（脸颊类似，雅美嘴宽）；  
          （下图左三）左一叠加左二：  
          （下图左四）雅美group1;  
          （下图左五）雅美group1渐变对比;  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img/face_front_masami_group1_vs_tomomi_09_047_1.gif) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**叶子front**(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）叶子front；  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_yoko-front.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_yoko-front.jpg) 
        ![](img/face_front_yoko_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**真琴front**(红色mark点)的对比（脸颊处的红色mark点和黑色mark点间隔出现，说明什么？）；  
          （下图左三）左一叠加左二：  
          （下图左四）真琴front;  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_makoto-front.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_makoto-front.jpg) 
        ![](img/face_front_makoto_front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**顺子front**(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）顺子front;  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_yoriko-front.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_yoriko-front.jpg) 
        ![](img/face_front_yoriko_front.jpg) 


- **八不(Hanzu)-正面**  
    - 02_183_7(正面):  
        - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__hanzu-02_183_7_rl-cmp.jpg) 
        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
          （下图左三）左一叠加左二：  
          （下图左四）雅彦Group2；  
          （下图左五）紫苑Group2；  
        ![](img/face_front_hanzu_02_183_7.jpg) 
        ![](img2/face_front_cmp_aps__hanzu-02_183_7_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__hanzu-02_183_7-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

        - （下图左一）这些图片叠加后的效果;  
          （下图左二）本组(黑色mark点)、**辰巳left**(红色mark点)的对比（本组脸颊窄）；  
          （下图左三）左一叠加左二：  
          （下图左四）辰巳left;  
          （下图左五）辰巳left渐变对比;  
        ![](img/face_front_hanzu_02_183_7.jpg) 
        ![](img2/face_front_cmp_aps__hanzu-02_183_7_vs_tatsumi-left.jpg) 
        ![](img2/face_front_cmp_aps__hanzu-02_183_7-p_vs_tatsumi-left.jpg) 
        ![](img/face_front_tatsumi_left.jpg) 
        ![](img/face_front_hanzu_02_183_7_vs_tatsumi_left.gif) 

        - 八不02_183_7和辰巳left脸型有点类似，前者下颌骨末端偏上。对比八不(蓝色)、辰巳(红色)脸颊上的点(前者29/ang、31/latgo重叠)。右脸29/ang，31/latgo的差异表明前者脸偏瘦(无法表明下颌骨末端偏上)：  
        ![](img2/face_front_hanzu_02_183_7_vs_tatsumi_left.jpg)  


- **哲(Te)-正面**  
没有正面图。   


**总结**：  

###正面左右脸颊不对称  {#正面左右脸颊不对称}  
- FC里角色脸部有“左右脸差异小”的画法，也有“左右脸差异大”的画法。  
    - 左右脸差异小。如：雅彦group2。以雅彦group2的脸颊为例, 下图依次是: 雅彦group2，脸颊曲线(红)，水平反转后的脸颊曲线(绿)，两条曲线的对比(左右脸颊对比)。  
        ![](./img/face_front_masahiko_group2_front.jpg) 
        ![](./img/face_asymmetry_masahiko-group2-front_original.jpg) 
        ![](./img/face_asymmetry_masahiko-group2-front_flip.jpg) 
        ![](./img/face_asymmetry_masahiko-group2_compare.jpg) 
    - 左右脸差异大。如：空front、辰巳front、葵front。首先，在以上这些角色的组里已经说明它们均是严格的正面像。其次，这些角色的组的左右脸颊很类似，呈现出一定规律。对比发现该规律是:**左脸腮帮子较凸出**。以若苗空front的脸颊为例, 下图依次是: 若苗空front，脸颊曲线(红)，水平反转后的脸颊曲线(绿)，两条曲线的对比(左右脸颊对比)，左右脸mark点对比(红色表示原mark点，绿色表示水平翻转mark点)。  
        ![](./img/face_front_sora_front.jpg) 
        ![](./img/face_asymmetry_sora-front_original.jpg) 
        ![](./img/face_asymmetry_sora-front_flipped.jpg) 
        ![](./img/face_asymmetry_sora-front_compare.jpg) 
        ![](img2/face_front_cmp_aps__sora-front_rl-cmp.jpg) 

      此外，下图叠加空front(红)、辰巳front(绿)、葵front(蓝)的mark点。可以看到：相应的mark点聚集在一起，说明他们的脸的特征相似：  
        ![](./img2/face_asymmetry_mark_sora_tatsumi_aoi.jpg) 




###对比更多角色  
- 下载[工程文件](./project2/face_front_cmp_aps.xcf.7z)，解压缩后，用软件Gimp(版本2.10)打开。 
 

- **其他**:  
    - 面部整形外科(Facial Plastic Surgery)于1960年代开始研究美貌[4]，心理学于1970年代开始研究美貌[4]。  
    - 论文[1]发表于2017年，本文图1([1]的图3)里的"23/24，25/26，29/30，31/32，33/34，35/36"点是作者添加的，前人没有定义过这些点。我的理解是：前人没有研究过这些点。以右脸为例，这些点包括"29, 31, 33, 35"。下颌骨上的这4个点刻画了脸颊的外轮廓。我在对比FC人物脸部正面的时候发现，不同人物的面部特征在大致这些点处有明显的不同。所以，综合这些信息，我觉得：**虽然北条司不是最早研究美貌的，但北条司对于(女性)美貌的理解，可能领先于美容整形外科(Aesthetic Plastic Surgery)领域约20年**。  



[0]: <./readme.md> "FC的画风-面部"  
[1]: <https://www.researchgate.net/publication/318548459> "Differences between Caucasian and Asian attractive faces, Seung Chul Rhee, 2017."  
[2]: <https://> "国家标准--人体测量术语(GB 3975-83)"  
[3]: <https://www.researchgate.net/publication/299371197> "Photogrammetric Analysis of Attractiveness in Indian Faces, Shveta Duggal and etc, 2016."  
[4]: <10.1097/01.prs.0000233051.61512.65> "History and Current Concepts in the Analysis of Facial Attractiveness, Mounir Bashour, 2006. (DOI: 10.1097/01.prs.0000233051.61512.65)"  
[5]: <./fc_face_side.md> "FC的画风-面部(侧面)"  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



