
source:  
https://www.manga-audition.com/japanesemanga101_002/


**“Two is better than One” / JapaneseManga101 #002 Double page spread**  
**“双页胜过单页” / JapaneseManga101 ＃002 双页展开**  

 Taiyo Nakashima, 29/12/2014, 1 min read  


Today, we talk about “The double page spread”.  
今天，我们谈论“双页展开”。  

Even in this age of smartphones, “Double page spread” is still hugely important.  
即使在这个智能手机时代，“双页展开”仍然非常重要。

Why? Well, take a look at some examples.  
为什么？ 好吧，看看一些例子。   

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_two_is_better_than_one/img/original0.jpg)  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_two_is_better_than_one/img/original1.jpg)  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_two_is_better_than_one/img/original2.jpg)  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_two_is_better_than_one/img/original3.jpg)   

These are the actual pages by Hojo Sensei!  
这些是北条老师的原画页面！  

Readers always read two pages at a time. From right to left, they actually read several panels at a time towards the last panel. It is really important, to leave a strong impression, as a spread. Concentrate on one panel, then build up the tension toward the end.
读者总是一次阅读两页。 从右到左，他们实际上沿顺序一次读几个分格。 留下深刻的印象很重要。将注意力集中在一个分格上，然后沿着顺序逐渐增加张力。

See, how nearly all pages follow this format?
看，几乎所有的页面都是如何都遵循这种格式的？

Take a look once again, in the printed magazine!
再看看印刷杂志吧！

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_two_is_better_than_one/img/magazine0.jpg)  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_two_is_better_than_one/img/magazine1.jpg)  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_two_is_better_than_one/img/magazine2.jpg)  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_two_is_better_than_one/img/magazine3.jpg)  

Easy follow, entertaining, impressive spread, is vital to your Manga’s success. That’s why all professional Manga editors always read in spread pairs.
读起来很轻松、娱乐性、令人印象深刻的展开，对于你的漫画成功至关重要。 这就是为什么所有专业的漫画编辑总是成对展开阅读。

Make every double page spread, strong and focused, to leave the impression.
把每个双页都展开，会给人强烈的印象，让人注意力集中。

GOT IT!?
懂了吗！？

In the next episode, we will look at the art of paneling. See you next time!
在下一集中，我们将探讨分格的艺术。 下次见！ 



注：  
从图里可以看出，印刷版截掉了原稿边缘的部分内容。我猜，这是为什么下图中文版比日文版FC还要完整一些的原因。
中文版(FC第2卷16页)：  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_two_is_better_than_one/img/02_016_cn.jpg)  
日文版(FC第2卷16页,左上部的筷子尖不完整)：  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/zz_two_is_better_than_one/img/02_016_jp.jpg)



