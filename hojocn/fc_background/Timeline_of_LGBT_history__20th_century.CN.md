source:
https://en.wikipedia.org/wiki/Timeline_of_LGBT_history,_20th_century


#LGBT历史年表, 20世纪

###1900s

- 1901 – On 8 June 1901 two women, Marcela Gracia Ibeas and Elisa Sanchez Loriga, attempted to get married in A Coruña (Galicia, Spain). To achieve it Elisa had to adopt a male identity: Mario Sánchez, as listed on the marriage certificate.[1]

- 1903 – In New York City on 21 February 1903, New York police conducted the first United States recorded raid on a gay bathhouse, the Ariston Hotel Baths. 34 men were arrested and 12 brought to trial on sodomy charges; 7 men received sentences ranging from 4 to 20 years in prison.[2]

- 1906 – Potentially the first openly gay American novel with a happy ending, Imre, is published.[3]

- 1907 – Adolf Brand, the activist leader of the Gemeinschaft der Eigenen, working to overturn Paragraph 175, publishes a piece "outing" the imperial chancellor of Germany, Prince Bernhard von Bülow. The Prince sues Brand for libel and clears his name; Brand is sentenced to 18 months in prison.[4]

- 1907–1909 – Harden–Eulenburg affair in Germany[5]
    

###1910s

- 1910 – Emma Goldman first begins speaking publicly in favor of homosexual rights. Magnus Hirschfeld later wrote "she was the first and only woman, indeed the first and only American, to take up the defense of homosexual love before the general public."[6]
    
- 1912 – The first explicit reference to lesbianism in a Mormon magazine occurred when the "Young Woman's Journal" paid tribute to "Sappho of Lesbos[8] "; the Scientific Humanitarian Committee of the Netherlands (NWHK), the first Dutch organization to campaign against anti-homosexual discrimination, is established by Dr. Jacob Schorer.

- 1913 – The word faggot is first used in print in reference to gays in a vocabulary of criminal slang published in Portland, Oregon: "All the faggots [sic] (sissies) will be dressed in drag at the ball tonight".

- 1917 – The October Revolution in Russia repeals the previous criminal code in its entirety—including Article 995.[9][10] Bolshevik leaders reportedly say that "homosexual relationships and heterosexual relationships are treated exactly the same by the law."

- 1919 – In Berlin, Germany, Doctor Magnus Hirschfeld co-founds the Institut für Sexualwissenschaft (Institute for Sex Research), a pioneering private research institute and counseling office. Its library of thousands of books was destroyed by Nazis in May 1933.[11][12][13]

- 1919 - Different from the Others, one of the first explicitly gay films, is released. Magnus Hirschfeld has a cameo in the film and partially funded its production


###1920s[edit]

- 1921 – In England, an attempt to make lesbianism illegal for the first time in Britain's history fails.[14]

- 1922 – A new criminal code comes into force in the USSR officially decriminalizing homosexual acts.

- 1923 – The word fag is first used in print in reference to gays in Nels Anderson's The Hobo: "Fairies or Fags are men or boys who exploit sex for profit."

- 1923 – Lesbian Elsa Gidlow, born in England, published the first volume of openly lesbian love poetry in the United States, titled "On a Grey Thread."[15]

- 1924 – The first homosexual rights organization in America is founded by Henry Gerber in Chicago— the Society for Human Rights.[16] The group exists for a few months before disbanding under police pressure.[17] Paraguay and Peru legalize homosexuality.

- 1926 – The New York Times is the first major publication to use the word homosexuality.[3] Iconic lesbian café Eve's Hangout in Greenwich Village closed after police raid.

- 1927 - Karol Szymanowski, Poland's openly gay composer, is appointed chief of Poland's state-owned national music school, the Fryderyk Chopin Music Academy.

- 1928 – The Well of Loneliness by Radclyffe Hall is published in the UK and later in the United States. This sparks great legal controversy and brings the topic of homosexuality to public conversation.

- 1929 – On 22 May, Katharine Lee Bates, author of America the Beautiful dies. On 16 October, a Reichstag Committee votes to repeal Paragraph 175; the Nazis' rise to power prevents the implementation of the vote.


###1930s[edit]

- 1931 - A group of transvestites from Barcelona known as "Las Carolinas" carries out the first documented LGTB demonstration in history. They do so after the destruction of a centric public bath of Barcelona (Spain) which was a common LGTB meeting place at the time.[18]

- 1931 - Mädchen in Uniform, one of the first explicitly lesbian films and the first pro-lesbian film, is released.

- 1931 - In Berlin in 1931, Dora Richter became the first known transgender woman to undergo vaginoplasty.[19][20]

- 1932 – Poland codifies the homosexual and heterosexual age of consent equally at 15. Polish law never had criminalized homosexuality, although occupying powers had outlawed it in 1835.[21]

- 1933 – New Danish penalty law decriminalizes homosexuality.

- 1933 – The National Socialist German Workers Party bans homosexual groups. Homosexuals are sent to concentration camps. Nazis burn the library of Magnus Hirschfeld's Institute for Sexual Research, and destroy the Institute; Denmark and Philippines decriminalizes homosexuality. Homosexual acts are recriminalized in the USSR.

- 1934 – Uruguay decriminalizes homosexuality. The USSR once again criminalizes muzhelozhstvo (specific Russian definition of "male sexual intercourse with male", literally "man lying with man"), punishable by up to 5 years in prison – more for the coercion or involvement of minors.[22]

- 1936 – Mona's 440 Club, the first lesbian bar in America, opened in San Francisco in 1936.[23][24] Mona's waitresses and female performers wore tuxedos and patrons dressed their roles.[24]

- 1936 – Federico García Lorca, Spanish poet, is shot at the beginning of the Spanish civil war.

- 1937 – The first use of the pink triangle for gay men in Nazi concentration camps.

- 1938 – The word gay is used for the first time on film in reference to homosexuality in the film Bringing Up Baby.[25]

- 1939 – Frances V. Rummell, an educator and a teacher of French at Stephens College, published an autobiography under the title Diana: A Strange Autobiography; it was the first explicitly lesbian autobiography in which two women end up happily together.[26] This autobiography was published with a note stating "The publishers wish it expressly understood that this is a true story, the first of its kind ever offered to the general reading public".[26]


###1940s[edit]

- 1940 – Iceland decriminalizes homosexuality; the NWHK is disbanded in the Netherlands in May due to the German invasion, and most of its archive is voluntarily destroyed, while the rest is confiscated by Nazi soldiers.
冰岛将同性恋合法化； NWHK(荷兰科学人道主义委员会)因德国入侵而于5月在荷兰解散，大部分档案被自愿摧毁，其余则被纳粹士兵没收。

- 1941 – Transsexuality was first used in reference to homosexuality and bisexuality.
变性首先用于指同性恋和双性恋。

- 1942 – Switzerland decriminalizes homosexuality, with the age of consent set at 20.
瑞士将同性恋合法化，同意年龄定为20岁。

- 1944 – Sweden decriminalizes homosexuality, with the age of consent set at 18 and Suriname legalizes homosexuality.
瑞典同性恋合法化，同意年龄定为18岁，苏里南则将同性恋定为合法。

- 1944 – The first prominent American to reveal his homosexuality was the poet Robert Duncan. This occurred when in 1944, using his own name in the anarchist magazine Politics, he wrote that homosexuals were an oppressed minority.[27]
诗人罗伯特·邓肯（Robert Duncan）是第一个揭示其同性恋的杰出美国人。 这发生在1944年，当时他在无政府主义者杂志《政治》中使用了自己的名字，他写道同性恋是被压迫的少数民族

- 1945 – The Holocaust ends and it is estimated that between about 3,000 to about 9,000 homosexuals died in Nazi concentration and death camps, while it is estimated that between about 2,000 to about 6,000 homosexual survivors in Nazi concentration and death camps were required to serve out the full term of their sentences under Paragraph 175 in prison. The first gay bar in post-World War II Berlin opened in the summer of 1945, and the first drag ball took place in American sector of West Berlin in the fall of 1945.[28] Four honourably discharged gay veterans form the Veterans Benevolent Association, the first LGBT veterans' group.[29] Gay bar Yanagi opened in Japan.[30]
大屠杀结束，据估计在纳粹集中营和死亡集中营中约有3,000至9,000名同性恋者死亡，而在纳粹集中营和死亡集中营中，根据第175条款在监狱中的刑罚，约有2,000至约6,000名同性恋幸存者要服满刑期 。第二次世界大战后柏林的第一个同性恋酒吧于1945年夏天开业，第一个拖曳球于1945年秋天在西柏林的美国地区举行。[28] 四名荣誉退伍的同性恋退伍军人建立了退伍军人慈善协会，这是首个LGBT退伍军人组织，其中。[29] **同性恋酒吧柳木(やなぎ(ゲイバー))在日本开业**。[30]

- 1946 – "COC" (Dutch acronym for "Center for Culture and Recreation"), one of the earliest homophile organizations, is founded in the Netherlands. It is the oldest surviving LGBT organization.
最早的同志组织之一“COC”（荷兰语“文化娱乐中心”的缩写）成立于荷兰。 它是现存最古老的LGBT组织。

- 1946 – Plastic surgeon Harold Gillies carries out sex reassignment surgery on Michael Dillon in Britain.
整形外科医生哈罗德·吉利斯（Harold Gillies）对英国的迈克尔·狄龙（Michael Dillon）进行了性别重置手术 。

- 1947-1948 – Vice Versa, the first American lesbian publication, is written and self-published by Lisa Ben (real name Edith Eyde) in Los Angeles. 
Vice Versa是美国第一部女同性恋出版物，由Lisa Ben（本名伊迪丝·艾德）在洛杉矶撰写和自行出版。

- 1948 – "Forbundet af 1948" ("League of 1948"), a homosexual group, is formed in Denmark. 
同性恋组织“Forbundet af 1948”（“1948联盟”）在丹麦成立。

- 1948 – The communist authorities of Poland make 15 the age of consent for all sexual acts, homosexual or heterosexual. 
波兰共产主义当局规定15岁为所有同性恋或异性恋行为的同意年龄。


###1950s[edit]

- 1950 – The Organization for Sexual Equality, now Swedish Federation for Lesbian, Gay, Bisexual and Transgender Rights (RFSL), is formed in Sweden; East Germany partially abrogates the Nazis' emendations to Paragraph 175; The Mattachine Society, the first sustained American homosexual group, is founded in Los Angeles (11 November); 190 individuals in the United States are dismissed from government employment for their sexual orientation, commencing the Lavender scare.
性别平等组织在瑞典成立，即现在的瑞典女同性恋、男同性恋、双性恋和变性者权利联盟（RFSL）； 东德部分废除了纳粹对第175条款的修正； Mattachine学会于11月11日在洛杉矶成立，它是美国第一个持续存在的同性恋团体，； 在美国有190人因性取向而被政府解雇，薰衣草恐慌就此开始。

- 1951 – Greece decriminalizes homosexuality.
希腊同性恋合法化。

- 1951 – Jordan In 1951, a revision of the Jordanian Criminal Code legalized private, adult, non-commercial, and consensual sodomy, with the age of consent set at 16.
约旦1951年，《约旦刑法》修订版将私人、成人、非商业和经同意的鸡奸合法化，同意年龄定为16岁。

- 1952 – "Spring Fire," the first lesbian paperback novel, and the beginning of the lesbian pulp fiction genre, was published in 1952 and sold 1.5 million copies.[31][32] It was written by lesbian Marijane Meaker under the false name Vin Packer.[31]
《春火》于1952年出版，售出150万本。这是第一部女同性恋平装小说，是女同性恋低俗小说类型的开端。[31] [32] 它是由女同性恋者Marijane Meaker以笔名Vin Packer所写。[31]

- 1952 – In the spring of 1952, Dale Jennings was arrested in Los Angeles for allegedly soliciting a police officer in a bathroom in Westlake Park, now known as MacArthur Park. His trial drew national attention to the Mattachine Society, and membership increased drastically after Jennings contested the charges, resulting in a hung jury.[33]
1952年春天，戴尔·詹宁斯（Dale Jennings）在洛杉矶被捕，原因据说是涉嫌在西湖公园（现称麦克阿瑟公园）的一间浴室里拉客一名警察。 他的审判引起了全国对Mattachine协会的关注，在詹宁斯对这些指控提出异议之后，成员数量急剧增加，导致陪审团无法作出决定。[33]

- 1952 – Christine Jorgensen becomes the first widely publicized person to have undergone sex reassignment surgery, in this case, male to female, creating a world-wide sensation.
克里斯汀·约根森（Christine Jorgensen）成为第一个广为人知的、接受过性重置手术的、由男变女的人。引起了全世界的轰动。

- 1952 – In Japan the male homosexual magazine "Adonis" is launched with the writer Yukio Mishima as a contributor.
**日本发行男性同性恋杂志“Adonis”，由作家三岛由纪夫（Yukio Mishima）撰稿。**

- 1953 – The Diana Foundation was founded on 19 March 1953 in Houston, TX by a small group of friends. The Diana Foundation is a nonprofit organization and recognized as the oldest continuously active gay organization in the United States and hosts two annual fundraising events including its Diana Awards.[34]
戴安娜基金会于1953年3月19日由几个朋友在德克萨斯州休斯顿成立。 戴安娜基金会是一家非营利组织，被认为是美国历史最悠久且持续活跃的同性恋组织，并举办两次年度筹款活动，包括戴安娜奖。[34]

- 1954 – 7 June–Mathematical and computer genius Alan Turing commits suicide by cyanide poisoning, 18 months after being given a choice between two years in prison or libido-reducing hormone treatment for a year as a punishment for homosexuality.[35] A succession of well-known men, including Lord Montagu, Michael Pitt-Rivers and Peter Wildeblood, were convicted of homosexual offences as British police pursued a McCarthy-like purge of Society homosexuals.[36] Arcadie, the first homosexual group in France, is formed.
6月7日-数学和计算机天才艾伦·图灵（Alan Turing）因氰化物中毒而自杀。他被判处两年徒刑或降低性欲的激素治疗一年，以惩罚同性恋。结果18个月后他自杀了。[35] 随着英国警察实施麦卡锡式清理社会同性恋者，一连串的知名人士，包括蒙塔古勋爵，迈克尔·皮特·里弗斯和彼得·怀尔德布洛德，均被判犯有同性恋罪。[36] 法国成立了第一个同性恋团体Arcadie。

- 1955 – The Daughters of Bilitis (DOB) was founded in San Francisco by four lesbian couples (including Del Martin and Phyllis Lyon) and was the first national lesbian political and social organization in the United States.[37] The group's name came from "Songs of Bilitis," a lesbian-themed song cycle by French poet Pierre Louÿs, which described the fictional Bilitis as a resident of the Isle of Lesbos alongside Sappho.[37] DOB's activities included hosting public forums on homosexuality, offering support to isolated, married, and mothering lesbians, and participating in research activities.[37] Mattachine Society New York chapter founded.
1955年 – 四对女同性恋恋人（包括德尔·马丁和菲利斯·里昂）在旧金山成立了Bilitis之女（DOB），这是美国第一个全国性的女同性恋政治和社会组织。[37] 该团体的名字来自法国诗人皮埃尔·卢厄斯（PierreLouÿs）以女同性恋为主题的歌曲套曲的“Bilitis之歌”，该小说将虚构的Bilitis人描述为与Sappho一起在Lesbos岛的居民。[37] DOB的活动包括举办有关同性恋的公共论坛，为孤立的、已婚和身为母亲的女同性恋者提供支持，以及参加研究活动。[37] 美国Mattachine学会纽约分会成立了。

- 1956 – The Ladder, the first nationally distributed lesbian publication in the United States, began publication.
1956年-美国第一本全国发行的女同性恋出版物《阶梯》开始发行。

- 1956 – Thailand decriminalizes homosexual acts.
1956年-泰国将同性恋行为合法化。

- 1956 – Florida Legislative Investigation Committee (commonly known as the Johns Committee) established. Failing to find communist ties to civil rights organizations, it investigates homosexuals as a threat to national security.
1956年-佛罗里达州立法调查委员会（俗称约翰委员会）成立。 由于未能找到与民权组织的共产党联系，它调查了同性恋者对国家安全的威胁。

- 1957 – The word "Transsexual" is coined by U.S. physician Harry Benjamin; The Wolfenden Committee's report recommends decriminalizing consensual homosexual behaviour between adults in the United Kingdom; Psychologist Evelyn Hooker publishes a study showing that homosexual men are as well adjusted as non-homosexual men, which becomes a major factor in the American Psychiatric Association removing homosexuality from its handbook of disorders in 1973. Homoerotic artist Tom of Finland first published on the cover of Physique Pictorial magazine from Los Angeles.[38]
1957年-“变性”一词由美国医生哈里·本杰明（Harry Benjamin）创造； 沃尔芬登委员会的报告建议将英国成年人之间的自愿同性恋行为合法化； 心理学家伊夫琳·胡克（Evelyn Hooker）发表的一项研究表明，同性恋男子与非同性恋男子一样适应能力强，这成为美国精神病学协会（1973年）从其疾病手册中删除同性恋的一个主要因素。 来自洛杉矶的Physique Pictorial杂志。[38]

- 1958 – The Homosexual Law Reform Society is founded in the United Kingdom; Barbara Gittings founds the New York chapter of Daughters of Bilitis.
1958年–同性恋法律改革协会在英国成立； 芭芭拉·吉廷斯（Barbara Gittings）创立了《炎性女儿》的纽约分会。

- 1958 – One, Inc. v. Olesen, 355 U.S. 371 (1958), was the first U.S. Supreme Court ruling to deal with homosexuality and the first to address free speech rights with respect to homosexuality. The Supreme Court reversed a lower court ruling that the gay magazine ONE magazine violated obscenity laws, thus upholding constitutional protection for pro-homosexual writing.[39]
1958年– One，Inc.诉Olesen，诉355 U.S. 371（1958），是美国最高法院处理同性恋的第一项裁决，也是解决同性恋自由言论权的第一项裁决。 最高法院推翻了下级法院的一项裁决，该裁决裁定同性恋杂志《 ONE》杂志违反了淫秽法律，因此维护了赞成同性恋者著作的宪法保护。[39]

- 1958 - The first gay leather bar in the United States, the Gold Coast, opened in Chicago in 1958. It was founded by Dom Orejudos and Chuck Renslow.
1958年-美国第一家同性恋皮革酒吧，黄金海岸，于1958年在芝加哥开业。该酒吧由Dom Orejudos和Chuck Renslow创立。

- 1959 – ITV, at the time the UK's only national commercial broadcaster, broadcasts the UK’s first TV gay drama, South, starring Peter Wyngarde.[40] The first homosexual uprising in the USA occurs at Cooper's Doughnuts in Los Angeles, USA; rioters were arrested by LAPD.[41]
1959年–当时英国唯一的国家商业广播公司ITV播放由彼得·温加德（Peter Wyngarde）主演的英国首部电视同性恋戏剧《南方》。[40] 美国首次发生同性恋起义发生在美国洛杉矶的库珀甜甜圈上。 骚乱者被洛杉矶警察局逮捕。[41]

    
###1960s[edit]

- 1960 – Cpls. Fannie Mae Clackum and Grace Garner, U.S. Air Force reservists in the late 1940s and early 1950s, became the first people to successfully challenge their discharges from the U.S. military for being gay, although the ruling turned on the fact that there wasn't enough evidence to show the women were lesbians.[42] According to scholars, since at least as early as 1960, Executive Order 10450 was applied to ban transgender individuals from serving in the United States military.[43]
1960年-Cpls. 1940年代末和1950年代初的美国空军后备役人员房利美（Fannie Mae Clackum）和格蕾丝·加纳（Grace Garner）成为成功挑战美国军人由于同性恋而被解职的第一批人，尽管该裁定基于事实，即证据不足 证明女人是女同性恋。[42] 根据学者的说法，至少从1960年开始，行政命令10450就被禁止使用跨性别者在美国军队中服役。[43]

- 1961 – Hungary decriminalizes sodomy; Victim is the first English-language film to use the word "homosexual", and premieres in the UK on 31 August 1961; the Vatican declares that anyone who is "affected by the perverse inclination" towards homosexuality should not be allowed to take religious vows or be ordained within the Roman Catholic Church; The Rejected, the first documentary on homosexuality broadcast on American television, is first broadcast on KQED TV in San Francisco on 11 September 1961; José Sarria becomes the first openly gay candidate for public office in the United States when he runs for the San Francisco Board of Supervisors.[44]
1961年–匈牙利将鸡奸合法化； 《受害者》是第一部使用“同性恋”一词的英语电影，于1961年8月31日在英国首映。 梵蒂冈宣布，任何因同性恋而受到“反常倾向”影响的人都不应被允许发表宗教誓言或在罗马天主教堂内受命； 1961年9月11日，第一部在美国电视上播出的关于同性恋的纪录片《被拒绝》在美国旧金山的KQED电视台上首次播出。 乔斯·萨里亚（JoséSarria）竞选旧金山监事会，成为美国首位公开竞选男同性恋者。[44]

- 1962 – Illinois becomes the first U.S. state to remove sodomy law from its criminal code through passage of the American Law Institute's Model Penal Code. While the adopted code did not penalize private sexual relations, it criminalized acts of "Open Lewdness;"[45][46] Czechoslovakia decriminalizes sodomy. The Tavern Guild, the first gay business association in the United States, was created by gay bar owners in 1962 as a response to continued police harassment and closing of gay bars (including the Tay-Bush Inn raid), and continued until 1995.[47]
1962年-伊利诺伊州成为美国第一个通过通过美国法律学会的《刑法典》从其刑法典中删除鸡奸法的州。 尽管通过的守则并未惩罚私人性关系，但将“公开淫荡”定为犯罪； [45] [46]捷克斯洛伐克将鸡奸合法化。 [Tavern Guild]是美国第一个同性恋商业协会，由同性恋酒吧老板于1962年创立，以回应警察对骚扰和关闭同性恋酒吧（包括Tay-Bush Inn袭击）的持续骚扰，并一直持续到1995年。 47]

- 1963 – East Coast Homophile Organizations (ECHO) is established in Philadelphia; initial members include the regional chapters of Daughters of Bilitis, Janus Society, and Mattachine Society.
1963年–在费城成立了东海岸同性恋组织（ECHO）； 最初的成员包括Bilitis女儿，Janus学会和Mattachine学会的区域性分会。

- 1963 – Israel de facto decriminalizes sodomy and sexual acts between men by judicial decision against the enforcement of the relevant section in the old British-mandate law from 1936 (which in fact was never enforced).[citation needed]
1963年-以色列事实上通过司法裁决将男人之间的鸡奸和性行为合法化，该判决是对1936年以前的旧英国授权法中有关部分的执行

- 1964 – Canada sees its first gay-positive organization, ASK, and first gay magazines: ASK Newsletter (in Vancouver), and Gay (by Gay Publishing Company of Toronto). Gay was the first periodical to use the term 'Gay' in the title and expanded quickly, including outstripping the distribution of American publications under the name Gay International. These were quickly followed by Two (by Gayboy (later Kamp) Publishing Company of Toronto).[48][49]
1964年–加拿大见证了其第一个同性恋积极组织ASK，以及第一个同性恋杂志：ASK新闻通讯（在温哥华）和Gay（多伦多同性恋出版公司出版）。 盖伊是第一个在标题中使用“同性恋”一词的期刊，并且迅速发展，包括以盖伊国际的名义超过了美国出版物的发行量。 紧随其后的是两个（多伦多的Gayboy（后Kamp）出版公司）。[48] [49]

- 1964 – Canada: In March 1964, Ted Northe founds the 'Imperial Court of Canada' a monarchist society compromised primarily of drag personalities and becomes a driving force in the effort to achieve equality in Canada. The Courts of Canada now have over 14 chapters across the country and is the oldest, continuously running, LGBT Organization in Canada.
1964年–加拿大：1964年3月，泰德·诺斯（Ted Northe）创立了“加拿大帝国法院”，这是一个君主专制的社会，主要是因拖曳性格而妥协，并成为推动加拿大实现平等的动力。 加拿大法院目前在全国设有14个分庭，是加拿大历史最悠久，持续不断的LGBT组织。

- 1964 – The first photograph of lesbians on the cover of lesbian magazine The Ladder was done in September 1964, showing two women from the back, on a beach looking out to sea.
1964年– 1964年9月拍摄了女同性恋杂志《阶梯》（Ladder）的封面上的第一张女同性恋照片，显示了两名来自背面的妇女在向海的海滩上。

- 1964 – The June 1964 Paul Welch Life article entitled "Homosexuality In America" was the first time a national publication reported on gay issues.
1964年– 1964年6月，保罗·韦尔奇生活（Paul Welch Life）发表题为“美国同性恋”的文章，这是全国性刊物首次报道同性恋问题。

- 1964 – Created in 1964, the Council on Religion and the Homosexual was the first group in the U.S. to use the word "homosexual" in its name.
1964年–成立于1964年的宗教与同性恋理事会是美国第一个使用“同性恋”一词的团体。

- 1965 – The Council on Religion and the Homosexual held an event where local politicians could be questioned about issues concerning gay and lesbian people, including police intimidation. The event marks the first known instance of "the gay vote" being sought.
1965年–宗教与同性恋理事会举行了一次活动，可以向当地政客询问有关男女同性恋者的问题，包括警察的恐吓行为。 该事件标志着正在寻求“同性恋投票”的第一个已知实例。

- 1965 – Everett George Klippert, the last person imprisoned in Canada for homosexuality, is arrested for private, consensual sex with men. After being assessed "incurably homosexual", he is sentenced to an indefinite "preventive detention" as a dangerous sexual offender. This was considered by many Canadians to be extremely homophobic, and prompted sympathetic articles in Maclean's and The Toronto Star, eventually leading to increased calls for legal reform in Canada which passed in 1969.[50]
1965年–埃弗里特·乔治·克里珀特（Everett George Klippert），他在加拿大因同性恋被囚禁的最后一个人，因与男性进行的自愿性性交而被捕。 在被评定为“无法治愈的同性恋”之后，他被判处危险性犯罪者无限期的“预防性拘留”。 许多加拿大人认为这是极端同性恋的行为，并在麦克林（Maclean）和《多伦多星报》（Toronto Star）中引起同情，最终导致在1969年通过的加拿大法律改革呼吁增加。[50]

- 1965 – Conservatively dressed gays and lesbians demonstrate outside Independence Hall in Philadelphia on 4 July 1965. This was the first in a series of Annual Reminders that took place through 1969.
1965年– 1965年7月4日，穿着保守的男女同性恋者在费城独立厅举行示威活动。这是1969年举行的一系列年度提醒活动的第一场。

- 1965 – Vanguard, an organization of LGBT youth in the low-income Tenderloin district, was created in 1965. It is considered the first Gay Liberation organization in the U.S.[51][52]
1965年-Vanguard，1965年成立，是低收入Tenderloin区LGBT青年组织。它被认为是美国第一个同性恋解放组织。[51] [52]

- 1966 – The Mattachine Society stages a "Sip-In" at Julius Bar in New York City challenging a New York State Liquor Authority prohibiting serving alcohol to gays; the National Planning Conference of Homophile Organizations is established (to become NACHO—North American Conference of Homophile Organizations later that year); the Compton's Cafeteria Riot occurred in August 1966 by transgender women and Vanguard members in the Tenderloin district of San Francisco. This incident was one of the first recorded transgender riots in United States history, preceding the more famous 1969 Stonewall Riots in New York City by three years. Vanguard was founded to demonstrate for equal rights. The first lesbian to appear on the cover of the lesbian magazine The Ladder with her face showing was Lilli Vincenz in January 1966. A coalition of Homosexual organizations organized demonstrations for Armed Forces Day to protest the exclusion of LGBT from the U.S. armed services. The Los Angeles group held a 15-car motorcade, which has been identified as the nation's first gay pride parade.[53] The National Transsexual Counseling Unit is created, becoming the first transgender organization ever and first American transgender organization ever.[54] The Society for Individual Rights opened America’s first gay and lesbian community center.
1966年-玛塔修因学会在纽约市的朱利叶斯·巴尔（Julius Bar）举行“ S饮”活动，挑战纽约州酒业管理局禁止向同性恋者提供酒类的行为；成立了全国同性恋者组织计划会议（该组织于当年下半年成为北美同性恋者组织北美会议）。康普顿咖啡馆骚乱发生在1966年8月，由变性妇女和先锋队成员在旧金山的田德隆区进行。这次事件是美国历史上最早记录的跨性别骚乱之一，比纽约市更著名的1969年石墙骚乱提前了三年。建立Vanguard是为了证明平等权利。 1966年1月，莉莉·文森兹（Lilli Vincenz）成为第一个出现在女同性恋杂志《 The Ladder》封面上的女同性恋者。一个同性恋组织联盟组织了“武装部队日”游行，以抗议将LGBT排除在美国武装部队之外。洛杉矶小组举行了15辆汽车的车队，这被确定为美国第一个同性恋游行。[53]成立了国家变性咨询小组，成为有史以来第一个变性组织和有史以来第一个美国变性组织。[54]个人权利协会开设了美国第一个同性恋社区中心。

- 1967 – The Black Cat Tavern in the Silver Lake neighborhood of Los Angeles is raided on New Year's Day by 12 plainclothes police officers who beat and arrested employees and patrons. The raid prompted a series of protests that began on 5 January 1967, organized by P.R.I.D.E. (Personal Rights in Defense and Education). It's the first use of the term "Pride" that came to be associated with LGBT rights.

- 1967 – The Advocate was first published in September as "The Los Angeles Advocate," a local newsletter alerting gay men to police raids in Los Angeles gay bars.
1967年-倡导者于9月份首次发行，名为“洛杉矶倡导者”，这是当地的新闻通讯，提醒男同性恋者在洛杉矶同性恋酒吧内对警察进行突袭。

- 1967 – The Sexual Offences Act 1967 decriminalised homosexual acts between two men over 21 years of age in private in England and Wales.;[55] The act did not apply to Scotland, Northern Ireland nor the Channel Islands; The book Homosexual Behavior Among Males by Wainwright Churchill breaks ground as a scientific study approaching homosexuality as a fact of life and introduces the term "homoerotophobia", a possible precursor to "homophobia"; The Oscar Wilde Bookshop, the world's first homosexual-oriented bookstore, opens in New York City; A raid on the Black Cat Tavern in Los Angeles, California promotes homosexual rights activity. The Student Homophile League at Columbia University is the first institutionally recognized gay student group in the United States.[citation needed]
1967年– 1967年的《性犯罪法》将英格兰和威尔士的两个21岁以上的男性之间的同性恋行为定为非刑事犯罪。[55]该法案不适用于苏格兰，北爱尔兰或海峡群岛； 韦恩·赖特·丘吉尔（Wainwright Churchill）撰写的《男性同性恋行为》一书作为一项将同性恋作为生活事实的科学研究而开创了先例，并引入了“同性恋恐惧症”一词，这可能是“同性恋恐惧症”的前兆； 奥斯卡·王尔德（Oscar Wilde）书店是世界上第一家以同性恋为导向的书店，在纽约市开业。 突袭加利福尼亚州洛杉矶的黑猫酒馆促进了同性恋权利活动。 哥伦比亚大学的学生嗜好者联盟是美国第一个获得机构认可的同性恋学生团体。[需要引用]

- 1967 –Grupo Nuestro Mundo (English: "Our World Group") is formed in Greater Buenos Aires, the first gay rights organization in Argentina and Latin America.[56][57] The Homosexuals, a 1967 episode of the documentary television series CBS Reports, was the first network documentary dealing with the topic of homosexuality.
1967年–格鲁波·纽埃斯特罗·蒙多（Grupo Nuestro Mundo）（英语：“ Our World Group”）在大布宜诺斯艾利斯成立，这是阿根廷和拉丁美洲第一个同性恋权利组织。[56] [57] 同性恋，是纪录片电视连续剧CBS Reports 1967年的一集，是第一部涉及同性恋话题的网络纪录片。

- 1968 – Paragraph 175 is eased in East Germany decriminalizing homosexual acts over the age of 18; Bulgaria decriminalizes adult homosexual relations. In Los Angeles, following the arrest of two patrons in a raid, The Patch owner Lee Glaze organized the other patrons to move on the police station. After buying out a nearby flower shop, the demonstrators caravanned to the station, festooned it with the flowers and bailed out the arrested men.[53] According to the online encyclopedia glbtq.com, "In the aftermath of the riot at Compton's, a network of transgender social, psychological, and medical support services was established, which culminated in 1968 with the creation of the National Transsexual Counseling Unit [NTCU], the first such peer-run support and advocacy organization in the world".[58]
1968年–东德放宽了第175段，将18岁以上的同性恋行为合法化； 保加利亚将成人同性恋关系合法化。 在洛杉矶，在一次突袭中逮捕了两名顾客之后，The Patch的老板李·格拉兹（Lee Glaze）组织了其他顾客在警察局继续前进。 示威者买断附近的一家花店后，乘车游行到车站，用鲜花把花彩装饰起来，并保释被捕人员。[53] 根据在线百科全书glbtq.com的说法，“在康普顿骚乱之后，建立了跨性别，社会，心理和医疗支持服务网络，并最终于1968年成立了国家变性咨询小组（NTCU）。 ，这是世界上第一个这样的由同伴经营的支持和倡导组织。[58]

- 1969 – The Stonewall riots occur in New York City; Paragraph 175 is eased in West Germany; Bill C-150 is passed, decriminalizing homosexuality in Canada. Pierre Trudeau, the Prime Minister of Canada, is quoted as having said: "The state has no place in the bedrooms of the nation."; Poland decriminalizes homosexual prostitution; An Australian arm of the Daughters of Bilitis forms in Melbourne and is considered Australia's first homosexual rights organisation.[citation needed] The Gay Liberation Front is formed in America, and it is the first gay organization to use "gay" in its name. On 31 December 1969, the Cockettes perform for the first time at the Palace Theatre on Union and Columbus in the North Beach neighborhood of San Francisco.
1969年–斯通沃尔暴动发生在纽约市； 第175段在西德放宽； C-150法案获得通过，将加拿大的同性恋合法化。 引用加拿大总理皮埃尔·特鲁多（Pierre Trudeau）的话说：“国家在国家的卧室里没有位置。” 波兰将同性恋卖淫合法化； 同性恋者女儿的澳大利亚分支机构在墨尔本成立，被认为是澳大利亚第一个同性恋权利组织。[盖伊解放阵线]在美国成立，并且是第一个以“同性恋”为名的同性恋组织。 1969年12月31日，歌舞剧院首次在旧金山北滩附近的联合剧院和哥伦布宫剧院演出。


###1970s[edit]

- 1970 – The first Gay Liberation Day March is held in New York City; The first LGBT Pride Parade is held in New York; The first "Gay-in" held in San Francisco; Carl Wittman writes A Gay Manifesto;[59][60] CAMP (Campaign Against Moral Persecution) is formed in Australia;[61][62] The Task Force on Gay Liberation formed within the American Library Association. Now known as the GLBT Round Table, this organization is the oldest LGBTQ professional organization in the United States.[63] In November, the first gay rights march occurs in the UK at Highbury Fields following the arrest of an activist from the Young Liberals for importuning.[64]
1970年–首届同性恋解放日游行在纽约市举行； 首届LGBT骄傲游行在纽约举行； 在旧金山举行的第一次“同性恋聚会”； 卡尔·威特曼（Carl Wittman）撰写了《同性恋宣言》； [59] [60]在澳大利亚成立了CAMP（反对道德迫害运动）； [61] [62]在美国图书馆协会内部成立了同性恋解放专责小组。 现在被称为GLBT圆桌会议，该组织是美国历史最悠久的LGBTQ专业组织。[63] 11月，在英国，一名年轻的自由主义者因举足轻重而被捕，一名同性恋人士被捕，这是英国首次在海布里球场举行同性恋游行。[64]

- 1971 – Campaign Against Moral Persecution (CAMP) is officially established on 6 February 1971, at the first public gathering of gay women and men in Australia, which takes place in a church hall in Balmain,[65] Society Five (a homosexual rights organization) is formed in Melbourne, Australia; Homosexuality is decriminalized in Austria, Costa Rica and Finland; Colorado and Oregon repeal sodomy laws; Idaho repeals the sodomy law — Then re-instates the repealed sodomy law because of outrage among Mormons and Catholics.[66][67] The Netherlands changes the homosexual age of consent to 16, the same as the straight age of consent; The U.S. Libertarian Party calls for the repeal of all victimless crime laws, including the sodomy laws; Dr. Frank Kameny becomes the first openly gay candidate for the United States Congress; The University of Michigan establishes the first collegiate LGBT programs office, then known as the "Gay Advocate's Office." The UK Gay Liberation Front (GLF) was recognized as a political movement in the national press and was holding weekly meetings of 200 to 300 people.;[68] George Klippert, the last man jailed for homosexuality in Canada, is released from prison. Ken Togo ran for national election, in Japan. During a UCLA conference called "The Homosexual in America," Betty Berzon became the first psychotherapist in the country to come out as gay to the public.[69] Boys in the Sand was the first gay porn film to include credits, to achieve crossover success, to be reviewed by Variety,[70] and one of the earliest porn films, after 1969's Blue Movie[71][72][73][74] by Andy Warhol, to gain mainstream credibility, preceding 1972's Deep Throat by nearly a year. It was promoted with an advertising campaign unprecedented for a pornographic feature, premiered in New York City in 1971 and was an immediate critical and commercial success.[75] The Alice B. Toklas Democratic Club, founded in San Francisco in 1971, was the first gay Democratic club of the United States.
1971年 – 反对道德迫害运动（CAMP）于1971年2月6日在澳大利亚的男女同性恋者首次公开集会上正式成立，该集会在巴尔曼的教堂礼堂举行，[65]第五协会（同性恋权利组织）在澳大利亚墨尔本成立；奥地利、哥斯达黎加和芬兰将同性恋合法化；科罗拉多州和俄勒冈州废除了鸡奸法；爱达荷州废除了鸡奸法，然后由于摩门教徒和天主教徒之间的愤怒而恢复了废除的鸡奸法。[66] [67]荷兰将同意的同性恋年龄更改为16岁，与同意的直接年龄相同；美国自由党呼吁废除所有无害犯罪法，包括鸡奸法；弗兰克·卡梅尼（Frank Kameny）博士成为美国国会的首位公开同性恋候选人；密歇根大学建立了第一个大学LGBT计划办公室，然后被称为“同性恋倡导者办公室”。英国同性恋解放阵线（GLF）在全国新闻界被认为是政治运动，每周举行200至300人的会议。[68]乔治·克里珀特（George Klippert）是加拿大最后一名因同性恋而入狱的人，已被释放出狱。肯·多哥在日本参加大选。在加州大学洛杉矶分校的一次名为“美国同性恋者”的会议上，贝蒂·贝松（Betty Berzon）成为美国第一位以同性恋身份向公众露面的心理治疗师。[69] Boys in the Sand是第一部获得好评的同性恋色情影片，取得了跨界成功，该片由Variety审查，[70]是继1969年安迪·沃霍尔（Andy Warhol）的Blue Movie [71] [72] [73]之后获得主流信誉的最早的色情影片之一，它比1972年发行的《深喉》（Deep Throat）早了将近一年。 它以广告活动进行了推广，其色情内容史无前例，1971年在纽约市首次亮相，并立即取得了重要的商业上的成功。[75]爱丽丝·托克拉斯民主俱乐部于1971年在旧金山成立，是美国第一家同性恋民主俱乐部。

- 1972 – Sweden becomes the first country in the world to allow transsexuals to legally change their sex, and provides free hormone therapy;[76] Hawaii legalizes homosexuality; In South Australia, a consenting adults in private-type legal defence was introduced; Norway decriminalizes homosexuality; East Lansing, Michigan and Ann Arbor, Michigan and San Francisco, California become the first cities in United States to pass a homosexual rights ordinance. Jim Foster, of San Francisco and Madeline Davis, of Buffalo, New York, the first openly gay and lesbian delegates to the Democratic Convention, give the first speeches advocating a gay rights plank in the Democratic Party Platform. "Stonewall Nation", the first gay anthem is written and recorded by Madeline Davis and is produced on 45 rpm record by the Mattachine Society of the Niagara Frontier. Lesbianism 101, the first lesbianism course in the U.S., is taught at the University of Buffalo by Margaret Small and Madeline Davis. Queens, NY schoolteacher Jeanne Manford marched with her gay son, gay rights activist Morty Manford, in New York's Christopher Street Liberation Day march. This was the origin of the straight ally movement and of PFLAG - (originally Parents of Gays, then Parents, Families and Friends of Lesbians and Gays, now simply PFLAG.)[77] Nancy Wechsler became the first openly gay or lesbian person in political office in America; she was elected to the Ann Arbor City Council in 1972 as a member of the Human Rights Party and came out as a lesbian during her first and only term there.[78] Also in 1972, Camille Mitchell became the first open lesbian to be awarded custody of her children in a divorce case, although the judge restricted the arrangement by precluding Ms. Mitchell's lover from moving in with her and the children.[79] Freda Smith became the first openly lesbian minister in the Metropolitan Community Church (she was also their first female minister).[80][81] Beth Chayim Chadashim was founded in 1972 as the first LGBT synagogue in the world, and the first LGBT synagogue recognized by the Union for Reform Judaism.[82] A Quaker group, the Committee of Friends on Bisexuality, issued the "Ithaca Statement on Bisexuality" supporting bisexuals.[83] The Statement, which may have been "the first public declaration of the bisexual movement" and "was certainly the first statement on bisexuality issued by an American religious assembly," appeared in the Quaker Friends Journal and The Advocate in 1972.[84][85][86] Today Quakers have varying opinions on LGBT people and rights, with some Quaker groups more accepting than others.[87] The first gay bar in San Francisco to have clear windows was Twin Peaks Tavern, which removed its blacked-out windows in 1972. Jack Fritscher’s book Popular Witchcraft Straight from the Witch's Mouth, the first book to investigate gay Wicca and witchcraft, was published.[88]
1972年–瑞典成为世界上第一个允许变性人合法改变性别并提供免费荷尔蒙疗法的国家； [76]夏威夷将同性恋合法化；在南澳大利亚州，引入了自愿的成年人进行私人法律辩护；挪威将同性恋合法化；密歇根州东兰辛，密歇根州安阿伯和加利福尼亚州旧金山成为美国第一批通过同性恋法令的城市。旧金山的吉姆·福斯特（Jim Foster）和纽约州布法罗的马德琳·戴维斯（Madeline Davis）是民主党大会的首批公开同性恋代表，他们在民主党纲要中首次倡导同性恋权利。第一个同性恋国歌“ Stonewall Nation”由马德琳·戴维斯（Madeline Davis）撰写和录制，并由尼亚加拉边疆国Mattachine Society以45 rpm的速度录制。女同性恋主义101是美国第一门女同性恋主义课程，由玛格丽特·史密斯（Margaret Small）和玛德琳·戴维斯（Madeline Davis）在布法罗大学教授。纽约州皇后区的女教师让娜·曼福德（Jeanne Manford）与她的同性恋儿子，同性恋权利活动家莫蒂·曼福德（Morty Manford）在纽约的克里斯托弗街解放日游行中游行。这是纯盟友运动和全美橄榄球联盟的起源-（最初是男同性恋的父母，然后是女同性恋和男同性恋的父母，家庭和朋友，现在简称为PFLAG。）[77]南希·韦斯勒成为政治上第一个公开的男同性恋者在美国的办公室；她当选为安阿伯市议会于1972年作为人权党成员，第一她在出来作为女同性恋者，且仅任期。[78]同样在1972年，卡米尔·米切尔（Camille Mitchell）成为第一位在离婚案中被判处其子女监护权的公开女同性恋者，尽管法官通过阻止米切尔女士的情人与她和孩子一起搬家限制了安排。[79]弗雷达·史密斯（Freda Smith）成为大都会社区教会的第一位公开女同性恋部长（她也是他们的第一位女部长）。[80] [81]贝丝·查伊姆·查戴西姆（Beth Chayim Chadashim）成立于1972年，是世界上第一个LGBT犹太教堂，也是改革犹太联盟认可的第一个LGBT犹太教堂。[82]一个Quaker团体，即双性恋之友委员会，发布了“ Ithaca关于双性恋的声明”，以支持双性恋。[83]该声明可能是“双性恋运动的第一个公开声明”，并且“肯定是美国宗教议会发表的关于双性恋的第一个声明”，发表于1972年的《贵格会友杂志》和《提倡者》。[84] [ 85] [86]如今，贵格会成员对LGBT人士和权利的看法有所不同，一些贵格会组织比其他人更能接受。[87]旧金山第一家拥有清晰窗户的同性恋酒吧是Twin Peaks Tavern，该酒吧于1972年将其涂黑的窗户拆除了。杰克弗里茨彻（Jack Fritscher）的书《从巫婆的嘴里直截了当的巫术》是第一本研究同性恋巫术和巫术的书。 [88]

- 1973 – On 15 October the Australian and New Zealand College of Psychiatry Federal Council declares homosexuality not an illness – the first such body in the world to do so; in December the American Psychiatric Association removes homosexuality from its Diagnostic and Statistical Manual of Mental Disorders (DSM-II), based largely on the research and advocacy of Evelyn Hooker. The first formal meeting of PFLAG took place on 26 March 1973 at the Metropolitan-Duane Methodist Church in Greenwich Village (now the Church of the Village). Approximately 20 people attended, including founder Jeanne Manford, her husband Jules, son Morty, Dick and Amy Ashworth, Metropolitan Community Church founder Reverend Troy Perry, and more.[77] Malta legalizes homosexuality; In West Germany, the age of consent is reduced for homosexuals to 18 (though it is 14 for heterosexuals).[citation needed]; Sally Miller Gearhart became the first open lesbian to obtain a tenure-track faculty position when she was hired by San Francisco State University, where she helped establish one of the first women and gender study programs in the country.[89] Lavender Country, an American country music band, released a self-titled album which is the first known gay-themed album in country music history.[90]
1973年– 10月15日，澳大利亚和新西兰精神病学院联邦委员会宣布同性恋不是一种疾病-世界上第一个这样做的机构；去年12月，美国精神病学协会主要根据伊夫琳·胡克（Evelyn Hooker）的研究和主张，从其《精神障碍诊断和统计手册》（DSM-II）中删除了同性恋。 PFLAG的第一次正式会议于1973年3月26日在格林威治村的大都会杜安卫理公会教堂（现为村里的教堂）举行。大约有20人参加，包括创始人Jeanne Manford，她的丈夫Jules，儿子Morty，Dick和Amy Ashworth，大都会社区教会创始人Reverend Troy Perry牧师等等。[77]马耳他将同性恋合法化；在西德，同性恋者的同意年龄降低为18岁（异性恋者为14岁）。萨利·米勒·吉尔哈特（Sally Miller Gearhart）在被旧金山州立大学聘用时，成为首位获得终身任职教职职位的公开女同性恋者，在那里她帮助建立了美国首批女性和性别研究计划之一。[89]美国乡村音乐乐队薰衣草乡村（Lavender Country）发行了同名专辑，这是乡村音乐历史上第一个以同性恋为主题的专辑。[90]

- 1974 – Chile allows a trans person to legally change her name and gender on the birth certificate after undergoing sex reassignment surgery, becoming the second country in the world to do so.[91] Kathy Kozachenko becomes the first openly gay American elected to public office when she wins a seat on the Ann Arbor, Michigan city council; In New York City Dr. Fritz Klein founds the Bisexual Forum, the first support group for the Bisexual Community; Elaine Noble becomes the second openly gay American elected to public office when she wins a seat in the Massachusetts State House; Inspired by Noble, Minnesota state legislator Allan Spear comes out in a newspaper interview; Ohio repeals sodomy laws. Robert Grant founds American Christian Cause to oppose the "gay agenda", the beginning of modern Christian politics in America. In London, the first openly LGBT telephone help line opens, followed one year later by the Brighton Lesbian and Gay Switchboard;[citation needed] the Brunswick Four are arrested on 5 January 1974, in Toronto, Ontario. This incident of Lesbophobia galvanizes the Toronto Lesbian and Gay community;[92] the National Socialist League (The Gay Nazi Party) is founded in Los Angeles, California.[citation needed] The first openly gay or lesbian person to be elected to any political office in America was Kathy Kozachenko, who was elected to the Ann Arbor City Council in April 1974.[93] Also in 1974, the Lesbian Herstory Archives opened to the public in the New York apartment of lesbian couple Joan Nestle and Deborah Edel; it has the world's largest collection of materials by and about lesbians and their communities.[94] Also in 1974, Angela Morley became the first openly transgender person to be nominated for an Academy Award, when she was nominated for one in the category of Best Music, Original Song Score/Adaptation for The Little Prince (1974), a nomination shared with Alan Jay Lerner, Frederick Loewe, and Douglas Gamley. The world's first gay softball league was formed in San Francisco in 1974 as the Community Softball League, which eventually included both women's and men's teams. The teams, usually sponsored by gay bars, competed against each other and against the San Francisco Police softball team.[95]
1974年–智利允许跨性别人士在进行性别重新分配手术后合法更改其出生证明上的姓名和性别，成为世界上第二个这样做的国家。[91]凯西·科萨切恩科成为第一个公开同性恋美国当选公职时，她赢得了密歇根州安阿伯市议会的座位; Fritz Klein博士在纽约市成立了“双性恋论坛”，这是双性恋社区的第一个支持小组。伊莱恩·诺布尔成为第二个公开同性恋身份的美国当选时，她胜在马萨诸塞州议会大厦的座位公职;明尼苏达州州议员艾伦·斯皮尔（Allan Spear）受诺布尔（Noble）的启发，接受了报纸采访俄亥俄州废除了鸡奸法。罗伯特·格兰特（Robert Grant）建立美国基督教事业反对“同性恋议程”，这是美国现代基督教政治的开端。在伦敦，第一条公开的LGBT电话求助热线开通，一年后，布赖顿女同性恋和同性恋总机局（Brighton Lesbian and Gay Switchboard）开通； [需要引证]不伦瑞克四人于1974年1月5日在安大略省多伦多被捕。同性恋恐惧症的这一事件激起了多伦多的男女同性恋社区； [92]在加利福尼亚州洛杉矶成立了全国社会主义同盟（同性恋纳粹党）。在美国的办公室是凯西·科萨切恩科，谁当选为安阿伯市议会于1974年4月。[93]也是在1974年，女同性恋者赫斯特里档案馆在女同性恋夫妇琼·雀巢和黛博拉·埃德尔的纽约公寓中向公众开放；它拥有世界上最大的女同性恋者及其社区资料。[94]同样是在1974年，安吉拉·莫利（Angela Morley）成为第一个获得奥斯卡金像奖提名的公开变性人，当时她被提名为最佳音乐，《小王子的原始乐谱/改编》（1974）中的一个奖项，该提名与艾伦·杰伊·勒纳（Alan Jay Lerner），弗雷德里克·洛伊（Frederick Loewe）和道格拉斯·甘利（Douglas Gamley）。 1974年在旧金山成立了世界上第一个同性恋垒球联盟，称为社区垒球联盟，该联盟最终包括男女球队。通常由同性恋酒吧赞助的球队互相竞争，并与旧金山警察垒球队竞争。[95]

- 1975 – Twelve women became the first group of women in Japan to publicly identify as lesbians, publishing one issue of a magazine called Subarashi Onna (Wonderful Women).[96] Homosexuality is legalized in California due to the Consenting Adult Sex Bill, authored by and successfully lobbied for in the state legislature by State Assemblyman from San Francisco Willie Brown; Leonard Matlovich, a Technical Sergeant in the United States Air Force, becomes the first U.S. gay service member to purposely out himself to fight their ban; South Australia becomes the first state in Australia to make homosexuality legal between consenting adults in private. Panama is the third country in the world to allow transsexuals who have gone through gender reassignment surgery to get their personal documents reflecting their new sex;[citation needed] UK journal Gay Left begins publication;[97] Minneapolis becomes the first city in the United States to pass trans-inclusive civil rights protection legislation;[98] Clela Rorex, a clerk in Boulder County, Colorado, issues the first same-sex marriage licenses in the United States, issuing the very first of them to Dave McCord and Dave Zamora, on 26 March 1975.[99] Six same-sex marriages were performed as a result of her giving out licenses, but all of the marriages were overturned later that year.[99] Maureen Colquhoun becomes the UK's first out lesbian MP after coming out in 1975. She is the UK's first openly gay MP. Gay American Indians, the first gay American Indian liberation organization, is founded.[100]
1975年-十二名女性成为日本第一批被公开认定为女同性恋的女性，并出版了一期名为《 Subarashi Onna》（《杰出女性》）的杂志。[96]由于《成年人同意性法案》是由加利福尼亚州议员从旧金山威利·布朗起草并在州议会中成功游说的，因此同性恋在加利福尼亚州合法化；美国空军技术警长伦纳德·马特洛维奇（Leonard Matlovich）成为第一个故意出击自己的禁令的美国同性恋服务人员；南澳大利亚州成为澳大利亚第一个将成年人之间自愿进行同性恋合法化的州。巴拿马是世界上第三个允许经过变性手术的变性人获取反映其新性别的个人文件的国家； [需要引用]英国期刊《同性恋左派》开始出版； [97]明尼阿波利斯成为美国第一个城市州将通过跨性别的民权保护立法； [98]科罗拉多州博尔德县的书记员克莱拉·罗雷克斯（Clela Rorex）在美国发行了第一批同性婚姻许可证，并向戴夫·麦考德和戴夫·萨莫拉颁发了第一张同性婚姻许可证。 ，1975年3月26日。[99]由于她发放了执照，进行了六次同性婚姻，但是当年晚些时候所有婚姻都被推翻了。[99]毛琳·科尔昆（Maureen Colquhoun）于1975年问世后成为英国首位女同性恋议员。她是英国首位公开同性恋女议员。成立了第一个同性恋美洲印第安人解放组织同性恋美洲印第安人。[100]

- 1976 – Robert Grant founds the Christian Voice to take his anti-homosexual-rights crusade national in United States; the Homosexual Law Reform Coalition and the Gay Teachers Group are started in Australia; the Australian Capital Territory decriminalizes homosexuality between consenting adults in private and equalizes the age of consent; Out Minnesota state legislator Allan Spear is reelected; and Denmark equalizes the age of consent.[citation needed] Association of homosexual liberation was founded in Japan.[101] Tom Gallagher became the first United States Foreign Service officer to come out as gay; he quit the Foreign Service after that, as he would have been unable to obtain a security clearance.[102][103][104] Patricia Nell Warren's third novel, The Fancy Dancer (1976) was the first bestseller to portray a gay priest and to explore gay life in a small town.
1976年-罗伯特·格兰特（Robert Grant）创立了基督教之声，将他的反同性恋权利运动带到美国。 在澳大利亚成立了同性恋法律改革联盟和同性恋教师团体； 澳大利亚首都特区将私下同意的成年人之间的同性恋合法化，并实现了同意年龄的平等化；走出明尼苏达州议员阿伦·斯皮尔再次当选; [丹麦]平等同意的年龄。[需要引用]日本成立了同性恋解放协会。[101] 汤姆·加拉格尔（Tom Gallagher）成为美国第一位以同性恋身份出任的美国外交官。 他此后退出了外交部门，因为他本来无法获得安全许可。[102] [103] [104] 帕特里夏·内尔·沃伦（Patricia Nell Warren）的第三本小说《幻想的舞者》（The Fancy Dancer，1976年）是第一位描写同性恋神父并探索小镇上同性恋生活的畅销书。

-1977 – Harvey Milk is elected city-county supervisor in San Francisco, becoming the first openly gay or lesbian candidate elected to political office in California, the seventh openly gay/lesbian elected official nationally, and the third man to be openly gay at time of his election. Dade County, Florida enacts a Human Rights Ordinance; it is repealed the same year after a militant anti-homosexual-rights campaign led by Anita Bryant. Quebec becomes the first jurisdiction larger than a city or county in the world to prohibit discrimination based on sexual orientation in the public and private sectors; Croatia, Montenegro, Slovenia and Vojvodina legalise homosexuality.[citation needed] Welsh author Jeffrey Weeks publishes Coming Out;[105]
1977年 - 米尔克当选的城市县主管在旧金山，成为当选为加州政治职务的第一位公开同性恋候选人，第七同性恋/女同性恋民选官员在全国范围内，第三人是同性恋在时间他的当选。 佛罗里达州达德县制定了《人权条例》； 在安妮塔·布莱恩特（Anita Bryant）领导的好战的反同性恋权利运动之后的同一年，该法案被废除了。 魁北克成为世界上第一个禁止城市或县以上的司法管辖区，禁止在公共和私营部门基于性取向的歧视； 克罗地亚，黑山，斯洛文尼亚和伏伊伏丁那使同性恋合法化。[需要引文]威尔士作家杰弗里·怀克斯（Jeffrey Weeks）出版《出来》； [105]
    
    Publication of the first issue of Gaysweek, NYC's first mainstream gay weekly. Police raided a house outside of Boston outraging the gay community. In response the Boston-Boise Committee was formed.[106] Anne Holmes became the first openly lesbian minister ordained by the United Church of Christ;[107] Ellen Barrett became the first openly lesbian priest ordained by the Episcopal Church of the United States (serving the Diocese of New York).[108][109] The first lesbian mystery novel in America was published; it was Angel Dance, by Mary F. Beal.[110][111] The National Center for Lesbian Rights was founded. Shakuntala Devi published the first[112] study of homosexuality in India.[113][114] Platonica Club and Front Runners were founded in Japan.[101] San Francisco hosted the world's first gay film festival in 1977.[115] Peter Adair, Nancy Adair and other members of the Mariposa Film Group premiered the groundbreaking documentary on coming out, Word Is Out: Stories of Some of Our Lives, at the Castro Theater in 1977. The film was the first feature-length documentary on gay identity by gay and lesbian filmmakers.[116][117] Beth Chayim Chadashim became the first LGBT synagogue to own its own building.[82] On March 26, 1977, Frank Kameny and a dozen other members of the gay and lesbian community, under the leadership of the then-National Gay Task Force, briefed then-Public Liaison Midge Costanza on much-needed changes in federal laws and policies. This was the first time that gay rights were officially discussed at the White House.[118][119]
纽约市首个主流同性恋周刊《同性恋周刊》的第一期发行。警方突袭了波士顿郊外的一所房屋，激怒了同性恋社区。作为回应，成立了波士顿博伊西委员会。[106]安妮·霍姆斯（Anne Holmes）成为由联合基督联合会任命的第一位公开女同性恋牧师； [107]埃伦·巴雷特（Ellen Barrett）成为由美国主教教堂（为纽约教区服务）任命的第一位公开女同性恋神父。[108] [109] ]美国出版了第一部女同性恋神秘小说。这是玛丽·比尔（Mary F. Beal）的《天使之舞》。[110] [111]成立了国家女同性恋权利中心。 Shakuntala Devi发表了印度第一项关于同性恋的研究[112]。[113] [114]在日本成立了柏拉图俱乐部和领跑者。[101]旧金山于1977年举办了世界上第一个同性恋电影节。[115]彼得·阿黛尔（Peter Adair），南希·阿黛尔（Nancy Adair）和马里波萨电影集团的其他成员在1977年的卡斯特罗剧院首映了开创性的纪录片《 Word Is Out：我们生活中的故事》。这部电影是第一部关于同性恋的纪录片男女电影制片人的身份。[116] [117]贝丝·查伊姆·查戴西姆（Beth Chayim Chadashim）成为第一个拥有自己建筑物的LGBT犹太教堂。[82] 1977年3月26日，弗兰克·卡梅尼（Frank Kameny）和其他十二个同性恋团体在当时的全国同性恋特别工作组（Gay Task Force）的领导下，向当时的公众联络官Midge Costanza简要介绍了联邦法律和政策的迫切需要。这是白宫首次正式讨论同性恋权利。[118] [119]

- 1978 – San Francisco Supervisor Harvey Milk and Mayor George Moscone are assassinated by former Supervisor Dan White; the first Sydney Gay and Lesbian Mardi Gras is held, with 2000 people attending and 53 subsequently arrested and some seriously beaten by police. The rainbow flag is first used as a symbol of gay pride; Sweden establishes a uniform age of consent. Samois, the earliest known lesbian-feminist BDSM organization, is founded in San Francisco; well-known members of the group include Patrick Califia and Gayle Rubin; the group is among the very earliest advocates of what came to be known as sex-positive feminism[citation needed]; The International Lesbian and Gay Association (ILGA) is established.[120] Robin Tyler became the first out lesbian on U.S. national television, appearing on a Showtime comedy special hosted by Phyllis Diller. The same year she released her comedy album, Always a Bridesmaid, Never a Groom, the first comedy album by an out lesbian.[121] The San Francisco Lesbian/Gay Freedom Band was founded by Jon Reed Sims in 1978 as the San Francisco Gay Freedom Day Marching Band and Twirling Corp. Upon its founding in 1978, it became the first openly gay musical group in the world. San Francisco became the first city in America to have a recruitment drive for gay police officers, bringing in over 350 applications.[122] Allen Bennett became the first openly gay rabbi in the United States.[123]
1978年–旧金山主管Harvey Milk和市长George Moscone被前主管Dan White暗杀；首届悉尼同性恋狂欢节举行，有2000人参加，随后53人被捕，其中一些人遭到警察殴打。彩虹旗首先被用作同性恋自豪感的象征；瑞典规定了统一的同意年龄。萨摩亚（Samois）是最早知名的女同性恋BDSM组织，成立于旧金山；该组织的知名成员包括Patrick Califia和Gayle Rubin；该组织是最早被称为性别阳性女权主义[需要引用]的倡导者之一；成立了国际男女同性恋协会（ILGA）。[120]罗宾·泰勒（Robin Tyler）成为美国国家电视台上首位女同志，出现在菲利斯·迪勒（Phyllis Diller）主持的Showtime喜剧特别节目中。同年，她发行了喜剧专辑《永远的伴娘，永不新郎》，这是外出女同性恋的第一张喜剧专辑。[121]旧金山女同性恋/同性恋自由乐队由乔恩·里德·西姆斯（Jon Reed Sims）于1978年成立，当时是旧金山同性恋自由日游行乐队和特威林公司（Twirling Corp）。1978年成立后，它成为了世界上第一个公开开放的同性恋音乐团体。旧金山成为美国第一个对同性恋警察进行招聘的城市，带来了350多份申请。[122]艾伦·贝内特（Allen Bennett）成为美国第一位公开同性恋同性恋者。[123]

- 1979 – The first national homosexual rights march on Washington, D.C. is held; The White Night riots occur, Harry Hay issues the first call for a Radical Faerie gathering in Arizona, and The Sisters of Perpetual Indulgence first appear in public on Easter Sunday in San Francisco.[124] Cuba and Spain decriminalize homosexuality.[citation needed] Esta Noche, a gay bar located at 3079 16th & Mission Street in San Francisco, was the first gay Latino bar in San Francisco, and first opened in 1979. A number of people in Sweden called in sick with a case of being homosexual, in protest of homosexuality being classified as an illness. This was followed by an activist occupation of the main office of the National Board of Health and Welfare. Within a few months, Sweden became the first country in the world to remove homosexuality as an illness.[76] Japan Gay Center was established in Japan.[101] Grady Quinn and Randy Rohl became the first known gay couple to attend a high school prom when they attended the Lincoln High School prom in Sioux Falls, South Dakota on May 23, 1979.[125]
1979年–在华盛顿特区举行了第一次全国同性恋权利游行；发生“白色之夜”骚乱，哈里·海（Harry Hay）首次呼吁在亚利桑那州召开激进仙灵聚会，而“永恒放纵的姐妹们”（Sisters of Perpetual Indulgence）则首先在旧金山的复活节星期天公开露面。[124]古巴和西班牙将同性恋合法化。[Esta Noche]是一家同性恋酒吧，位于旧金山16th＆Mission Street 3079号，是旧金山的第一家同性恋拉丁裔酒吧，并于1979年首次开业。瑞典的许多人都叫因同性恋而生病的人，抗议将同性恋归类为疾病。随后是国家健康和福利委员会总办公室的激进主义者职业。在几个月内，瑞典成为世界上第一个消除同性恋病的国家。[76]日本同性恋中心在日本成立。[101]格雷迪·奎因和兰迪·罗尔于1979年5月23日参加了南达科他州苏福尔斯的林肯高中毕业舞会，成为参加高中毕业舞会的第一对同性恋夫妇。[125]


###1980s[edit]

- 1980 – The United States Democratic Party becomes the first major political party in the U.S. to endorse a homosexual rights platform plank; Scotland decriminalizes homosexuality; The Human Rights Campaign Fund is founded by Steve Endean; The Human Rights Campaign is America's largest civil rights organization working to achieve lesbian, gay, bisexual and transgender equality.[126] Lionel Blue becomes the first British rabbi to come out as gay;[127] "Becoming Visible: The First Black Lesbian Conference" is held at the Women's Building, from October 17 to 19, 1980. It has been credited as the first conference for African-American lesbian women.[128] The Socialist Party USA nominates an openly gay man, David McReynolds, as its (and America's) first openly gay presidential candidate in 1980.[129]
1980年-美国民主党成为美国第一个认可同性恋权利平台的主要政党； 苏格兰将同性恋合法化； 人权运动基金会由史蒂夫·恩迪恩（Steve Endean）创立； 人权运动是美国最大的民权组织，致力于实现女同性恋，男同性恋，双性恋和变性者的平等。[126] 莱昂内尔·布鲁（Lionel Blue）成为第一位以同性恋身份出现的英国拉比； [127]“变得可见：第一位黑人女同性恋会议”于1980年10月17日至19日在妇女大楼举行。 非洲裔美国女同性恋妇女。[128] 美国社会党提名一位公开的同性恋男子大卫·麦克雷诺兹（David McReynolds）为1980年（也是美国）第一位公开的同性恋总统候选人。[129]

- 1981 – The European Court of Human Rights in Dudgeon v United Kingdom strikes down Northern Ireland's criminalisation of homosexual acts between consenting adults, leading to Northern Ireland decriminalising homosexual sex the following year; Victoria (Australia) and Colombia decriminalize homosexuality with a uniform age of consent; The Moral Majority starts its anti-homosexual crusade; Norway becomes the first country in the world to enact a law to prevent discrimination against homosexuals; Hong Kong's first sex-change operation is performed. The first official documentation of the condition to be known as AIDS was published by the US Centers for Disease Control and Prevention (CDC) on 5 June 1981.[130] Tennis player Billie Jean King became the first prominent professional athlete to come out as a lesbian, when her relationship with her secretary Marilyn Barnett became public in a May 1981 "palimony" lawsuit filed by Barnett.[131] Due to this she lost all of her endorsements.[132] Mary C. Morgan became the first openly gay or lesbian judge in America when she was appointed by California Governor Jerry Brown to the San Francisco Municipal Court.[133] Randy Shilts was hired as a national correspondent by the San Francisco Chronicle, becoming "the first openly gay reporter with a gay 'beat' in the American mainstream press."[134] A Sergeant of the New York City Police Department (Charles H. Cochrane Jr.) came out as gay during a city council hearing. This made him the first New York City Police Department member to publicly announce his homosexuality.[135]
1981年–欧洲人权法院在Dudgeon诉英国一案中，将北爱尔兰将同意的成年人之间的同性恋行为定为刑事犯罪，导致北爱尔兰在第二年将同性恋性行为合法化；维多利亚州（澳大利亚）和哥伦比亚以统一的同意年龄将同性恋合法化；道德多数派开始了反对同性恋的运动。挪威成为世界上第一个颁布法律以防止歧视同性恋者的国家；香港首次进行变性手术。美国疾病控制与预防中心（CDC）于1981年6月5日发布了被称为艾滋病的第一份官方文件。[130]网球选手比利·简·金（Billie Jean King）在1981年5月由巴内特（Barnett）提起的“ palimony”诉讼中公开与女秘书玛丽莲·巴内特（Marilyn Barnett）的关系时，成为第一位以女同性恋身份出场的职业运动员。[131]因此，她失去了所有的认可。[132]玛丽·C·摩根（Mary C. Morgan）被加利福尼亚州州长杰里·布朗（Jerry Brown）任命为旧金山市法院法官时，成为美国第一位公开的同性恋法官。[133]兰迪·希尔茨（Randy Shilts）被《旧金山纪事报》（San Francisco Chronicle）聘为国民记者，成为“美国主流媒体中第一位公开公开接受同性恋“殴打”的同性恋记者。” [134]纽约市警察局警长（查尔斯·H·小科克伦（Cochrane Jr.）在市议会听证会上以同性恋身份露面。这使他成为纽约市警察局第一位公开宣布其同性恋的人。[135]

- 1982 – Laguna Beach, CA elects the first openly gay mayor in United States history; France equalizes the age of consent; The first Gay Games is held in San Francisco, attracting 1,600 participants; Northern Ireland decriminalizes homosexuality; Wisconsin becomes the first US state to ban discrimination against homosexuals; New South Wales becomes the first Australian state to outlaw discrimination on the basis of actual or perceived homosexuality. The condition to be known as AIDS had acquired a number of names – GRID5 (gay-related immune deficiency), ‘gay cancer’, ‘community-acquired immune dysfunction’ and ‘gay compromise syndrome’[136] The CDC used the term AIDS for the first time in September 1982, when it reported that an average of one to two cases of AIDS were being diagnosed in America every day.[137] Ken Togo founded the Deracine Party in Japan. New York City police sergeant Charles H. Cochrane Jr. and former Fairview, New Jersey sergeant Sam Ciccone formed the first group targeted at the needs of gay members of law enforcement, the Gay Officers Action League (GOAL).[138] Portugal decriminalizes homosexuality for the second time in its history.
1982  - 拉古纳海滩，CA选举在美国历史上的第一位公开同性恋市长;法国等于同意年龄。首届同性恋运动会在旧金山举行，吸引了1600名参与者；北爱尔兰将同性恋合法化；威斯康星州成为美国第一个禁止歧视同性恋者的州。新南威尔士州成为第一个将基于实际或感知到的同性恋歧视的澳大利亚州。被称为艾滋病的疾病有许多名称-GRID5（同性恋相关免疫缺陷），“同性恋癌症”，“社区获得性免疫功能障碍”和“同性恋妥协综合症” [136] CDC使用了艾滋病一词这是1982年9月的第一次，当时它报告说，每天在美国平均诊断出一到两例艾滋病。[137] Ken Togo在日本创立了Deracine党。纽约警察中士查尔斯·H·科克伦和前新泽西州费尔维尤中士山姆·西科尼组成了第一个针对同性恋执法人员的团体，即同性恋军官行动联盟（GOAL）。[138]葡萄牙有史以来第二次将同性恋合法化。

- 1983 – Massachusetts Representative Gerry Studds reveals he is gay on the floor of the House, becoming the first openly gay member of Congress; Guernsey (Including Alderney, Herm and Sark) decriminalizes homosexuality; AIDS is described as a "gay plague" by Reverend Jerry Falwell; BiPOL, the first and oldest bisexual political organization, is founded in San Francisco by bisexual activists Autumn Courtney, Lani Ka'ahumanu, Arlene Krantz, David Lourea, Bill Mack, Alan Rockway, and Maggi Rubenstein; Governor Jerry Brown appoints Herb Donaldson as the first openly gay male municipal court judge in the State of California;[139][140] David Scondras becomes the first openly gay official elected to the Boston City Council.[141]
1983年-马萨诸塞州代表格里·斯图兹（Gerry Studds）透露自己在众议院同志，成为国会的首位公开同性恋会员； 根西岛（包括Alderney，Herm和Sark）将同性恋合法化； 杰里·佛威尔牧师将艾滋病描述为“同性恋瘟疫”。 BiPOL是第一个也是最古老的双性恋政治组织，由双性恋活动家Autumn Courtney，Lani Ka'ahumanu，Arlene Krantz，David Lourea，Bill Mack，Alan Rockway和Maggi Rubenstein在旧金山成立。州长杰里·布朗任命赫伯·唐纳森在加利福尼亚州的第一个公开同性恋男性市级法院法官;[139] [140]大卫Scondras成为当选为波士顿市议会的第一个公开同性恋身份的官员[141]

- 1984 – The lesbian and gay association "Ten Percent Club" is formed in Hong Kong; Massachusetts voters reelect representative Gerry Studds, despite his revealing himself as homosexual the year before; New South Wales and the Northern Territory in Australia make homosexual acts legal; Chris Smith, newly elected to the UK parliament declares: "My name is Chris Smith. I'm the Labour MP for Islington South and Finsbury, and I'm gay", making him the first openly out homosexual male politician in the UK parliament (Maureen Colquhoun being the first openly out homosexual politician in the UK in 1975). The Argentine Homosexual Community (Comunidad Homosexual Argentina, CHA) is formed uniting several different and preexisting groups. Berkeley, California becomes the first city in the U.S. to adopt a program of domestic partnership health benefits for city employees; West Hollywood, CA is founded and becomes the first known city to elect a city council where a majority of the members are openly gay or lesbian. Reconstructionist Judaism became the first Jewish denomination to allow openly gay and lesbian rabbis and cantors.[142] ILGA Japan is founded in Japan. On Our Backs, the first women-run erotica magazine and the first magazine to feature lesbian erotica for a lesbian audience in the United States, was first published in 1984 by Debi Sundahl and Myrna Elana, with the contributions of Susie Bright, Nan Kinney, Honey Lee Cottrell, Dawn Lewis, Happy Hyder, Tee Corinne, Jewelle Gomez, Judith Stein, Joan Nestle, and Patrick Califia.[143] In 1984, BiPOL sponsored the first bisexual rights rally, outside the Democratic National Convention in San Francisco. The rally featured nine speakers from civil rights groups allied with the bisexual movement.
1984年–在香港成立了男女同性恋协会“百分之十俱乐部”；马萨诸塞州选民改选代表格里·施图兹，尽管他在今年显露自己是同性恋的前;澳大利亚的新南威尔士州和北领地将同性恋行为合法化；克里斯·史密斯，新当选为英国国会宣称：“我的名字是克里斯·史密斯我是工党议员伊斯灵顿南部和芬斯伯里，和我是同性恋。”，这使他成为第一位公开出男同性恋者政客在英国议会（莫琳·科尔昆（Maureen Colquhoun）是1975年英国第一位公开露面的同性恋政治家）。阿根廷同性恋社区（Comunidad Homosexual Argentina，CHA）是由几个不同的既存群体组成的。加利福尼亚州伯克利市成为美国第一个对城市雇员采用家庭合作伙伴健康福利计划的城市；加利福尼亚州西好莱坞成立，并成为第一个选出市议会的知名城市，在该议会中，大多数成员公开为同性恋。重建主义的犹太教成为第一个公开允许男女同性恋拉比和康托舞者的犹太教派。[142] ILGA Japan在日本成立。在《背靠背》上，Debi Sundahl和Myrna Elana于1984年首次出版了第一本由女性经营的情色杂志，并且是第一本为美国的女同性恋读者介绍女同性恋情色的杂志，由Susie Bright，Nan Kinney，亲爱的李·科特雷尔，黎明·刘易斯，海德·海德，蒂·科林妮，珠宝·戈麦斯，朱迪思·斯坦，琼·雀巢和帕特里克·卡里菲亚。[143] 1984年，BiPOL赞助了在旧金山举行的民主党全国代表大会之外的首次双性恋权利集会。集会上有来自与两性运动结盟的民权团体的九名发言人。

- 1985 – France prohibits discrimination based on lifestyle (moeurs) in employment and services; the first memorial to gay Holocaust victims is dedicated; Belgium equalizes the age of consent; the Restoration Church of Jesus Christ (informally called the Gay Mormon Church) is founded by Antonio A. Feliz.[144] Actor Rock Hudson dies of AIDS. He is the first major public figure known to have died from an AIDS-related illness.[145] Terry Sweeney becomes Saturday Night Live's first openly gay male cast member; Sweeney was "out" prior to being hired as a cast member.[146] The Bisexual Resource Center (BRC) in Massachusetts has served the bisexual community since 1985. Liverpool-based soap opera, Brookside featured the first openly gay character on a British TV series this year.[147]
1985年-法国禁止在就业和服务中基于生活方式（潮湿）的歧视； 纪念同性恋大屠杀受害者的第一座纪念馆； 比利时等于同意年龄； 安东尼奥·费利斯（Antonio A. Feliz）创立了耶稣基督恢复教堂（非正式地称为盖摩尔门教堂）。[144] 演员洛克·哈德森（Rock Hudson）因艾滋病去世。 他是已知因艾滋病相关疾病死亡的第一位主要公众人物。[145] 特里·斯威尼（Terry Sweeney）成为《周六夜现场》（Saturday Night Live）的首位公开同性恋男演员。 理发师在被聘为演员之前是“退出”的。[146] 自1985年以来，马萨诸塞州的双性恋资源中心（BRC）一直为双性恋社区提供服务。布鲁克塞德的肥皂剧，布鲁克赛德今年是英国电视连续剧中首位公开露面的同性恋角色。[147]

- 1986 – Homosexual Law Reform Act passed in New Zealand, legalizing sex between males over 16; Haiti decriminalizes homosexuality, June in Bowers v. Hardwick case, U.S. Supreme Court upholds Georgia law forbidding oral or anal sex, ruling that the constitutional right to privacy does not extend to homosexual relations, but it does not state whether the law can be enforced against heterosexuals. Becky Smith and Annie Afleck became the first openly lesbian couple in America granted legal, joint adoption of a child.[148] From 1 till 3 May, the 1986, ILGA Asia Conference took place in Japan's capital Tokyo.[149] The Dutch Remonstrants are the world's first Christian denomination to perform same-sex unions and marriages.[150] Canadian province Ontario passes anti-discrimination legislation. Hill Street Blues featured the first lesbian recurring character on a major network; the character was a police officer called Kate McBride, played by Lindsay Crouse.[151]

1986年–新西兰通过了《同性恋法律改革法》，将16岁以上男性之间的性行为合法化； 海地将同性恋合法化，6月在Bowers诉Hardwick案中，美国最高法院维持佐治亚州禁止口交或肛交的法律，裁定宪法的隐私权不延伸至同性恋关系，但未说明该法律是否可以针对 异性恋。 贝基·史密斯（Becky Smith）和安妮·阿弗莱克（Annie Afleck）成为美国第一批合法联合收养儿童的公开女同性恋夫妇。[148] 5月1日至3日，1986年的ILGA亚洲会议在日本首都东京举行。[149] 荷兰示威者是世界上第一个进行同性婚姻和婚姻的基督教教派。[150] 加拿大安大略省通过反歧视立法。 希尔街布鲁斯（Hill Street Blues）是主要网络上的第一个女同性恋重复性角色； 角色是林赛·克鲁斯（Lindsay Crouse）饰演的名叫凯特·麦克布赖德（Kate McBride）的警官。[151]

- 1987 – AIDS Coalition to Unleash Power(ACT-UP) founded in the US in response to the US government's slow response in dealing with the AIDS crisis.[152] ACT UP stages its first major demonstration, seventeen protesters are arrested; U.S. Congressman Barney Frank comes out. Boulder, Colorado citizens pass the first referendum to ban discrimination based on sexual orientation.[153][154] In New York City a group of Bisexual LGBT rights activist including Brenda Howard found the New York Area Bisexual Network (NYABN); Homomonument, a memorial to persecuted homosexuals, opens in Amsterdam. David Norris is the first openly gay person to be elected to public office in the Republic of Ireland. A group of 75 bisexuals marched in the 1987 March On Washington For Gay and Lesbian Rights, which was the first nationwide bisexual gathering. The article "The Bisexual Movement: Are We Visible Yet?", by Lani Ka'ahumanu, appeared in the official Civil Disobedience Handbook for the March. It was the first article about bisexuals and the emerging bisexual movement to be published in a national lesbian or gay publication.[155] Canadian province of Manitoba and territory Yukon ban sexual orientation discrimination.
1987年-为响应美国政府在应对艾滋病危机方面反应迟钝而在美国成立的艾滋病释放力量联盟（ACT-UP）。[152] ACT UP进行了第一次大规模示威，逮捕了十七名抗议者；美国国会议员Barney Frank出来了。科罗拉多州博尔德市公民通过第一次公投，禁止基于性取向的歧视。[153] [154]在纽约市，包括布伦达·霍华德（Brenda Howard）在内的一群双性恋LGBT维权人士创建了纽约地区双性恋网络（NYABN）；同性恋者纪念碑，在阿姆斯特丹揭幕。大卫·诺里斯是爱尔兰共和国的当选公职的第一公开同性恋人。 1987年3月，由75名双性恋者组成的游行队伍在华盛顿举行，这是全国首例双性恋聚会。拉尼·卡阿曼鲁努（Lani Ka'ahumanu）撰写的文章“双性恋运动：我们还能看见吗？”出现在3月的官方《公民抗命手册》中。这是关于双性恋者和新兴双性恋运动的第一篇文章，将在全国的男女同性恋出版物上发表。[155]加拿大曼尼托巴省和育空地区禁止性取向歧视。

- 1988 – Sweden is the first country to pass laws protecting homosexual regarding social services, taxes, and inheritances. The anti-gay Section 28 passes in England and Wales; Scotland enacts almost identical legislation; Canadian MP Svend Robinson comes out; Canada lowers the age of consent for sodomy to 18; Belize and Israel decriminalize (de jure) sodomy and sexual acts between men (the relevant section in the old British-mandate law from 1936 was never enforced in Israel). After losing an Irish High Court case (1980) and an Irish Supreme Court case (1983), David Norris takes his case (Norris v. Ireland) to the European Court of Human Rights. The European Court strikes down the Irish law criminalising male-to-male sex on the grounds of privacy. Stacy Offner became the first openly lesbian rabbi hired by a mainstream Jewish congregation, Shir Tikvah Congregation of Minneapolis (a Reform Jewish congregation).[156][157] Robert Dover became the first openly gay Olympic athlete when he came out in 1988.[158]
1988年-瑞典是第一个通过法律保护同性恋者有关社会服务，税收和继承的国家。第28条反对同性恋者法案在英格兰和威尔士通过；苏格兰颁布了几乎相同的立法；加拿大国会议员Svend Robinson出庭；加拿大将鸡奸的同意年龄降低到18岁；伯利兹和以色列将男子之间的鸡奸和性行为合法化（法律上合法）（1936年的旧英国授权法中的相关部分从未在以色列执行）。在败诉爱尔兰高等法院案（1980）和爱尔兰最高法院案（1983）之后，大卫·诺里斯（David Norris）将他的案子（诺里斯诉爱尔兰）移交给欧洲人权法院。欧洲法院以私隐为由，废除了爱尔兰将男女同性恋定为犯罪的爱尔兰法律。斯泰西·奥弗纳（Stacy Offner）成为主流犹太人教会明尼阿波利斯（Shire Tikvah）教会（改革的犹太人教会）雇用的第一位公开女同性恋拉比。[156] [157]罗伯特·多佛（Robert Dover）于1988年问世，成为第一位公开露面的同性恋奥林匹克运动员。[158]

- 1989 – Western Australia decriminalizes male homosexuality (but the age of consent is set at 21); Liechtenstein legalizes homosexuality; Denmark is the first country in the world to enact registered partnership laws (like a civil union) for same-sex couples, with most of the same rights as marriage (excluding the right to adoption (until June 2010) and the right to marriage in a church).
1989年-西澳大利亚州将男性同性恋合法化（但同意年龄定为21岁）； 列支敦士登使同性恋合法化； 丹麦是世界上第一个针对同性伴侣制定注册伴侣关系法（例如民事婚姻法）的国家，该法律具有与婚姻大部分相同的权利（不包括领养权（至2010年6月）和 教堂）。

- 1989 - Braschi v Stahl Associates Co was a landmark case in which the New York court ruled that a gay couple that had lived together for a decade could be considered a family under New York City's rent control regulations. The decision in favor of litigant Miguel Braschi said that protection against eviction "should not rest on fictitious legal distinctions or genetic history, but instead should find its foundation in the reality of family life."[159][160][161]
1989年-Braschi诉Stahl Associates Co是一个具有里程碑意义的案子，纽约法院裁定，根据纽约市的租金管制规定，生活在一起十年的同性恋夫妇可以被视为一家人。 赞成诉讼人米格尔·布拉斯基（Miguel Braschi）的决定说，防止驱逐“不应基于虚假的法律区别或遗传历史，而应在家庭生活的现实中找到基础。” [159] [160] [161]


###1990s[edit]

- 1991 4th October, Gary Doyle born Carpenterstown (Actually blanch) infamous drag queen and co parent to Louis
1991年10月4日，加里·多伊尔（Gary Doyle）出生于卡彭特斯敦（实际上是一伙人）

    - Equalization of age of consent: Czechoslovakia (see Czech Republic, Slovakia)
    同意年龄均等：捷克斯洛伐克（请参阅捷克共和国，斯洛伐克）

    - Decriminalisation of homosexuality: UK Crown Dependency of Jersey and the Australian state of Queensland
    同性恋非刑事化：泽西岛和澳大利亚昆士兰州的英国王室依赖

    - LGBT Organizations founded: BiNet USA (USA), OutRage! (UK), Queer Nation (USA), The Humsafar Trust (India)
    LGBT组织成立：美国BiNet，OutRage！ （英国），酷儿国家（美国），Humsafar Trust（印度）

    - Homosexuality no longer an illness: The World Health Organization
    同性恋不再是一种疾病：世界卫生组织

    - Other: Justin Fashanu is the first professional footballer to come out in the press.
    其他：贾斯汀·法沙奴（Justin Fashanu）是第一位出现在新闻界的职业足球运动员。

    - Reform Judaism decided to allow openly lesbian and gay rabbis and cantors.[162]
    改革犹太教决定公开允许同性恋和拉比教士。[162]

    - Dale McCormick became the first open lesbian elected to a state Senate (she was elected to the Maine Senate).[163]
    戴尔·麦考密克成为第一个开放女同志当选为州参议院（她当选为缅因州参议院）。[163]

    - In 1990, the Union for Reform Judaism announced a national policy declaring lesbian and gay Jews to be full and equal members of the religious community. Its principal body, the Central Conference of American Rabbis (CCAR), officially endorsed a report of their committee on homosexuality and rabbis. They concluded that "all rabbis, regardless of sexual orientation, be accorded the opportunity to fulfill the sacred vocation that they have chosen" and that "all Jews are religiously equal regardless of their sexual orientation."
    1990年，犹太人改革联合会宣布了一项国家政策，宣布男女同性恋者是宗教团体的正式成员。 它的主要机构美国拉比斯中央会议（CCAR）正式批准了其同性恋和拉比委员会的报告。 他们得出结论：“所有犹太教徒，不论其性取向如何，都应有机会实现他们所选择的神圣职业”，并且，“所有犹太人在宗教上都是平等的，而不论其性取向如何。”

    - The oldest national bisexuality organization in the United States, BiNet USA, was founded in 1990. It was originally called the North American Multicultural Bisexual Network (NAMBN), and had its first meeting at the first National Bisexual Conference in America.[164][164][165] This first conference was held in San Francisco in 1990, and sponsored by BiPOL. Over 450 people attended from 20 states and 5 countries, and the mayor of San Francisco sent a proclamation "commending the bisexual rights community for its leadership in the cause of social justice," and declaring June 23, 1990 Bisexual Pride Day.
    美国最古老的国家双性恋组织BiNet USA成立于1990年。它最初被称为北美多元文化双性恋网络（NAMBN），并在美国第一次全国双性恋会议上首次召开会议。[164] [ 164] [165] 第一次会议于1990年在旧金山举行，由BiPOL赞助。 来自20个州和5个国家的450多人参加了会议，旧金山市长发表声明“赞扬双性恋社区在社会正义事业中的领导地位”，并宣布1990年6月23日为“双性恋自豪日”。
    
    - The first Eagle Creek Saloon, that opened on the 1800 block of Market Street in San Francisco in 1990 and closed in 1993, was the first black-owned gay bar in the city.[166]
    第一家鹰溪轿车于1990年在旧金山市场街1800号街区开业，并于1993年关闭，是该市第一家黑人拥有的同性恋酒吧。[166]

- 1991

    - Decriminalisation of homosexuality: Bahamas, Hong Kong and Ukraine
    同性恋非刑事化：巴哈马，香港和乌克兰
    
    - AIDS Related: The red ribbon is first used as a symbol of the campaign against HIV/AIDS.
    与艾滋病相关：红丝带首先被用作对抗艾滋病毒/艾滋病的运动的象征。
    
    - Anti-discrimination legislation: Canadian province Nova Scotia (sexual orientation)
    反歧视立法：加拿大新斯科舍省（性取向）
    
    - Sherry Harris was elected to the City Council in Seattle, Washington, making her the first openly lesbian African-American elected official.[167]
    雪莉·哈里斯当选为市议会在西雅图，华盛顿，使她成为第一位公开同性恋非洲裔民选官员。[167]
    
    - The first lesbian kiss on television occurred; it was on L.A. Law between the fictional characters of C.J. Lamb (played by Amanda Donohoe) and Abby (Michele Greene).[168]
    电视上的第一个女同性恋之吻发生了。 [168]在C.J. Lamb（由Amanda Donohoe饰演）和Abby（Michele Greene）的虚构角色之间是根据洛杉矶法律制定的。[168]
    
    - The Chicago Gay and Lesbian Hall of Fame was created in June 1991.[169] The hall of fame is the first "municipal institution of its kind in the United States, and possibly in the world."[169]
    芝加哥男女同性恋名人堂成立于1991年6月。[169] 名人堂是“美国乃至世界上第一个此类的市政机构。” [169]
    
    - The AB101 Veto Riot occurs in California in response to then-Mayor of California Pete Wilson vetoing the Assembly Bill 101, a bill that would prohibit employers from discriminating against employees because of their sexual orientation.[170]
    AB101否决权骚乱发生在加利福尼亚州，是对当时的加利福尼亚州市长皮特·威尔逊否决《大会法案101》的一项回应，该法案将禁止雇主因性取向而歧视雇员。[170]

- 1992

    - Equalization of age of consent: Iceland, Luxembourg and Switzerland
    同意年龄均等：冰岛，卢森堡和瑞士
    
    - Decriminalisation of homosexuality: Estonia and Latvia
    同性恋非刑事化：爱沙尼亚和拉脱维亚
    
    - Repeal of Sodomy laws: UK Crown Dependency of Isle of Man (homosexuality still illegal until 1994)
    废除鸡奸法律：马恩岛的英国王室依附关系（直到1994年，同性恋仍然是非法的）
    
    - End to ban on gay people in the military: Australia, Canada
    结束军人同性恋禁令：澳大利亚，加拿大
    
    - Recriminalisation of homosexuality: Nicaragua (until Mar 2008)
    同性恋再犯罪化：尼加拉瓜（至2008年3月）
    
    - Anti-discrimination legislation: Canadian provinces New Brunswick and British Columbia (sexual orientation)
    反歧视立法：加拿大新不伦瑞克省和不列颠哥伦比亚省（性取向）
    
    - Althea Garrison was elected as the first transgender state legislator in America, and served one term in the Massachusetts House of Representatives; however, it was not publicly known she was transgender when she was elected.[171]
    西娅·加里森当选为美国第一个跨州议员，并在代表马萨诸塞众院一个期限;然而，它并未公开，她是变性人时，她当选。[171]
    
    - The Lesbian Avengers was founded in New York City by Ana María Simo, Sarah Schulman, Maxine Wolfe, Anne-Christine d'Adesky, Marie Honan, and Anne Maguire
    女同性恋复仇者联盟由安娜·玛丽亚·西莫（AnaMaríaSimo），莎拉·舒尔曼（Sarah Schulman），马克西·沃尔夫（Maxine Wolfe），安妮·克里斯汀·德阿比（Anne-Christine d'Adesky），玛丽·霍南（Anne Mananre）和安妮·马奎尔（Anne Maguire）在纽约成立
    
    - Tokyo International Lesbian & Gay Film Festival was held in Japan.
    东京国际男女同性恋电影节在日本举行。
    
    - Then-Mayor of California Pete Wilson signs the Assembly Bill 101, prohibiting employers in California from discriminating against employees based on their sexual orientation.[172]
    当时的加利福尼亚州市长皮特·威尔逊（Pete Wilson）签署了第101号大会法案，禁止加利福尼亚州的雇主基于性取向歧视雇员。[172]

- 1993
    - Civil Union/Registered Partnership laws:
    公民联盟/注册合伙法：
    
        - Passed and Came into effect: Norway (without adoption until 2009, replaced with same-sex marriage in 2008/09)
        通过并生效：挪威（在2009年之前尚未收养，在2008/09年被同性婚姻取代）
        
    - Repeal of Sodomy laws: Australian Territory of Norfolk Island
    废除鸡奸法律：诺福克岛的澳大利亚领土
    
    - Decriminalisation of homosexuality: Belarus, UK Crown Dependency of Gibraltar, Ireland, Lithuania, Russia (with the exception of the Chechen Republic);
    同性恋非刑事化：白俄罗斯，英国直布罗陀，爱尔兰，立陶宛，俄罗斯的王室依存关系（车臣共和国除外）；
    
    - Anti-discrimination legislation:
    反歧视立法：
    
        - US state of Minnesota (gender identity)
        美国明尼苏达州（性别认同）
        
        - New Zealand parliament passes the Human Rights Amendment Act which outlaws discrimination on the grounds of sexual orientation or HIV
        新西兰议会通过了《人权修正案》，该法禁止基于性取向或艾滋病毒的歧视
        
        - Canadian province Saskatchewan (sexual orientation)
        加拿大省萨斯喀彻温省（性取向）
        
    - End to ban on gay people in the military: New Zealand
    结束对军人同性恋的禁令：新西兰
    
    - Significant LGBT Murders: Brandon Teena
    重大LGBT谋杀案：布兰登·蒂娜（Brandon Teena）
    
    - Melissa Etheridge came out as a lesbian.
    梅利莎·埃瑟里奇（Melissa Etheridge）是一名女同性恋。
    
    - The Triangle Ball was held; it was the first inaugural ball in America to ever be held in honor of gays and lesbians.
    举行了三角舞会； 这是美国有史以来第一个为纪念同性恋而举行的开幕舞会。
    
    - The first Dyke March (a march for lesbians and their straight female allies, planned by the Lesbian Avengers) was held, with 20,000 women marching.[173][174]
    举行了第一届堤防行军（由女同性恋复仇者组织的一次针对女同性恋者及其直女盟友的游行），有20,000名妇女游行。[173] [174]
    
    - Roberta Achtenberg became the first openly gay or lesbian person to be nominated by the president and confirmed by the U.S. Senate when she was appointed to the position of Assistant Secretary for Fair Housing and Equal Opportunity by President Bill Clinton.[175]
    罗伯塔·阿希滕贝格（Roberta Achtenberg）成为总统提名的第一个公开的同性恋者，并在美国总统比尔·克林顿（Bill Clinton）被任命为公平住房和机会均等助理部长时得到美国参议院的确认。[175]
    
    - Lea DeLaria was "the first openly gay comic to break the late-night talk-show barrier" with her 1993 appearance on The Arsenio Hall Show.[176]
    莉亚·德拉里亚（Lea DeLaria）1993年在Arsenio Hall Show露面时，是“第一个打破深夜脱口秀节目障碍的公开同性恋漫画。” [176]
    
    - In December 1993 Lea DeLaria hosted Comedy Central's Out There, the first all-gay stand-up comedy special.[176]
    1993年12月，莉亚·德拉里亚（Lea DeLaria）主持了喜剧中心的外出活动，这是第一个全同性恋单口喜剧特别节目。[176]
    
    - Before the "Don't Ask Don't Tell" policy was enacted in 1993, lesbians and bisexual women and gay men and bisexual men were banned from serving in the military.[177] In 1993 the "Don't Ask Don't Tell" policy was enacted, which mandated that the military could not ask servicemembers about their sexual orientation.[178][179] However, until the policy was ended in 2011 service members were still expelled from the military if they engaged in sexual conduct with a member of the same sex, stated that they were lesbian, gay, or bisexual, and/or married or attempted to marry someone of the same sex.[180]
    在1993年“不问不告诉”政策颁布之前，女同性恋者和双性恋女性以及男同性恋者和双性恋男人被禁止参军。[177] 1993年颁布了“不问不告诉”政策，该政策规定军方不得向军人询问其性取向。[178] [179] 但是，直到2011年政策终止之时，如果服务人员与同性成员发生性行为，他们仍会被逐出军方，称他们是女同性恋，男同性恋或双性恋，并且/或者已婚或试图结婚。 同性。[180]
        
        
- 1994

    - Unregistered Cohabitation recognition:
    未注册的同居识别：
        
        - Passed and Came into effect: Israel (without adoption, without step-adoption until 2005)
        通过并生效：以色列（未收养，直到2005年才逐步收养）
        
    - Anti-discrimination legislation: South Africa (sexual orientation, interim constitution)
    反歧视立法：南非（性取向，临时宪法）
    
    - Decriminalisation of homosexuality: Bermuda, UK Crown Dependency of Isle of Man and Serbia, Commonwealth of Australia
    同性恋非刑事化：百慕大，英国英属马恩岛和塞尔维亚的王室依赖，澳大利亚联邦
    
    - Equalization of age of consent:
    同意年龄均等：
        
        - Partial: UK reduces the age of consent for homosexual men to 18;
        部分：英国将同性恋男子的同意年龄降低至18岁；
    
    - Homosexuality no longer an illness: American Medical Association
    同性恋不再是一种疾病：美国医学会
    
    - LGBT Organizations founded: National Coalition for Gay and Lesbian Equality (South Africa)
    成立了LGBT组织：全国男女平等联盟（南非）
    
    - Other : Canada grants refugee status to homosexuals fearing for their well-being in their native country; Toonen v. Australia decided by UN Human Rights Committee; fear of persecution due to sexual orientation becomes grounds for asylum in the United States.[181]
    其他：加拿大向担心自己的国家幸福的同性恋者授予难民身份； 联合国人权事务委员会裁定Toonen诉澳大利亚案； 由于性取向而遭受迫害的恐惧在美国成为庇护的理由。[181]
    
    - Deborah Batts became the first openly gay or lesbian federal judge; she was appointed to the U.S. District Court in New York.[182][183]
    黛博拉·巴茨（Deborah Batts）成为第一位公开宣布同性恋的联邦法官； 她被任命为纽约美国地方法院的法官。[182] [183]
    
    - The Gay Asian Pacific Alliance and Asian Pacific Sister joined the Chinese New Year Parade in San Francisco, which was the first time that queer Asian American communities had attended in a publicly ethnic activity.[184]
    亚太同性恋同盟和亚太姐妹参加了旧金山的农历新年游行，这是同志美国亚裔社区首次参加公开种族活动。[184]
    
    - Gay Parade was held in Japan. (8.1994)
    同性恋游行在日本举行。 （1994年8月）
    
    - Wilson Cruz became the first gay actor to play an openly gay character in a leading role in a television series (My So-Called Life).
    威尔逊·克鲁兹（Wilson Cruz）成为第一位在电视连续剧《我所谓的生活》（My So-Called Life）中扮演公开角色的男同性恋演员。
    
    - Steve Gunderson was outed as gay on the House floor by representative Bob Dornan (R-CA) during a debate over federal funding for gay-friendly curricula,[185] making him one of the first openly gay members of the U.S. Congress and the first openly gay U.S. Republican representative.[186]
    史蒂夫·冈德森（Steve Gunderson）在关于联邦资助同性恋友好课程的辩论中，由鲍勃·多南（Bob Dornan）（R-CA）在众议院以同性恋出庭，[185]使他成为美国国会第一批公开同性恋成员之一，也是第一位 公开表示美国同性恋共和党代表。[186]
    
    - Liverpool-based soap opera, Brookside, broadcast the UK's first pre-watershed lesbian kiss.[187]
    利物浦的肥皂剧布鲁克赛德播出了英国首个分水岭前的女同性恋之吻。[187]
    
    - The broadcast of Pedro Zamora and Sean Sasser's commitment ceremony in 1994, in which they exchanged vows, was the first such same-sex ceremony in television history, and is considered a landmark in the history of the medium.[188][189]
    佩德罗·扎莫拉（Pedro Zamora）和肖恩·萨瑟（Sean Sasser）在1994年所作的承诺仪式的转播是他们在电视上的誓言，这是电视史上第一次此类同性仪式，被认为是媒体历史上的一个里程碑。[188] [189]

-1995
        - Civil Union/Registered Partnership laws:
        公民联盟/注册合伙法：
        
            - Passed and Came into effect: Sweden (with adoption, replaced with same-sex marriage in April 2009)
            通过并生效：瑞典（收养，于2009年4月由同性婚姻取代）
            
        - Anti-discrimination legislation: Canadian province Newfoundland and Labrador (sexual orientation)
        反歧视立法：加拿大纽芬兰省和拉布拉多省（性取向）
        
        - Decriminalisation of homosexuality: Albania and Moldova
        同性恋非刑事化：阿尔巴尼亚和摩尔多瓦
        
        - AIDS Related: Triple combination therapy of drugs such as 3TC, AZT and ddC shown to be effective in treating HIV, the virus responsible for AIDS[190]
        与艾滋病相关：3TC，AZT和ddC等三联疗法可有效治疗艾滋病毒（一种引起艾滋病的病毒）[190]
        
        - LGBT Organizations founded: Gay Advice Darlington/Durham was founded by local gay and bisexual men, and has developed into a Charity that works with and for the Lesbian, Gay, Bisexual and Transgender (LGBT) Community of County Durham and Darlington.
        LGBT组织成立：同性恋咨询机构Darlington / Durham由当地的同性恋者和双性恋者创立，现已发展成为一个慈善团体，与达勒姆郡和达灵顿郡的女同性恋，同性恋，双性恋和变性者（LGBT）社区合作。
        
        - Other : The Human Rights Campaign drops the word "Fund" from their title and broadens their mission to promote "an America where gay, lesbian, bisexual and transgender people are ensured equality and embraced as full members of the American family at home, at work and in every community;"
        其他：人权运动从标题中删除“基金”一词，并扩大了其使命，以促进“确保同性恋，女同性恋，双性恋和变性者在美国得到平等并被接纳为美国家庭的正式成员的美国。 在每个社区中；”
        
        - Rachel Maddow became the first openly gay or lesbian American to win an international Rhodes Scholarship.
        Rachel Maddow成为第一个公开获得国际罗德奖学金的美国同性恋者。
        
        - Kings Cross Steelers, the world's first gay rugby club, was founded.[191]
        成立了世界上第一个同性恋橄榄球俱乐部国王十字钢人队。[191]
        
        - Rabbi Margaret Wenig's essay "Truly Welcoming Lesbian and Gay Jews" was published in The Jewish Condition: Essays on Contemporary Judaism Honoring Rabbi Alexander M. Schindler; it was the first published argument to the Jewish community on behalf of civil marriage for gay couples.[192]
        犹太教教士玛格丽特·韦尼格（Rabbi Margaret Wenig）的论文《真正欢迎男女同性恋》发表在《犹太人的状况：纪念犹太教的散文》上。 [192]这是代表犹太人以公证方式向犹太社区公开的第一个论点。[192]
        
        - Ed Flanagan served as Vermont's State Auditor from 1993 through 2001, becoming the first openly gay, statewide-elected official in the United States when he came out in 1995, before his 1996 reelection.[193]
        埃德·弗拉纳根曾担任佛蒙特州的状态审计员从1993年到2001年，成为第一个公开同性恋，全州当选美国官员时，他在1995年就出来了，他在1996年竞选连任之前。[193]
        
- 1996
        
        - Civil Union/Registered Partnership laws:
        公民联盟/注册合伙法：
            
            - Passed and Came into effect: Iceland (with step-adoption, without joint adoption until 2006, replaced with same-sex marriage in 2010)
        通过并生效：冰岛（逐步收养，直到2006年才共同收养，2010年由同性婚姻取代）
        
        - Unregistered Cohabitation recognition:
        未注册的同居识别：
        
            - Passed and Came into effect: Hungary (replaced with registered partnerships in 2009)
            通过并生效：匈牙利（2009年由注册合伙企业取代）
            
        - Restriction of LGBT partnership rights: USA, (federal, see DOMA)
        限制LGBT合作伙伴权利：美国，（联邦，请参见DOMA）
        
        - Equalization of age of consent: Burkina Faso
        同意年龄均等：布基纳法索
        
        - Decriminalisation of homosexuality: Romania, North Macedonia, Macau
        同性恋非刑事化：罗马尼亚，北马其顿，澳门
        
        - Anti-discrimination legislation: Canada (federal, sexual orientation)
        反歧视立法：加拿大（联邦，性取向）
        
        - The first lesbian wedding on television occurred, held for fictional characters Carol (played by Jane Sibbett) and Susan (played by Jessica Hecht) on the TV show "Friends".[194]
        电视上的第一次女同性恋婚礼是在电视节目《老友记》中为虚构人物卡罗尔（Carol Jane）和简·西贝特（Jessica Hecht）饰演的苏珊（Susan）举行的。[194]
        
        - The first openly gay speaker at a Republican National Convention was Log Cabin Republicans member Steve Fong of California in 1996.[195]
        在共和党全国代表大会上，第一位公开露面的同性恋演讲者是1996年加利福尼亚州的小木屋共和党人史蒂夫·方。[195]
        
        - Muffin Spencer-Devlin became the first LPGA player to come out as gay.[196]
        Muffin Spencer-Devlin成为第一位以同性恋身份露面的LPGA球员。[196]

- 1997
        
        - Anti-discrimination legislation: Fiji (sexual orientation, constitution) and South Africa (sexual orientation, constitution)
        反歧视立法：斐济（性取向，宪法）和南非（性取向，宪法）
        
        - Equalization of age of consent: Russia
        同意年龄均等：俄罗斯
        
        - Decriminalisation of homosexuality: Ecuador, Venezuela and the Australian state of Tasmania
        同性恋非刑事化：厄瓜多尔，委内瑞拉和澳大利亚塔斯马尼亚州
        
        - Other : Israeli President Ezer Weizman compares homosexuality to alcoholism in front of high school students.[197] The UK extends immigration rights to same-sex couples akin to marriage; Ellen DeGeneres came out as a lesbian, one of the first celebrities to do so.[198] Furthermore, later that year her character Ellen Morgan came out as a lesbian on the TV show Ellen, making Ellen DeGeneres the first openly lesbian actress to play an openly lesbian character on television.[199] Patria Jiménez became the first openly gay person to win a position in the Mexican Congress, doing so for the Party of the Democratic Revolution.[200] The Gay and Lesbian Medical Association launched the Journal of the Gay and Lesbian Medical Association, the world's first peer-reviewed, multi-disciplinary journal dedicated to LGBT health. The first open-mouth kiss between two women on prime time television occurred, on ABC's Relativity.[201]
        其他：以色列总统埃泽·韦兹曼（Ezer Weizman）将同性恋与酗酒者在高中生面前进行比较。[197] 英国将移民权利扩展到与婚姻相似的同性伴侣。 埃伦·德杰内雷斯（Ellen DeGeneres）是一位女同性恋者，是最早这样做的名人之一。[198] 此外，那年晚些时候，她的角色艾伦·摩根（Ellen Morgan）在电视节目《艾伦》中以女同性恋的身份出现，使艾伦·德杰内雷斯（Ellen DeGeneres）成为首位在电视上公开扮演女同性恋角色的女同性恋女演员。[199] 帕特里亚·希门尼斯（PatriaJiménez）成为民主革命党在墨西哥国会中首位公开露面的同性恋者。[200] 男女同性恋医学协会创办了《男女同性恋医学杂志》，这是世界上第一本致力于LGBT健康的同行评审，多学科杂志。 ABC的《相对论》在黄金时段电视上发生了两个女人之间的第一次开嘴之吻。[201]

- 1998
        
        - Anti-discrimination legislation: Ecuador (sexual orientation, constitution), Ireland (sexual orientation) and the Canadian provinces of Prince Edward Island (sexual orientation) and Alberta (court ruling only; legislation amended in 2009)
        反歧视立法：厄瓜多尔（性取向，宪法），爱尔兰（性取向）以及加拿大爱德华王子岛省（性取向）和艾伯塔省（仅法院裁决；该法于2009年修订）
        
        - Significant LGBT Murders: Rita Hester, Matthew Shepard
        重大LGBT谋杀案：Rita Hester，Matthew Shepard
        
        - Decriminalisation of homosexuality: Bosnia and Herzegovina, Kazakhstan, Kyrgyzstan, South Africa (retroactive to 1994), Republic of Cyprus and Tajikistan
        同性恋非刑事化：波斯尼亚和黑塞哥维那，哈萨克斯坦，吉尔吉斯斯坦，南非（追溯至1994年），塞浦路斯共和国和塔吉克斯坦
        
        - Equalization of age of consent: Croatia and Latvia
        同意年龄均等：克罗地亚和拉脱维亚
        
        - End to ban on gay people in the military: Romania, South Africa
        结束对军人同性恋的禁令：罗马尼亚，南非
        
        - Gender identity was added to the mission of Parents and Friends of Lesbians and Gays after a vote at their annual meeting in San Francisco.[202] Parents and Friends of Lesbians and Gays is the first national LGBT organization to officially adopt a transgender-inclusion policy for its work.[203]
        在旧金山举行的年会上投票表决后，性别认同被添加到男女同性恋者父母和朋友的使命中。[202] 男女同性恋者父母和朋友组织是第一个正式采用跨性别纳入政策的全国性LGBT组织。[203]
        
        - Tammy Baldwin became the first openly gay or lesbian non-incumbent ever elected to Congress, and the first open lesbian ever elected to Congress, winning Wisconsin's 2nd congressional district seat over Josephine Musser.[204][205]
        塔米·鲍德温成为第一位公开同性恋非现任曾经当选国会议员，并首开女同性恋曾当选为国会议员，赢得了约瑟芬·马瑟威斯康星州的第2个国会选区席位。[204] [205]
        
        - Dana International became the first transsexual to win the Eurovision Song Contest, representing Israel with the song "Diva".[206]
        达娜国际（Dana International）成为首位赢得欧洲歌唱大赛（Eurovision Song Contest）的变性人，代表以色列演唱了歌曲“迪瓦（Diva）”。[206]
        
        - Robert Halford comes out as being the first openly gay heavy metal musician.[207]
        罗伯特·霍尔福德（Robert Halford）成为第一位公开公开的同性恋重金属音乐家。[207]
        
        - The first bisexual pride flag was unveiled on 5 December 1998.[208]
        第一面双性恋骄傲旗帜于1998年12月5日揭幕。[208]
        
        - Julie Hesmondhalgh first began to play Hayley Anne Patterson, British TV's first transgender character.[209]
        朱丽·赫斯蒙达（Julie Hesmondhalgh）首先开始扮演英国电视台的第一个跨性别角色海莉·安妮·帕特森（Hayley Anne Patterson）。[209]
        
        - BiNet USA hosted the First National Institute on Bisexuality and HIV/AIDS.[210]
        美国BiNet主办了第一家双性恋和艾滋病毒/艾滋病国家研究所。[210]

- 1999
        
        - Civil Union/Registered Partnership laws:
        公民联盟/注册合伙法：
            
            - Passed and Came into effect: US State of California (without adoption, without step adoption until 2001, same-sex marriage in June 2008-November 2008)
            通过并生效：美国加利福尼亚州（未收养，直到2001年才逐步收养，2008年6月至2008年11月为同性婚姻）
            
            - Passed and Came into effect: France
            通过并生效：法国
        
        - Decriminalisation of homosexuality: Chile
        同性恋非刑事化：智利
        
        - Equalization of age of consent: Finland (without adoption)
        同意年龄均等：芬兰（未收养）
        
        - LGBT Organizations founded: "Queer Youth Alliance" (UK)
        LGBT组织成立：“ Queer青年联盟”（英国）
        
        - Other: Israel's supreme court recognizes a lesbian partner as another legal mother of her partner's biological son; South Africa grants spousal immigration benefits to same-sex partners.
        其他：以色列最高法院承认一名女同性恋伴侣是其伴侣亲生儿子的另一位合法母亲； 南非向同性伴侣提供配偶移民福利。
        
        - Steven Greenberg publicly came out as gay in an article in the Israeli newspaper Maariv. As he has a rabbinic ordination from the Orthodox rabbinical seminary of Yeshiva University (RIETS), he is generally described as the first openly gay Orthodox Jewish rabbi.[211] However, some Orthodox Jews, including many rabbis, dispute his being an Orthodox rabbi.[212]
        史蒂文·格林伯格（Steven Greenberg）在以色列报纸《 Maariv》上的一篇文章中公开以同性恋为生。 [211]由于他从耶希瓦大学（RIETS）的东正教拉比神学院获得拉比教令，因此他被普遍称为第一位公开同性恋的东正教犹太拉比。[211] 但是，一些东正教犹太人，包括许多拉比，对他是东正教拉比表示怀疑。[212]
        
        - The Transgender Day of Remembrance was founded in 1999 by Gwendolyn Ann Smith, a trans woman who is a graphic designer, columnist, and activist,[213] to memorialize the murder of Rita Hester in Allston, Massachusetts.[214] Since its inception, TDoR has been held annually on 20 November,[215] and it has slowly evolved from the web-based project started by Smith into an international day of action.
        跨性别纪念日由格温多琳·安·史密斯（Gwendolyn Ann Smith）于1999年创立，她是一名跨性别女人，是一名平面设计师，专栏作家和激进主义者，[213]是为了纪念在马萨诸塞州奥尔斯顿谋杀丽塔·海丝特的事件。[214] 自成立以来，TDoR每年于11月20日举行[215]，它已从由Smith发起的基于网络的项目逐渐演变为国际行动日。
        
        - In 1999, the first Celebrate Bisexuality Day was organized by Michael Page, Gigi Raven Wilbur, and Wendy Curry.[216]
        1999年，迈克尔·佩奇，吉吉·拉文·威尔伯和温迪·库里组织了首个庆祝双性恋日。[216]


###2000s

- 2000
        
        - Civil Union/Registered Partnership laws:
        公民联盟/注册合伙法：
            
            - Passed and Came into effect: US State of Vermont
            通过并生效：美国佛蒙特州
        
        - Anti-discrimination legislation: South Africa (discrimination, hate speech, harassment; see PEPUDA)
        反歧视立法：南非（歧视，仇恨言论，骚扰；请参阅PEPUDA）
        
        - Revoking of discrimination legislation: UK subdivision of Scotland (Section 28)
        废除歧视法：英国苏格兰分部（第28条）
        
        - End to ban on gay people in the military: United Kingdom. See also: Sexual orientation and military service and Stonewall
        结束对军方同性恋者的禁令：英国。 另请参阅：性取向和兵役以及石墙
        
        - Equalization of age of consent: Belarus, Israel and United Kingdom (passed eff. 2001)
        同意年龄均等：白俄罗斯，以色列和英国（自2001年起生效）
        
        - Decriminalisation of homosexuality: Azerbaijan and Georgia
        同性恋非刑事化：阿塞拜疆和格鲁吉亚
        
        - Other: In Germany the Bundestag officially apologizes to gays and lesbians persecuted under the Nazi regime, and for "harm done to homosexual citizens up to 1969"; Israel recognizes same-sex relations for immigration purposes for a foreign partner of an Israeli resident.
        其他：在德国，联邦议院正式向在纳粹政权下受到迫害的男女同性恋者道歉，并称“对直到1969年对同性恋公民造成的伤害”； 以色列承认出于移民目的的同性关系，涉及以色列居民的外国伴侣。
        
        - The Transgender Pride flag was first shown, at a pride parade in Phoenix, Arizona.
        在亚利桑那州凤凰城举行的一次骄傲游行中，首次显示了变性人骄傲标志。
        
        - Hillary Clinton became the first First Lady to march in an LGBT pride parade.[217]
        希拉里·克林顿（Hillary Clinton）成为第一个参加LGBT骄傲游行的第一夫人。[217]









   
        
