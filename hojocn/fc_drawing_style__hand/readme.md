

"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
    - 如有错误，请指出，非常感谢！  


# FC的画风-手  

- 素描教程里的介绍：  
    - 男性的手[1]：  
    ![](img/Drawing.The.Head.Hands.p137.jpg) 
    ![](img/Drawing.The.Head.Hands.p138.jpg)  

    - 女性的手[1]：  
    ![](img/Drawing.The.Head.Hands.p144.jpg)  
    相比于男性的手，女性的手的骨骼更小、肌肉更纤细、手面更圆。如果中指长度大于手掌长度的1/2，则看起来更漂亮。指甲长、椭圆，显得更有魅力。  

    - 手的大小[2]：  
    ![](img/Drawing.Dynamic.Hands.Burne.Hogarth.p38.jpg) 
    ![](img//Drawing.Dynamic.Hands.Burne.Hogarth.p39.jpg)  


- 11_149_0，这个镜头里，乍一看，紫苑的手似乎偏大。用线测量后，发现大小接近标准、或偏小。  
    ![](img/11_149_0__mod.jpg)  


- 02_125_2，紫的手指下粗上细，符合女性的手指。  
    ![](img/02_125_2__mod.jpg)  


- 12_021_5，紫苑的手指下粗上细，符合女性的手指。  
    ![](img/12_021_5__mod.jpg)  



**参考资料**  
1. Drawing The Head & Hands, Andrew Loomis.  
2. Drawing Dynamic Hands, Burne Hogarth. (《动态素描-手部结构》，伯恩霍加思)。  



--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
