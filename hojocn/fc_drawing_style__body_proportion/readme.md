# FC的画风-人物身材比例

----------------------------------------------------------
## 雅彦
选图多出自于前两卷

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion_masahiko-00.jpg)
7：1 （vol12-112）
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/masahiko/masahiko-00-2.jpg)
6.5：1（vol12-169）
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/masahiko/masahiko-01.jpg)
6.5：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/masahiko/masahiko-02.jpg)
6.5：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/masahiko_yukari-03.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion_masahiko-17.jpg)
6.5：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion_masahiko-18.jpg)
6.5：1

所以，雅彦的身材比例多为6.5：1,少数为7：1
 
----------------------------------------------------------
## 紫苑
选图多出自于前两卷

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-11.jpg)
6.5：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion_masahiko-00.jpg)
7：1（vol12-112）
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-04.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-05.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-07.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-08.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-09.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-10.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-12.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-13.jpg)
1:7 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-14.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-15.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion_masahiko-17.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion_masahiko-18.jpg)
7：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-06.jpg)
1:7.5
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-16.jpg)
1:7.5
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-19.jpg)
7.5：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-20.jpg)
7.5：1
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/shion/shion-21.jpg)
7.5：1
 
所以，紫苑的身材比例多为7：1,少数为7.5：1。特写用的是1:7.5。

----------------------------------------------------------
## 紫

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/yukari.jpg)
少数(第一行)比例接近8：1，多数（第二行）比例略大于7：1

----------------------------------------------------------
## 空

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/sora.jpg)
少数(第一行)比例接近9：1，和(第二行)7.5:1，多数（第三行）比例7：1

----------------------------------------------------------

## 雅美

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/masahiko-girl.jpg)


----------------------------------------------------------

## 森

![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_drawing_style__body_proportion/10_102_3__mod.jpg)

----------------------------------------------------------

**导演**

导演第一次出镜时，身材不太矮（没带帽子）。  
![](img/02_134_3__mod.jpg)  

第二次出镜时，身材也不太矮。  
![](img/02_145_1__mod.jpg) 

统计导演身高的变化:  

1:6 :   
![](img/02_134_3__mod.jpg) 
![](img/02_145_1__mod.jpg) 
![](img/07_090_1__mod.jpg) 
![](img/14_195_4__mod.jpg) 

1：5.5 :  
![](img/06_141_4__mod.jpg) 
![](img/10_135_0__mod.jpg) 
![](img/12_128_0__mod.jpg) 
![](img/14_010_2__mod.jpg) 

1：5 :  
![](img/14_240_3__mod.jpg) 

("导演桑，你越长越矮是肿么回事呀？(笑)" )

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



