
# PIXAR STORYTELLING, Rules for Effective Storytelling Based on Pixar’s Greatest Films

BY Dean Movshovitz

This book focuses only on Pixar’s storytelling techniques and will neglect myriad other storytelling options that have proven to be successful, evocative, and moving.


## CHAPTER 1, CHOOSING AN IDEA

### **Mother Lodes**—Choosing Ideas That Have a LOT of Potential

### Leaving the Comfort Zone: More **Discomfort** = More Story


Toy Story’s concept of “toys are actually alive” is an immediately exciting one that offers many narrative possibilities and a rich world to explore. However, it isn’t until the concept evolves into “favorite toy gets replaced by a newer, shinier toy,” that emotional stakes are introduced.  
> 玩具总动员的“玩具实际上还活着”的概念立即引起人们的兴趣，它提供了许多叙事可能性和丰富的探索世界。 但是，直到概念演变为“最喜欢的玩具被更新的，更有光泽的玩具所取代”时，才引入了情感上的赌注。 


To truly upset a character, you must create a weakness or fear that you can tap into. Therefore, Pixar creates a pre-existing problem in each protagonist’s world.
>要真正打乱角色，您必须制造弱点或担心自己会被利用。 因此，皮克斯（Pixar）在每个主角的世界中都创建了一个预先存在的问题。 

### A Character and World That Vie for Adventure—The Existing Flaw

As most of us go about our lives—working, dating, socializing—we tend to ignore things that are bothering us. Maybe it’s a relationship we don’t quite understand, a loss we haven’t properly mourned, or a part of ourselves we haven’t quite accepted. These emotional cracks are what makes us human, and they are what will make your characters compelling.
>这些情感上的裂缝使我们成为人类，并且使您的角色引人注目。

Ideally, even before the gears of the plot start to turn, there should be a problem[1] in your protagonist’s life or world. 
>理想情况下，即使情节开始运转，主角的生活或世界也应该有一个问题[1]。

Once you have found **the existing flaw**[1] in your core idea, craft a story that pushes it to the extreme[2]. .... Whatever the existing flaw, it must be clearly related to the plot you have crafted for your protagonist. The more these two([1] and [2]) work in tandem, the higher the emotional stakes will be and the more invested your audience will be.
>一旦发现核心思想中存在“现有缺陷” [1]，就制作一个故事，将其推向极限[2]。 ... 无论存在什么缺陷，它都必须与您为主角制作的剧情明显相关。 这两个（[1]和[2]）协同工作的越多，情感投入就越高，观众的投入也就越大。 


### Economy: How Every Moment in Ratatouille Stems from Its Core Idea

Pixar’s films find the heart of their stories and never stray from it. Once they find **the emotional core—the flaw and the plot that infringes on it**—they make sure every development and every character are closely connected to this main narrative undertow.

Every scene that follows is an escalation of those reasons, with the answer swinging like a pendulum from “Yes, look at Remy and Linguini cooking together and becoming friends” to “No, no matter how talented he is, Remy will never be recognized as a cook on his own because he does not belong among humans, who will never accept him.” This is economy. Everything in your screenplay should relate to your core idea, to your main conflict. In Ratatouille, all the supporting characters reflect Remy’s conflict. 
>这就是经济。 剧本中的所有内容都应与您的核心思想和主要冲突相关。 

### Summary

Every moment in Ratatouille evolved out of its core idea. The same should be true for any story you write. Once you have a good idea, such as one of those discussed in the first part of this chapter, treat it as a seed that you must sprout into story. Let it grow, step by step, hewing close to its core. What isn’t part of this essence, this seed, probably shouldn’t be part of your story and should be pruned mercilessly.

Uncomfortable characters are so appealing because we all like feeling comfortable. And once our cushy existence is taken from us, we need to reconcile these new circumstances with who we are and what we have lost. This desire creates scenes and conflicts for your characters and story. Pixar’s characters go to great lengths to retrieve what they have lost. Watching them react to their new circumstances, fighting, and growing, is what makes Pixar’s films so moving and enjoyable. From an extremely nervous father, a small fish who crosses an enormous ocean to learn how to let his son live; to a superhero stuck in retirement in the most mundane of lives; to a reclusive, heartbroken old man who must take responsibility over an innocent child; to an aspiring monster that just isn’t scary—Pixar excels in putting characters in the worst places possible for them.

### In Inside Out

### Do It Yourself:

What is **the core idea** of your story? 
Does it offer **many possibilities** for dramatic moments? 
Does it impart strong, specific emotional discomfort to your protagonist?
Do you mine this **discomfort** to create scenes and movements that affect your characters emotionally? 
What is the **flaw** in your fictional universe? 
Is it closely related to the plot you constructed? 
Do they mutually enhance and enrich each other? 
Do all your characters, narrative decisions, scenes, and themes pertain to your core idea? 
Are you constantly exploring and expanding the seed of your story as it progresses? 
Have you branched off in directions that aren’t part of your core idea?



## CHAPTER 2, CREATING COMPELLING CHARACTERS

### Interesting Characters Care Deeply

Once you have a strong idea for a movie, your next step should be getting **to know your lead characters**. Strong, unique characters are the secret to a movie’s success. No matter what your story is, the events that construct it are happening to someone. And that someone better be interesting and, more importantly, must care about what is happening to and around them.

Pixar has created many unforgettable characters:。。。。Why do these characters resonate so strongly? Well, they are all one of a kind. Pixar’s commitment to exploring different worlds leads to a varied array of characters. Each of these characters enjoys a unique graphic design, representative of their unique attributes.。。。They are given specific, unique traits, 。。。These original, specific decisions are key to making main characters entertaining. But for them to compel an audience, you must also endow them with a deep passion for something…anything. We care because they care.。。。

。。。Good characters care. Great characters care because they have strong opinions.

### Strong Caring Stems from Strong Opinions

Elinor cares about Merida’s manners because she has strong opinions about duty, diplomacy, and governing.

Why is this so important? Because it amplifies dramatic effect. 

Merida and Elinor’s deeply-held opinions transform the scene into a clash between ideas of freedom versus duty, and honest expression versus tactful diplomacy. These things mean a great deal more to the characters, and by proxy, to us.

As you can see from this example, strong opinions are fuel for conflict.
As a matter of fact, the plots and relationships in most of Pixar’s films revolve around opposing opinions.

### The Best Opinions Come from (Painful) Experience 

Of course, opinions don’t just manifest out of the ether. Think of your own opinions. They are most likely conclusions you have arrived at through your upbringing and years of education and experience. The same is true for your characters. You don’t need to flash back to fifth grade to explain every attitude your character has, but strong opinions are often shaped by a character’s past. It’s even better if that past is rife with conflict and tension.

Almost all of Pixar’s films present the past in a meaningful sequence:。。。Often these glimpses of the past are used to provide texture and depth to a character’s behavior.

Sometimes it takes only one line to give us all the background we need. 。。。

Common screenwriting lore often considers flashbacks as a tricky, treacherous tool that usually does more harm than good and should be avoided when possible. According to these “dry rules,” many of Pixar’s prologues should be jettisoned, especially considering that some of them are mostly expositional. However, these sequences work wonderfully, proving yet again that screenwriting rules should always be understood and examined rather than taken as blanket statements.

Pixar’s flashbacks work so well because they are usually very economical, entertaining and well-placed. They usually occur at the beginning of the film, making them more of a prologue than a flashback,or they arrive organically as the story unfolds, as part of a character’s action or reaction。 Longer flashbacks succeed because Pixar treats them as independent short films, rather than as supplementary material for the main plot. Up’s prologue is complete on its own, with a full set of desires, obstacles, and turning points—all of which remain deeply linked to the main plot’s goals and challenges.  

### Summary

Creating compelling characters is one of the biggest challenges you will face as a writer. You must use all your originality and insight to create a distinct and memorable individual whose story, appearance, and world are unique. Most importantly, your characters should care about ideas, values, and people, ideally out of a specific, opinionated point of view. When your characters’ opinions are rooted in their experiences, especially painful experiences, it gives them depth and makes them more realistic. These three tools— **passion, opinions, and experience**—make the events in your story more meaningful and dramatic for your characters, and by proxy, for your audience.

(Painful) Experience --> (Best) Opinions --> (Strong) Caring/passion --> (Good) Character ( --> (good)plots --> (to compel) reader )

### Do It Yourself: 

Think about the characters in your story. 

What is important to them? 

What do they believe about love, friendship, death, freedom, and happiness? 

Why do they believe these things? 

How can you use their values and history to give the events of your plot a stronger impact on your characters?



## CHAPTER 3, CREATING EMPATHY

### The Three Levels of Liking

1st level: (good looking). they were the most attractive and charismatic ones. you can “like” characters—for their obvious, external traits. 

2nd level: (positive/attractive traits). they’re just entertaining, or knowledgeable and passionate about something. Maybe they’re funny or talented. 

3rd level: (empathy). it can include less desirable aspects of a character. We forgive our friends their trespasses because we know them better and understand where they’re coming from. allowing the audience to view the character as their proxy. The character becomes a surrogate for the audience’s own hopes and fears.

The third layer requires more details and originality, as well as more patience from your audience, but is more rewarding and creates a stronger bond with viewers. 

Use it wisely; the further you take us into the heart of someone completely different, the more rewarding and transformative our journey will be.

There’s another method of creating empathy with a character, and arguably it is the most crucial: **Place them in trouble**. But remember that no one enjoys merely watching someone else suffer—simply tormenting your characters will cause more **pity and aversion** than empathy. Instead, characters should be placed in harm’s way and then forced to bravely chart their course out of it.


### Desire and Motive

How can you express someone’s inner world in a manner to which an audience can relate? One way is to focus on a character’s desires....When we see a character truly desire something, we almost immediately take their side and hope they obtain it. Why? Because we hope to have our goals met just like the character does. 

It’s not enough to know only what a character wants. As storytellers, we must also know (and convey) **why** they want it. .... This is especially important when designing antagonistic characters.


### “When Remedies Are Past, the Griefs Are Ended”

Your characters should feel grief when they meet adversity, and that grief must last until remedies are past. Simply put, a character can give up and accept their fate only after every imaginable course of action has been tried.

Movie characters should have flaws, but among those flaws you’ll almost never find defeatism. The characters in Pixar’s films never give up. They will look death in the eye; they will conquer their deepest fears; they will change and adapt, if doing so offers a chance for them to get what they want. This is part of why Pixar’s films are so satisfying. It’s why you must design a strong reason for your character’s desire. This motivation must be powerful enough to propel them through many trials.

Again, this relates to our identification with the character. We wish to live life this way, implacably following our desires. But we usually don’t. Movies show us the risks and rewards, the trials and joys, of those who do.

This isn’t to say that movie characters can’t give up. As a matter of fact, they practically must give up—for a moment. 

Characters must experience these dark moments, 。。。Such a character simply isn’t realistic. 。。。A character that gives up easily frustrates an audience. A character that suffers a true, earned failure will resonate strongly with anyone invested in their quest.

There are many ways for a protagonist to demonstrate their determination—a character’s journey isn’t measured in miles, but in painful experiences and overcoming obstacles.

### Summary

For the audience to transcend from liking your characters to empathizing with them, you must create **rich, specific characters**. 
Through a process of discovery, you must dole out the idiosyncrasies of your fictional creations while highlighting **what is relatable, human, and universal about them**. One of the most universal things is **desire**. Giving your character a clear goal and a strong motivation behind it will help people empathize with your character, even when their actions are questionable. 
Lastly, while pursuing their goals, characters should be bold and determined, bravely battling their self-doubt, and never giving up until they have done everything imaginable to achieve their goal.


## CHAPTER 4，DRAMA AND CONFLICT

### More Than Life and Death

The more unique, original, and layered these obstacles are, the more your story will stand out and satisfy your audience.

Keep in mind that life-threatening situations are just a starting point. Death is an obstacle to all goals, but merely living is a dull goal. Designing specific, personal goals for your protagonists, based on their opinions and desires, can lead you toward more interesting conflicts that you should develop and explore.

Another way to define conflict is as a situation in which two opposite forces struggle with each other. This means that every conflict has a built-in question: Who will win? While this view pertains to the physical conflicts, it is particularly useful when discussing internal conflicts.

opinions are fuel for conflict. Once a character cares deeply about something, you can create powerful, emotional conflict surrounding that emotion. Conflicts rooted in a character’s opinions and emotions are a little harder to convey, both because they exist in a character’s mind, and because they are more specific and less universal than life or death. They require deeper understanding of the character and world you’ve created. You must explain the emotional constitution that allows your character to have these opinions. (This explanation is more formally known as “exposition.”) Furthermore, you must highlight the submerged conflict question, the two forces at play.

### Creating and Communicating Emotional Conflict

Conflict is more effective when there is something your character is risking. This is where the opinions we discussed in the first chapter come in.

Your characters must care about something.When they stand to lose that something, you have conflict. 

### Making the Stakes Real and Larger Than Life

Pixar’s films deal with extremes. This means that no matter what the stakes are, Pixar will amplify them as much as possible.

Most love stories revolve around finding, winning over, and holding on to the love of your life. Usually, should a character fail to succeed in this, we know they will be miserable, but we also know it’s likely that after a while, they will find someone else. 。。。If Wall-E doesn’t win Eve’s heart, he will remain alone. But this is loneliness with a capital L. We’re talking about remaining as the only sentient being on the planet, talking to a cockroach and watching the same movie over and over forever. It’s not just about some guy losing a girl. It’s about remaining alone in the universe. 。。。The climax of Ratatouille revolves around the visit of influential critic Anton Ego to Gusteau’s restaurant. For this conflict to be interesting, the stakes need to be high: Ego must have true and complete destructive capability over Remy and Linguini. Making him the toughest and most respected food critic in Paris is a nice start. 

### Exposing and Changing Characters—A Chance at Construction

Conflict is needed because, well, audiences find it enjoyable. They are presented with two opposing forces and want to know which will win—very much like spectator sports, which have entertained masses since the dawn of time (except that movies come with a much better chance that your favorite will win). Pixar’s films usually offer something more in this struggle. In their movies, conflict tends to expose or change something emotional within the core of their characters.

This is the flip side of the destructive force mentioned earlier. Characters must be in danger of destruction, but they must also have a chance at construction as well. They must stand a chance of surviving the threat they’re facing, and of emerging from the struggle stronger and whole—perhaps defeating the flaw mentioned earlier. If a force is defeated but the protagonist remains susceptible to the same form and strength of attack again, then they haven’t truly defeated that force. Furthermore, the movie would feel futile.

In storytelling, the most powerful instance of construction is personal, when a protagonist must change something deep within themselves to achieve their goal. This is deeply moving because true change is extremely hard to achieve—in fiction and in life. We resist change, and so do our characters, because change entails risk. Did you ever try to kick a bad habit? Or embark on some new venture? Change doesn’t just bring with it a new, unknown territory and the risk of ridicule or failure—it also forces us to dismantle some part of us. Every change isn’t just a birth but also a death, as characters must part with some deep aspect of themselves.

...... Notice how this change is both death and birth: Yes, the toys found a new home that seems to solve all their problems, but this solution forces them to leave their cherished owner, Andy. Change always comes with a price.

This means a protagonist’s goal might change throughout your story.

If the conflict in your story merely allows your character to show their skills, or to stretch them, you’re only halfway there. Try cranking up the discomfort, forcing your characters to dispense with whatever baggage is hindering them, and build themselves anew, to deal with the threats you’ve created.

### Summary

Conflict is a collision of two opposing forces, which offers a dramatic question to an audience: “Which force will win?” Pixar’s films often depict dangerous worlds rife with life-and-death situations, where losing the struggle would mean the demise of a protagonist. While these conflicts are entertaining and easily relatable, Pixar strives to create deep emotional effects on its audience. To achieve them, they create strong internal conflicts. (总结： strong internal conflicts --> deep emotional effects on the audience)  

These kinds of conflicts are challenging to create and communicate. They must be rooted in the opinions and beliefs of a character and must put them in danger of losing something dear, usually a part of their identity. To express the emotional forces struggling within, we must find filmable, external expressions of the conflicts: other characters, mementos, dialogue, or a symbol system that is clear to the audience. Making the situation extreme also helps convey the meaning of your character’s struggle.

The best kind of conflict offers a chance for both destruction and construction, which would have a fortifying effect against the antagonistic forces. Construction can only come from change, to which characters and people are naturally averse. Therefore, the quality of the conflict in your script is measured by the believable change it propels in your characters. Change is the measuring unit of conflict.

Pixar also wisely exacerbates smaller conflicts by manipulating expectations. ... Before springing something good or bad on your characters and audience, set opposite expectations for a stronger effect.

### Do It Yourself: 

What is the main dramatic question in your script?  

What is the answer the audience must stick around to see?  

Does this question have an emotional component?  

Have you found an original, organic way of expressing this inner struggle in the physical world of your story?  

Is there a force capable of destroying your characters?  

Is there a force in your story capable of pushing the characters to construct something new and stronger?  

Do your characters change in a clear, discernible way because of the conflicts they face?  


## CHAPTER 5， PIXAR’S STRUCTURE

### A Word on Structure

The most ubiquitous structure, ..., has the following characteristics:

3 parts: 

- the beginning.  
It gives needed information to the viewer (exposition). In our terms, the beginning is needed to show a character’s comfort zone (or what Christopher Vogler calls the Ordinary World) so that we can appreciate their discomfort when they are forced to leave it.   
    
- the middle.   
It will be longest and will focus on obstacles.   
    
- the end.   
It resolves the dramatic questions presented and tends to be a little shorter than the beginning and rhythmically quicker (with shorter scenes and the convergence of plot lines).  


A satisfying story will usually have between 3 and 5 major events.   

    - 1st (major) event (inciting incident). It is the first meaningful event to happen in your story. It takes place in act one and begins the plot and propels your protagonist to act.   
    
    - 2nd (major) event (1st plot point). It often cements the protagonist’s commitment to their quest and serves as an inciting incident of sorts for the 2nd act, sending our hero in a new direction. ... It usually happens around the 1/4 mark of the film. Sometimes it converges with the inciting incident.  
    
    - 3rd (major) event (2nd plot point), which takes place around the 3/4 mark of the film. It will usually feature a strong blow to our protagonists and will set up the 3rd act.   
    
    - The last major event (climax). The climax should be a suspenseful, grand scene rife with conflict that answers the main dramatic question of your story. It will usually take place several minutes before the end of your film.  

Some theories include an additional major event that takes place at the 1/2 of the film.

The best way to think of plot points is as a combination of an inciting incident and a climax.  
    - The inciting incident begins a story, raises dramatic questions, and sends a character on a journey. 
    - The climax answers dramatic questions and features a tense, potent moment of conflict, which is resolved in a satisfying manner.   
    
Plot points must do both. They must set your hero on a new path while also resolving some of the dramatic questions already raised—though never the dramatic question of your story. Because plot points are a combination of inciting incident and climax, they aren’t as potent a catalyst as the former nor as final a resolution as the latter.

The position of these events isn’t exact science. Yes, the markers described above seem to work well. 

Use structures to help you figure out what works for your film.....The main point to take from this section is that stories require a setup, a series of trials and a resolution. They also seem to require at least three major events—three major clashes of opposing forces that usually result in a major decision by your protagonist. Pixar’s films seem to include more than three.


### Major Events—Whats, Hows, and Whys

One way to think of major events is as the “what happens” of your story, connected by the “hows” and “whys.”....The “whys” are answered by exposition, and the “hows” by scenes of activity.

What differentiates these 4 major moments from the rest of the film? They aren’t necessarily the most exciting moments. .....One way to consider these moments is to think about how they relate to the existing flaw we saw at the beginning of the movie. More specifically, they must both either offer a chance to fix it or a chance to destroy all hope of fixing it (destruction or construction), forcing the protagonist to plough ahead or be destroyed. .... Major events are crossroads, which usually offer a chance for a character to cut their losses or continue bravely ahead.

Pixar creates more plot points and major events than it initially seems. The writers use subplots, parallel plots, and internal plots to create more stories and thus more major events. This story inflation seems to keep the audience’s interest and satisfy them emotionally and narratively.

### A Multilayered Storytelling Cake

Pixar’s films aren’t the only ones that have a multilayered set of subplots that create many meaningful turns, but it does seem that Pixar cracked the code as to how these subplots should relate to one another. The main plot should be epic, adventurous, and rife with action, with an emphasis on life-or-death, physical situations. .... Under that main plot, there will be an emotional, internal story. .... All the studio’s stories also feature a major, complex bonding process with another equally flawed character from a different world. ... Sometimes Pixar will add another subplot, usually taking place parallel to the main plot. ...

This triple and sometimes quadruple structure seems to satisfy all the reasons we go to the movies: high-end adventure, meaningful relationships, and deep emotional struggles. Pixar’s skill in juggling these layers, and threading them together strongly, seamlessly, and honestly is what sets them apart. The different layers reinforce one another, giving emotional weight to the daring actions and clear dramatic expression to the emotional conflicts.

### Bonding Stories

As mentioned, conflict is crucial to your story and, yes, there is something deeply satisfying about watching a character fight against the odds for something they deeply believe in. But it is also very satisfying to watch two characters from different worlds grow closer together and balm each other’s wounds—when it is earned. By “earned,” I’m referring to a believable, specific process of bonding. Watching two people meet and immediately become best friends isn’t moving. Nor is watching two people hate each other for an entire movie, only to turn on a dime and fall in love in the last minutes of the film.

Bonding should involve two characters who have clear reasons not to like each other. Only through a parallel process of external events pushing them together and inner changes that remove the relationship’s emotional obstacles, can they come together in a way that is truly meaningful.

Sometimes bonding is propelled by discoveries about the other characters. ..... Sometimes it is a self-discovery....

### Double Climaxes

Many of Pixar’s films feature a double climax. The main story is resolved in a grand, high-stakes sequence, as in most films. In Pixar’s films, this storytelling peak is usually followed by a quieter climax scene, designed to resolve one of the more internal stories.

Pixar goes a step further by creating an inner world and existing flaw for these characters. 

Whenever you’re adding an element to your story, make sure it’s inherent in your original concept. Try to explore and expand existing characters, settings, plots, and themes. If you’re adding a more external element, take extra care to develop it in a way that interacts meaningfully with your core characters and story.

### Summary

There are many forms of structure. Most agree on the three parts of story: setup, trials, and climax and resolution. 

Pixar’s films usually have a layered structure that involves an adventurous life-or-death action story, an interpersonal story of bonding, and an inner emotional struggle. These layers are interconnected. This structure serves as a force multiplier, enriching each of these stories separately and creating more major events, which audiences seem to enjoy. Often, Pixar’s films will include yet another storyline, taking place concurrently with the main story.

This structure also results in a double climax, where Pixar stages a grand, suspenseful, awe-inspiring, life-or-death sequence—the climax of the film’s main plot—which is followed by a moving emotional climax. This double whammy creates powerfully moving and satisfying endings.


## CHAPTER 6，CASTING CHARACTERS

### Your Story as an Efficient Machine

Everything you introduce in your script—every line, character, theme, or piece of information—must have a function and be part of the grand scheme. Often, all this means is exploring how to make the most out of every preexisting element in your script. 

### Characters as Plot Functions

Most of your characters will have a meaningful relationship with your protagonist. They will be their friends, lovers, mentors, or adversaries. However, some characters in your script will simply have a story function to perform. It’s important to design these characters with care, so the mechanism that necessitated their existence isn’t too transparent.

In Brave, Merida longs to have her destiny changed. This leads her to a witch.

It’s good to keep in mind that there are no such things as small characters.

Pixar’s devotion to ensuring that every character and detail in the worlds they create is original and entertaining is a great part of why their films are successful. 

### Keep Your Writing Honest

you should never choose uniqueness or “coolness” over honesty.

It’s important to dream big and imagine outlandish events and characters. It is equally important to cement these moments in real, relatable emotions.

### Designing Distinct Characters

### Summary

Treat your story as an efficient machine. Every part of it should be treated with care and should be a part of your grand scheme. Nothing should be missing, and nothing should be redundant. This is especially true for your characters. While some characters may have specific functions to perform—obstacles, catalysts, and so on—they must also be drawn with care and imbued with their own stories and personalities. Never sacrifice honesty for originality or coolness. No matter what awesome invention you come up with, work hard to tie it to an emotional reality that is part of your fictional universe.


### CHAPTER 7， VILLAINS

### A Word about Antagonism

Antagonism refers to anything that stands between your protagonist and their goal. It can be a character, an object, a concept, or even the protagonist themself. 

Often, though, they are a more innocuous character who happens to inadvertently make life harder for your protagonists.

### Evil versus Troublesome

When discussing antagonistic characters, we must make a distinction between “evil” and “troublesome.”

“Evil” characters have no regard for morals or fairness, and are indifferent or even joyous at the pain of others. Nevertheless, they should still have their own story and their own reasons for their actions....Other antagonists may mean well but just happen to cause our protagonists grief. Let’s call them “troublesome.”

Pixar films often feature two main antagonists—one benevolent and one malicious


### “Good” Villains

“Good” villains appear, on the surface, to belong to the malicious group. They are often mean, indifferent to the pain they cause, and even terrifying. What differentiates them from malicious antagonists such as Muntz or Hopper is that they have a benevolent core set of beliefs that strive to benefit their community. This is important. They can’t be purely self-interested. The “good” villains—such as Anton Ego, Dean Hardscrabble, or the ship’s autopilot in Wall-E—aren’t nice, but they have positive core values. They never unfairly target anyone, and they don’t enjoy hurting people. Their moral compass is in place, and they have certain lines they will never cross

### Antagonists as Mirror Images

Some antagonists will have a thematic relationship to your protagonist. They will be a distorted mirror image, presenting our heroes with their darkest fears, or at least shining a light on their fallacies and weaknesses.

### Summary

A charismatic, cackling villain can be a lot of fun. Some of your stories will need those.

Keep in mind that many antagonists have good intentions and can be complex characters. When looking for antagonists to put in your hero’s way, don’t limit yourself to villains. Friends, supporting characters, and even the environment itself can provide you with plenty of dramatic fodder.

### Do It Yourself: 

Who are the characters standing in your protagonist’s way? 

Are they evil for the sake of evil, or do they have reasons for their maliciousness? 

How can you make them more understandable and relatable? 

Is there a way to make them benevolent while retaining the opposition they present to your hero? 

Are your characters’ allies constantly helping them out, or do they challenge them as well? 

Friendship must be earned, and even then, it is there to challenge just as much as it is to support.



## CHAPTER 8，DEVELOPING AN IDEA

You must carefully select which ideas you choose to develop into full stories.

Equally important, if not more so, is how best to develop them.

### Plotting versus Exploring

- to develop an idea focuses on the plan for your plot and characters...you know the end... You must now design stops that will reach the end.

- explore your universe. You don't know the end. 

    1. to subvert expectations.
    
    2. to impose creative limitations on your story.

These approaches aren’t mutually exclusive. You’ll often switch between them. 

### Subvert Expectations

When developing your idea, discover what is expected from you, and then try to subvert those expectations.

Another mechanism Pixar excels at is adapting familiar settings into their fictional universe. 

### Focus Your Canvas—Creative Limitations

In his TED Talk, Andrew Stanton discusses some of the self-imposed limitations Pixar’s writers decided on when they began, among them, not having a love story. Selecting that one limitation must’ve forced the writers to consider what the relationships in their films would then be, thus inevitably pushing them in new, interesting directions.

### Summary

Part of Pixar’s success is due to the studio’s originality and unique inventions. These stem from exploring your fictional universe and learning about all the places, people, emotions, and ways of life it has to offer. You don’t have to use all your findings, but you should incorporate what helps you develop your plot and character arc. One of the best ways to explore your universe is to try to subvert expectations. Another way is to impose creative limitations on your story.

**Do It Yourself:** 

- How much do you know about your fictional world? 

- Have you allowed yourself to wander through its streets and fields, talking to its denizens, or 

- did you only mine the parts needed for your plot? Consider the expectations from the material you’re developing.

- Where can you subvert those expectations to create a moment that will surprise and delight your audience? 

- If you’re stuck, consider what you don’t want to happen in your story world. Anything you eliminate will inevitably point toward something you would like to incorporate.


## CHAPTER 9, ENDINGS

### Coincidence versus Character

A good ending must make sense without being predictable. It should come with a bit of a surprise but also justify and elucidate the journey leading it up to it. One key way to reach this goal is to tie the ending deeply to the actions and constitution of your protagonist. The final action must be a direct result of the journey your characters have taken. In other words, avoid coincidences.

If you let your character’s fate—good or bad—simply fall in their lap, you’re robbing us of the joy of knowing who these characters really are. 

### Back to the Beginning—Answering a Question the Audience Forgot

One way to craft a strong ending is to answer a question the audience has probably forgotten or hadn’t even thought about but should have.

### Resolution—Showing the New, Healthy World

We’ve discussed how audiences love seeing characters change. This change, while satisfying, feels more meaningful and carries more weight when it creates a ripple effect. In many of Pixar’s films, the journey the protagonists undertake often creates a better world for the people around them, fixing the flaw in the world.

### Summary

Your ending must be a reflection of your character and a direct result of the path upon which they were set. It shouldn’t be expected or predictable, but it must be tied to your protagonist’s journey. One way to create this effect is to have the ending relate to a seed you have subtly planted earlier in the film. Hopefully the audience forgets about that detail. When this seed pays off in your resolution, the audience will feel an increased sense of cohesiveness, strengthening the meaning of your ending. Pixar film endings often involve creating a better world. The most moving of endings show the positive results of the journey your character has taken, preferably in a visual way.

**Do It Yourself:**

Is your ending a coincidence, or is it linked through a chain of causality to your character’s actions? 

Does it tell us something new about your character’s personality? 

Does your ending feel like an inseparable part of your story? 

Is it linked strongly to your plot through dramatic questions you’ve left unanswered until later in the story? 

Lastly, does your story create a ripple effect? 

Does it change something in the people, community, or world surrounding your protagonist? 

Is there a clear, potent visual way to express this change?


## CHAPTER 10, THEME

### What Is Theme?

What does your story present and explore that is universal and timeless? What is inherently human about it?

Theme is what your story—your scenes, your chases, your one-liners—seeks to create and present.

### Creating Theme, Step 1: Recognizing What Your Story Is About

Ideally, your theme arises organically from your story. 

### Creating Theme, Step 2: Permeate Your Theme Ubiquitously Throughout Your Story

You must make your theme present in your universe. There are several ways to do so.

If you are creating a character as an embodiment of a value, it is often better to make it a supporting character. Create a complex environment that depicts struggles with different attitudes and values, and populate it with characters who embody aspects of your theme (which often are also the conflicting aspects of your main protagonist), 

To reinforce your theme, you can sometimes craft an antagonist as a reflection of your hero. 

You can also imbue objects with thematic meanings.

Most of Pixar’s films simply assert their themes, or a variation on them, at some point or another. 

### Summary

Theme is the part of your story that is universal and abstract. It isn’t part of your plot. It is what your plot expresses. Your theme should emerge naturally from the fictional universe you’ve chosen to explore. Once you’ve found your theme, use plot, characters, locations, objects, and dialogue to make it as present as possible in your screenplay.

Childhood, family, sadness, maturity, death: These universal themes, placed at the core of our existence, are surprisingly underexplored in mainstream popular culture (compared with issues such as love or crime). Pixar’s brave choice of themes and its equally brave (and wise) explorations of these themes are part of what sets the studio’s films apart.

**Do It Yourself:** 

What is your story about? 

What are the abstract questions or issues it flirts with or explores? 

Can you make these themes more present throughout your story via objects, dialogue, or characters?


### CHAPTER 11, THOUGHTS FOR THE ASPIRING ARTIST

Monsters University tells us two things about giving up on our dreams.

The first is that we should only consider the possibility of giving up after we’ve tried our very best in every manner we can imagine.

The other thing Monsters University tells us is that Mike’s pursuit of his dream was not in vain. Not only because he made friends, but because the pursuit did lead him to his calling

Apart from the immense craftsmanship, creativity, and heart, you can also learn from these films’ core themes: Don’t let anyone stop you from pursuing your dream, and don’t let pursuing your dream stop you from finding something that you’re good at and that makes you happy.




