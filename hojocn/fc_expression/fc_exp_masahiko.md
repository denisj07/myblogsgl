"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


# FC表情包

----------------------------------------------------------

- Yanagiba Masahiko:  
    - admire  
03_009_7__

    - adore  
03_147_3__
01_200_3__
03_147_6__
06_155_6__
12_068_5__

    - apprec  
14_207_4__

    - amuse  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_122_1__.jpg) 
05_008_5__
05_092_6__
06_145_2__

    - anger  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/03_006_4__mod.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/14_006_2__mod.jpg) 
03_121_2__
07_102_1__
06_118_2__

    - anxiety  
14_008_0__   
    
    - awe  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_054_6__.jpg) 
              
    - awkward  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/14_004_3__mod.jpg)     
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/05_005_0__mod.jpg) 
14_004_3__
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_101_2__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_121_4__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_122_1__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_134_6__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_135_0__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_141_3__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_142_2__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_144_0__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_146_1__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_147_4__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/12_148_2__.jpg) 
10_166_6__
08_011_4__
04_174_1__
02_096_1__
05_042_3__
08_155_6__
11_017_1__
13_085_3__
01_124_4__
14_139_7__
10_119_6__
14_005_3__
14_028_4__
03_147_2__

    - bored  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_118_6__mod.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_005_1__mod.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod1.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod2.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod3.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod4.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod5.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod6.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_118_6__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_133_4__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_142_2__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_144_6__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_023_6__.jpg) 
            
    - calm  
01_149_3__

    - confused  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/03_006_1__mod.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_147_4__.jpg) 
01_126_4__

    - crave  
              
    - disgust  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/14_006_3__mod.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/03_006_6__mod.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_142_3__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_144_6__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_066_3__.jpg) 
14_006_3__

    - pain  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_121_4__.jpg) 
12_111_4__
04_121_3__

    - entranced  

    - excited 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/05_004_4__mod.jpg) 
12_134_6__

    - fear/horror 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/14_005_2__mod.jpg)     
11_005_1__
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/01_075_4__.jpg)  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_121_2__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_070_1__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/12_122_0__.jpg) 
05_163_2__
02_101_4__
06_032_2__
01_091_4__

    - interest  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_146_3__.jpg) 
03_147_3__

    - joy  
11_006_7__
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_134_2__.jpg) 
06_130_0__

    - nostalgia  

    - relief  
04_191_4__

    - romance  

    - sad 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_005_3__mod.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/14_006_6__mod.jpg) 
09_004_6__ 
02_005_5__
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_118_2__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_133_4__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_135_0__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_141_3__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_010_4__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/14_034_2__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_066_3__.jpg) 
14_013_5__
03_007_1__
05_131_5__

    - satisfy  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/11_006_4__mod.jpg) 

    - sexual  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_155_6__.jpg) 
02_039_1__

    - surprise  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/03_005_1__mod.jpg)  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_006_1__mod.jpg)    
05_004_4__
11_005_1__
11_005_6__
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_064_4__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/12_133_5__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/14_048_1__.jpg) 
10_166_6__
05_045_0__
01_021_3__
14_269_3__
03_006_1__
14_140_2__
14_160_3__


    - others  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/12_000a__mod.jpg)     
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_118_5__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_146_3__.jpg) 
03_074_0__

    - fall
    
    - whisper
    05_027_1__
    
    - snicker  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/11_006_7__mod.jpg) 
    
    - palmface  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/14_006_4__mod.jpg) 

--------------------------------------------------------------------------------  
--------------------------------------------------------------------------------  
--------------------------------------------------------------------------------  
--------------------------------------------------------------------------------  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



