"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


# FC表情包

----------------------------------------------------------
--------------------------------------------------------------------------------  
--------------------------------------------------------------------------------  
--------------------------------------------------------------------------------  
--------------------------------------------------------------------------------  


- Wakanae Shion:  
    - admire     
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_072_0__.jpg)  

    - adore  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/05_006_4__mod.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/05_006_3__mod.jpg) 

    - apprec  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_134_2__.jpg) 

    - amuse  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_4__mod_shion.jpg)     
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_118_6__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_051_3__.jpg) 
08_011_4__

    - anger  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_006_2__mod0.jpg)     
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_006_2__mod1.jpg) 

    - anxiety  
              
    - awe  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/11_005_3__mod.jpg) 

    - awkward  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_069_0__.jpg) 
06_129_4__

    - bored  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_050_5__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_005_5__mod.jpg) 
              
    - calm  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod_shion1.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod_shion2.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod_shion3.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod_shion4.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod_shion5.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod_shion6.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/08_004_1__mod_shion7.jpg) 

    - confused  
05_044_0__

    - crave  
              
    - disgust  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_133_6__.jpg) 

    - pain  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_118_2__.jpg) 

    - entranced  

    - excited  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_004_1__mod.jpg)     
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_133_6__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_051_3__.jpg) 

    - fear/horror  

    - interest  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_133_4__.jpg) 

    - joy  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_115_7__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_133_6__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_066_3__.jpg) 
02_172_5__
07_043_0__

    - nostalgia  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_066_3__.jpg) 

    - relief  

    - romance  

    - sad  

    - satisfy  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_066_3__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/11_006_1__mod.jpg) 

    - sexual  

    - surprise  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/11_005_3__mod.jpg)      
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/11_006_3__mod.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_006_3__mod.jpg)    
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/11_006_6__mod.jpg)  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_004_1__mod.jpg)    
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_143_4__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_064_4__.jpg) 
05_045_0__
11_036_4__

    - others  
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/12_005_0__mod.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/12_004_4__mod1.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/12_004_4__mod2.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_072_0__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/06_118_0__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/07_050_5__.jpg) 
![](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/fc_expression/img/11_122_3__.jpg) 



--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



