
**关于北条司的签名**  

北条司(Tsukasa Hojo)的签名好像都是英文, 分两种:

1. 漫画里和插画里常见的签名格式:  
    **Tsukasa +年月**  
    例如, FC里每话开篇的插画:  
	![](img/10_001_0__sign.jpg)  

2. 参加各种活动时的签名格式:  
    **Tsukasa + Hojo + 年月日**  或  **Tsukasa + Hojo + 季节**
    例如：
    - [https://acg.178.com/201704/286049004430_s.html](https://acg.178.com/201704/286049004430_s.html)里的獠这张图:  
	![](img/signature_2017_spring.jpg)       
    - [现场作画视频](https://v.qq.com/x/page/e085865yho3.html):   
	![](img/signature_2019_04_07.gif)  
    但是，我怎么也辨认不出签名里的Hojo字样。有人能帮忙辨认一下吗？谢谢  



--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



