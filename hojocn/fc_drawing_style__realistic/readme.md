
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  



**人物面部对比真人**  
---

- 关于画风写实  
打算从这几个方面说说FC里的写实画风：透视(perspective)、形状(geometry)、色度(shade)、纹理(texture)。   
    - 形状逼真。比如，画面里人物面部五官的比例符合实物；画面里物体(比如，脸颊、眼框)的边缘符合实物。例如下图, 只有形状, 没有色度和纹理:  
        ![](img/07_082_6__mod.jpg)   
    - 色度逼真。色度指物体表面的明暗度[1]。比如，面部不同部位因凹凸不平而有不同的明暗度，这些明暗度符合实物。例如下图, 只有色度,没有形状和纹理:  
        ![](img/.jpg)   
    - 纹理逼真。纹理指物体表面的花纹。比如，服饰上有不同的花纹，这些花纹符合实物。  
  
    
    

- **面部五官**:  

[这里](../fc_drawing_style__face_proportion/readme.md)讨论了面部五官比例.  

[这里](../fc_drawing_style__body_proportion/readme.md)讨论了面部身材比例.  


- **着色(Shading)**:  
    - 头发的光泽, 见[FC的画风-头发](../fc_drawing_style__hair/readme.md)  




 

- **纹理**:  
    - 头发的纹理：  


    - 13_014，吉他背带的纹理很细腻：  
    ![](img/13_014_3__mod.jpg)  
    
    - 07_127_2。裙子的花边很逼真：  
    ![](img/07_127_2__mod.jpg)  


- [4]里p81页阐述了“眼睛向上看”的细节；  
    ![](img/TheArtistsCompleteGuideToFacialExpression.p081.A.jpg) 
    ![](img/TheArtistsCompleteGuideToFacialExpression.p081.B.jpg) 
    ![](img/TheArtistsCompleteGuideToFacialExpression.p081.C.jpg) 
    ![](img/TheArtistsCompleteGuideToFacialExpression.p081.D.jpg)  

- [4]里p83页阐述了“眼睛向下看”的细节(与"闭眼"的细微差别)。  
    ![](img/TheArtistsCompleteGuideToFacialExpression.p083.jpg)  
FC里的“眼睛向下看”（01_188_3, 02_011_1，09_036_0, 09_041_7, 10_119_3，）：  
    ![](img/01_188_3__mod.jpg) 
    ![](img/02_011_1__mod.jpg) 
    ![](img/09_036_0__mod.jpg) 
    ![](img/09_041_7__mod.jpg) 
    ![](img/10_119_3__mod.jpg)  


- [4]里p84页阐述了“斜眼看”的细节：    
    ![](img/TheArtistsCompleteGuideToFacialExpression.p084.jpg)  
作者认为“斜眼看时，两眼视线方向不一致”是不正常的(p87页)。FC有这样的画法(04_141_0)：  
    ![](img/04_141_0__mod.jpg)  


- **透视**  
    - 03_180_1 v.s. 03_155_5  (in eye_front_yoriko.xcf)  


- 和其他写实漫画家的比较  



**参考资料**：  
1. [Shading - Wikipedia](https://en.m.wikipedia.org/wiki/Shading)  
2. [Differences between Caucasian and Asian attractive faces](https://www.researchgate.net/publication/318548459), Seung Chul Rhee, 2017.  
3. Drawing The Head & Hands, Andrew Loomis.  
4. The Artist's Complete Guide to Facial Expression. 1990. Gary Faigin.   
5. 
--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
