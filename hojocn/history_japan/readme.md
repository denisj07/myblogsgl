#The History Of Japan


###

Modern Japan A Historical Survey(5th edition, 2012), Mikiso Hane, Louis G. Perez

Appendix B: Chronological Chart年表


Tokugawa Period (1600–1867)德川时代

1825 An edict to drive off foreign vessels is issued.发布了驱逐外国船只的法令。
1833–
1836 The Tempō famine.天宝饥荒
1837 Ōshio Heihachirō leads an insurrection.押尾平八郎领导起义。
1841 Rōjū Mizuno Tadakuni initiates the Tempō Reforms.水野Roju Tadakuni发起天宝改革
1849 The woodblock artist Hokusai dies.木版画家北斋去世。
1853 Commodore Perry arrives.佩里准将到来。
1854 The Treaty of Kanagawa is signed with the United States.与美国签署《神奈川条约》。
1858 Ii Naosuke is appointed great councilor (tairō); a commercial treaty with the United States is concluded; the woodblock artist Hiroshige dies.饭直辅被任命为大议员（tairō）； 与美国缔结了一项商业条约； 木版画家广重死。
1867 Shōgun Keiki restores political power to the imperial court.幕府将军恢复了朝廷的政治权力。


Meiji Period (1868–1912)明治时代

1868 A new government is established; Tokyo (formerly Edo) becomes the capital.新政府成立； 东京（以前的江户）成为首都。
1869 Four major daimyō relinquish control over their han to the imperial government.四个主要的大名将他们的汉族控制权交给了帝国政府。
1871 The han are replaced by prefectures; the postal system is introduced; Tokugawa class distinctions are eliminated; the Iwakura mission is dispatched to the West.汉族由县取代； 引入邮政系统； 消除了德川的阶级区别； 岩仓县的任务被派往西方。
1872 The Tokyo-Yokohama Railroad is opened; the freedom to buy and sell land is granted; compulsory elementary education is instituted.东京横滨铁路开通； 授予买卖土地的自由； 建立了义务基础教育。
1873 The Gregorian calendar is adopted (December 3, 1872, of the old lunar calendar is converted to January 1, 1873); universal military conscription and a new land tax are instituted.采用公历（旧历的1872年12月3日转换为1873年1月1日）； 建立了普遍的军事征兵制度和新的土地税。
1874 A request for the establishment of a national assembly is submitted by Itagaki and others.板垣等人提出建立国民议会的要求
1876 The wearing of swords by former samurai is banned.禁止前武士佩戴剑。
1877 Saigō Takamori leads Satsuma Rebellion.西贡高森领导萨摩叛乱。
1879 The Ryukyu Islands become Okinawa Prefecture.琉球群岛成为冲绳县。
1881 A national assembly is promised by the government.政府承诺举行国民议会。
1884 The peerage is created; the Chichibu Uprising occurs.建立贵族； 发生秩父起义。
1885 The cabinet system is adopted; Itō Hirobumi becomes the first prime minister.采用内阁制; 伊藤博文成为第一任总理。
1887 Electric lighting is introduced.引进电照明。
1888 The Privy Council is established.枢密院成立。
1889 The Meiji constitution is promulgated.明治宪法颁布。
1890 The first Diet convenes; the Imperial Rescript on Education is issued; telephone service is introduced.第一次国会召开； 发行了《教育御书》； 介绍电话服务。
1894 A treaty revision is agreed upon between Japan and England.日本和英国之间同意对条约进行修订。
1894 First Peace Preservation Law.第一部《治安维持法》。
1894–
1895 The Sino-Japanese War.甲午战争
1895 Triple Intervention by Russia, Germany, and France.俄罗斯，德国和法国的三重干预。
1898 The Ōkuma-Itagaki cabinet is formed.大熊茨城内阁形成。
1900 Public Order and Police Law.公共秩序和警察法。
1902 The Anglo-Japanese Alliance is concluded.英日同盟缔结。
1904–
1905 The Russo-Japanese War is concluded by Treaty of Portsmouth.日俄战争是根据《朴茨茅斯条约》结束的。
1910 Korea is annexed; Kōtoku Shūsui and others are executed.韩国被吞并； 高德寿水等人被处决。
1912 Emperor Meiji dies.明治天皇去世


Taishō Period (1912–1926)大正时代

1914 Japan enters the First World War.日本进入第一次世界大战。
1915 The Twenty-One Demands are presented to China.对华二十一条要求。
1918 The Hara Cabinet is formed.原内阁成立。
1921 The Washington Conference on naval arms limitations convenes.华盛顿海军武器限制会议召开。
1923 The Great Earthquake.关东大地震
1925 Universal manhood suffrage is enacted; radio broadcasting commences.制定了普遍的男子气概选举权； 广播开始。
1925 Second Peace Preservation Law.第二部《治安维持法》。
1926 Emperor Taishō dies.大正天皇去世


Shōwa Period (1926–1989)昭和时代

1930 London Naval Conference.伦敦海军军缩会议【2】。
1931 The Manchurian Incident.九一八事变
1932 Prime Minister Inukai is assassinated; party government ends.井上首相被暗杀； 党政结束。
1933 Lytton Report; Japan withdraws from the League of Nations.利顿报告； 日本退出国际联盟。
1935 Minobe Tatsukichi’s Organ Theory is condemned.美浓部达吉的“天皇机关说”受谴责。[1]
1936 Prominent leaders are assassinated by radical militarists; the Anti-Comintern Pact with Germany is concluded.杰出的领导人被激进的军国主义者暗杀； 与德国的《反共产国际公约》缔结。
1937 War with China breaks out.侵华战争爆发。
1940 Japanese troops move into French Indochina; a tripartite alliance with Germany and Italy is concluded.日军进入法国支那； 与德国和意大利建立了三方联盟。
1941 Japan attacks Pearl Harbor, beginning the Pacific war.日本袭击了珍珠港，开始了太平洋战争。
1942 The Battle of Midway (June).中途岛海战（6月）
1944 The tide of war shifts; Saipan falls; Prime Minister Tōjō resigns; US bombers carry out massive air raids on Japanese cities.战争的潮流在改变； 塞班失守； 总理东条英机辞职；美国轰炸机对日本城市进行大规模空袭。
1945 US troops land in the Philippines and Okinawa; atomic bombs are dropped on Hiroshima and Nagasaki; Russia enters the war; Japan surrenders; Allied occupation under General MacArthur begins.美军在菲律宾和冲绳登陆； 原子弹投在广岛和长崎； 俄罗斯参战； 日本投降； 麦克阿瑟将军的占领军开始了。
1946 A new constitution is promulgated.颁布了新宪法。
1948 General Tōjō and others are executed.东条英机等人被处决。
1951 The peace treaty is signed in San Francisco.签署旧金山和约。
1951 A US–Japanese Mutual Security Agreement is signed; television broadcasting begins.签署美日安保条约【2】； 电视广播开始。
1952 The Allied occupation ends.盟军的占领结束了。
1955 The Liberal Democratic Party (LDP) is formed.成立了自由民主党（LDP）。
1956 Japan is admitted to the United Nations.日本被接纳加入联合国。
1960 A new United States–Japan Mutual Security Agreement is concluded.新的《日美安全保障条约【2】》缔结了。
1964 Japan hosts the Olympic Games in Tokyo.日本在东京举办奥运会。
1965 Ienaga Saburo, a prominent historian, files the first of his three lawsuits against the ministry of education, charging that the process of textbook approval is unconstitutional.著名的历史学家饭永三郎针对教育部提出了三项诉讼中的第一项，指控教科书的批准程序违宪。
1970 Novelist Mishima Yukio commits suicide.小说家三岛由纪夫自杀
1971 The United States agrees to relinquish control of Okinawa by 1972.美国同意在1972年放弃对冲绳的控制。
1972 Prime Minister Tanaka visits China and normalizes relations.田中首相访华，使两国关系正常化。
1973 The Arab oil embargo and energy crisis.阿拉伯石油禁运和能源危机。
1975 Emperor Hirohito visits the United States.裕仁天皇访问美国。
1975 Prime Minister Miki Takeo visits the Yasukuni Shrine as a “private individual” on the thirtieth anniversary of the end of World War II.第二次世界大战结束30周年之际，首相三木武夫以“私人”身份参拜靖国神社。
1978 Fourteen Class A war criminals (convicted by the International War Tribunal for the Far East), including Tōjō Hideki, are quietly enshrined as “Martyrs of Shōwa” at Yasukuni Shrine.包括东丈英树在内的14名甲级战犯（由远东国际战争法庭定罪）在靖国神社被静悄悄地奉为“昭和烈士”
1979 Prime Minister Ohira Masayoshi visits the Yasukuni Shrine.首相大平正义参拜靖国神社
1980 Japan produces more automobiles than the United States.日本生产的汽车比美国多。
1980 Prime Minister Suzuki Zentaro visits the Yasukuni Shrine.首相铃木善太郎参拜靖国神社
1981 Prime Minister Suzuki Zentaro visits the Yasukuni Shrine.首相铃木善太郎参拜靖国神社
1982 Prime Minister Suzuki Zentaro visits the Yasukuni Shrine.首相铃木善太郎参拜靖国神社
1982 Honda opens first car plant in the United States.本田在美国开设第一家汽车厂。
1983 Prime Minister Nakasone Yasuhiro visits the Yasukuni Shrine.首相中曾根康弘参观靖国神社。
1983 President Reagan visits Japan.里根总统访问日本。
1984 Matsui Yayori publishes a short article in Asahi Shimbun on the subject of “comfort women,” the first time any major newspaper addresses the issue.松井弥代在《朝日新闻》（Asahi Shimbun）上发表了一篇有关“慰安妇”的简短文章，这是任何一家主要报纸首次报道这一问题。
1985 Prime Minister Nakasone Yasuhiro visits the Yasukuni Shrine.首相中曾根康弘参观靖国神社。
1986 Equal Employment Opportunity Law comes into effect to protect women’s rights.《平等就业机会法》生效以保护妇女的权利。
1987 Privatization of national railways begins.国家铁路私有化开始。
1987 An Okinawan supermarket owner burns Japanese flag in protest of Japan’s treatment of Okinawa.冲绳一家超市的老板焚烧日本国旗以抗议日本对冲绳的待遇。
1988 Recruit Scandal: Prime Minister Takeshita resigns for accepting bribes from the Recruit Company.新兵丑闻：竹下首相因接受新兵公司的贿赂辞职。
1988 Supreme Court rules that Nakaya Yasuko cannot prevent government from enshrining the soul of her deceased husband (killed while serving in the Self-Defense Forces) at Yasukuni Shrine.最高法院裁定，中谷靖子不能阻止政府将其已故丈夫的灵魂奉献给靖国神社（在自卫队服役时丧生）。
1989 Emperor Hirohito dies; Crown Prince Akihito is enthroned as Heisei Emperor.裕仁天皇去世； 明仁皇太子即位平成天皇。


The Heisei Era (1989– )平成时代

1990 Nagasaki mayor Motoshima Hitoshi survives assassination attempt for suggesting Hirohito assume blame for World War II atrocities committed in his name.长崎市市长元岛仁幸免于暗杀，因为他暗示裕仁天皇因以其名义犯下的二战暴行而应该承担责任。
1991 Japan pledges billions of dollars to support the Gulf War; refuses to send troops, citing Article 9 of the Peace Constitution.日本承诺提供数十亿美元支持海湾战争； 援引《治安宪法》第9条的规定，拒绝派兵。
1991 In Diet session, Japanese government denies the involvement of the wartime state and its military in the matter of “comfort women.”在国会会议上，日本政府否认战时国家及其军方介入“慰安妇”问题。
1991 Mount Unzendake in southern Japan erupts, leaving 43 people dead and nearly 2,300 homeless.日本南部的云仙岳山爆发，造成43人死亡，近2300人无家可归。
1992 Historian Yoshimi Yoshiaki publishes documentary evidence proving the Japanese government was actively involved in wartime “comfort women” program.历史学家吉见义明（Yoshimi Yoshiaki）发表文献证据，证明日本政府积极参与战时的“慰安妇”计划。
1992 Mohri Minoru is the first Japanese astronaut in space.Mohri Minoru是日本第一位太空宇航员。
1992 Kanemaru Shin is forced to resign his Diet seat for bribes from the Sagawa Kyubin Company.Kanemaru Shin被迫从佐川急便公司贿赂他的Diet席位以收受贿赂。（东京佐川急便事件【2】）
1992 Itami Jūzō, film director, is attacked and seriously injured by Japanese mobsters upset over his unflattering portrayal of yakuza in a film.电影导演伊丹·尤佐（ItamiJūzō）因其在影片中对雅库扎的刻薄描绘而受到日本流氓的袭击，并受到重创。
1992 Prime Minister Miyazawa Kiichi officially apologizes to South Korea over the “comfort women” issue.宫泽喜一首相就“慰安妇”问题正式向韩国道歉。
1992 Prime Minister Miyazawa Kiichi visits the Yasukuni Shrine.首相宫泽喜一参拜靖国神社
1993 The LDP loses its majority in the Diet lower house for the first time since 1955.自1955年以来，自民党首次在议会下议院丧失多数席位。
1993 As millions of Japanese watch on television, Crown Prince Naruhito marries commoner Owada Masako in an elaborate Shinto religious ceremony.随着数以百万计的日本人在电视上观看，皇太子鸣人在精心设计的神道教宗教仪式中与平民小田田昌子结婚。
1993 American Chad Rowan becomes first non-Japanese Sumo Grand Champion (Yokozuna), under name Akebono.美国人查德·罗恩（Chad Rowan）以Akebono的名字成为首位非日本相扑冠军（Yokozuna）。
1993 Earthquake measuring a magnitude of 7.8 strikes northern Japan, killing 196 people.日本北部发生7.8级地震，造成196人死亡。
1994 The LDP returns to power.自民党重新执政。
1994 Ōe Kenzaburō, Japanese novelist, wins the Noble Prize for literature.日本小说家大江健三获得诺贝尔文学奖。
1994 Japan fires a rocket to the moon.日本向月球发射火箭。
1995 Japanese government creates the Asian Women’s Fund to receive private contributions to aid “comfort women.”日本政府设立了亚洲妇女基金会，以接收私人捐款以援助“慰安妇”。
1995 Aum Shinrikyo religious cult under the leadership of Asahara Shoko releases deadly sarin gas in Tokyo subway, killing seven people and injuring hundreds more.在朝原翔子的领导下，奥姆真理教的邪教组织在东京地铁中释放致命的沙林毒气，炸死7人，多人受伤。
1995 Kansai Earthquake strikes in Kobe, causing $100 billion in property losses and killing over 5,000 people.关西地震在神户发生，造成1,000亿美元的财产损失，并造成5,000多人丧生。
1995 On the anniversary of the end of World War II, Prime Minister Murayama makes the first official apology to other Asian countries for Japan’s wartime atrocities.在第二次世界大战结束的周年纪念日，村山首相就日本战时暴行向其他亚洲国家首次道歉。
1995 US President Bill Clinton formally apologizes to Japan for rape of young girl by three US Marines in Okinawa.美国总统克林顿正式向日本道歉，要求冲绳的三名美国海军陆战队强奸少女。
1996 Tupac Amaru guerrillas capture Japanese ambassador in Peru.图帕克·阿马鲁（Tupac Amaru）游击队占领了日本驻秘鲁大使。
1996 Prime Minister Hashimoto Ryutaro visits the Yasukuni Shrine.桥本龙太郎首相参拜靖国神社。
1997 Consumption tax is raised from 3 percent to 5 percent over loud public outcry.由于公众强烈抗议，消费税从3％提高到5％。
1997 Japan signs an international agreement promising to remove remnants of chemical warfare agents left in China after World War II.日本签署了一项国际协议，承诺清除二战后留在中国的化学战剂残余。
1997 In response to historian Ienaga Saburo’s third lawsuit alleging government interference in textbook contents, Supreme Court finds partially for Ienaga.为了回应历史学家饭永三郎关于政府干预教科书内容的第三次诉讼，最高法院对饭永三郎做出了部分裁定。
1998 Winter Olympic games are held in and around Nagano.长野县及周边地区都举办冬季奥运会。
1998 Prime Minister Obuchi issues an apology to the people of South Korea for thirty-five years of brutal colonial rule.小渊(惠三)首相就残酷的殖民统治三十五年向韩国人民道歉。
1999 “Kimigayo” is reinstated as national anthem; Rising Sun reinstated as official national flag.“君之代”恢复为国歌； 朝阳恢复为官方国旗。
1999 The Health Ministry approves Viagra (after six months of consideration) but still holds back approval for the birth control pill (which has been in consideration nine years).卫生部批准了伟哥（经过6个月的审议），但仍推迟批准避孕药（已经审议了9年）。
1999 Tokaimura nuclear power plant accident exposes at least seventy people to various levels of radiation and ends up taking the lives of two.东海村核电站事故使至少七十人遭受各种辐射，最终夺去了两人的生命。
2000 Empress Dowager Nagako dies. She was chosen as Hirohito’s wife when she was fourteen years old.皇太后永子去世。 14岁那年，她被选为裕仁的妻子。
2000 Ota Fusae is elected governor of Osaka, becoming the first woman governor in Japan.大田不二当选大阪府知事，成为日本首位女州长。
2000 A new 2,000-yen bill is released into circulation by the bank of Japan. This is the first release of a new banknote since 1958.日本银行发行了新的2,000日元面额的纸币。 这是自1958年以来首次发行新纸币。
2001 US submarine Greeneville sinks Japanese fishing vessel Ehime Maru near Honolulu, Hawaii. Nine people on the Ehime Maru die as the trawler sinks.美国潜艇格林维尔号在夏威夷火奴鲁鲁附近击沉日本渔船爱媛丸。 拖网渔船沉没时，爱媛丸市上的9人死亡。
2001 Prime Minister Mori and Russian President Putin sign an accord for return of two Kurile Islands, Etorofu and Kunashiri, to Japan.森首相和俄罗斯总统普京签署了一项协议，要求将两个千岛群岛埃托罗夫和库纳扎里归还日本。
2001 First case of Mad Cow disease in Japan.日本首例疯牛病
2001 The United States turns over to Japanese authorities an American serviceman accused of rape.美国将被指控强奸的美国军人移交给日本当局。
2001 Prime Minister Koizumi makes a surprise visit to Yasukuni Shrine.小泉首相出人意料地参拜了靖国神社。
2001 Prime Minister Koizumi visits China and South Korea in an attempt to smooth relations between countries.小泉首相访问中国和韩国，以期理顺两国之间的关系。
2001 Japan dispatches two destroyers and a supply ship to the Indian Ocean to support US forces fighting in Afghanistan. This is the first time for Japan to send military ships outside of its own waters since the end of World War II.日本向印度洋派遣了两艘驱逐舰和一艘补给舰，以支持美军在阿富汗的战斗。 自第二次世界大战以来，这是日本第一次向其自己的水域以外派遣军舰。
2001 Crown Princess Masako gives birth to a daughter, and lineage controversy ensues.皇太子雅子（Masako）生下一个女儿，随后发生血统之争。
2002 Japan and Korea cohost the 2002 Soccer World Cup.日本和韩国共同主办了2002年足球世界杯。
2002 North Korean leader Kim Jung Il allows five Japanese who had been kidnapped twenty years earlier to return to Japan.朝鲜领导人金正日允许20年前被绑架的五名日本人返回日本。
2002 A Tokyo court acknowledges for the first time Japan’s use of biological weapons before and during World War II.东京法院首次承认日本在第二次世界大战之前和第二次世界大战期间使用生物武器。
2002 Prime Minister Koizumi visits Yasukuni Shrine.小泉首相参拜靖国神社。
2002 Koizumi is the first prime minister to visit North Korea.小泉是第一位访问朝鲜的总理。
2003 Prime Minister Koizumi visits Yasukuni Shrine.小泉首相参拜靖国神社。
2004 Prime Minister Koizumi visits Yasukuni Shrine.小泉首相参拜靖国神社。
2004 Japan dispatches Army Self-Defense Forces to Samawah, in southern Iraq. This is the first time troops have been deployed to an active war zone since World War II.日本向伊拉克南部的萨马瓦派遣了自卫队。 自第二次世界大战以来，这是部队首次部署到活跃的战区。
2004 The death sentence for Aum Shinrikyo leader Asahara is confirmed.奥姆真理教领导人朝原的死刑判决得到确认。
2004 Nonradioactive steam leaks from Mihama nuclear power plant, killing four workers and severely burning seven others.美滨（Mihama）核电厂的非放射性蒸汽泄漏，导致四名工人丧生，另外七人严重燃烧。
2004 Japan applies for a permanent seat on the UN Security Council.日本申请联合国安理会常任理事国。
2005 Prime Minister Koizumi visits Yasukuni Shrine.小泉首相参拜靖国神社。
2005 Anti-Japanese protest in Beijing.北京反日抗议活动。
2005 Princess Sayako (age thirty-six), the emperor’s only daughter, quits the monarchy and marries Yoshiki Kuroda, a forty-year-old urban planner.天皇的独生女Sayako公主（三十六岁）退出君主制，嫁给了40岁的城市规划者黑田佳树。
2005 Bills finally pass both houses of the Diet to privatize the postal system.法案最终通过国会的两院将邮政系统私有化。
2005 More than a hundred people are killed when a commuter train crashes near Amagasaki.尼崎市附近的通勤火车相撞时，一百多人丧生。
2006 Prime Minister Koizumi visits Yasukuni Shrine.小泉首相参拜靖国神社。
2006 North Korea test-fires missiles over Sea of Japan.朝鲜在日本海试射导弹。
2006 Japan agrees to pay $6 billion of the $10 billion cost in transferring 8,000 US marines from Okinawa to Guam.日本同意支付100亿美元中的60亿美元，以将8,000名美国海军陆战队从冲绳转移到关岛。
2006 Crown Prince’s younger brother, Akishino, and his wife have a baby boy, the first male heir to the imperial throne born since the mid-1960s. He is named Hisahito and is now the third in line to the throne.皇太子的弟弟秋野和他的妻子有一个男婴，这是自1960年代中期以来出生的第一位皇位男性继承人。 他被命名为久仁，现在位居第三。
2007 Japan recalls ships that supported fighting in Afghanistan.日本召回支持阿富汗战争的船只。
2007 Radiation leaks, burst pipes, and fires at Kashiwazaki nuclear power plant follow a 6.8 magnitude earthquake near Niigata.在新泻附近发生6.8级地震后，柏崎核电站的辐射泄漏，爆管和大火。
2007 Nagasaki mayor Ito Itcho is assassinated by yakuza.长崎市长伊藤町被黑社会刺杀
2007 Japanese whaling fleet sets sail on a six-month mission described as scientific research.日本捕鲸船队起航，历时六个月，被称为科学研究。
2007 Asian Women’s Fund (founded in 1995) dissolves. It had provided 285 women in the Philippines, South Korea, and Taiwan 2 million yen (US$17,800) each in compensation, helped set up nursing homes for Indonesian former sex slaves, and offered medical assistance to some eighty Dutch former sex slaves.亚洲妇女基金会（成立于1995年）解散。 它为菲律宾，韩国和台湾的285名妇女提供了200万日元（17,800美元）的赔偿，帮助印尼前性奴隶建立了疗养院，并为约80名荷兰前性奴隶提供了医疗援助。
2008 A US marine accused of raping a young girl in Okinawa surrenders to Japanese jurisdiction (the case was later dropped).一名美国海军陆战队被控强奸冲绳的一个年轻女孩向日本管辖投降（此案后来被撤销）。
2008 Defamation lawsuit against novelist Ōe Kenzaburō over statements that military officers ordered civilians to commit mass suicide in Okinawa during World War II is rejected in Osaka district court.针对小说家大江健三的诽谤诉讼，因有关军官命令第二次世界大战期间冲绳平民自杀的言论在大阪地方法院被驳回。
2008 District Court of Nagoya rules that Japan’s 2004 dispatch of air force troops to Iraq breached the country’s pacifist constitution.名古屋地方法院裁定，日本2004年向伊拉克派遣空军违反了该国的和平宪法。
2008 Former agricultural minister Shimamura Yoshinobu joins 159 other lawmakers to pray at the Yasukuni Shrine. Prime Minister Fukuda Yasuo does not attend.前农业大臣岛村吉信与其他159名议员一起在靖国神社祈祷。 福田康夫首相未出席。
2008 Tokyo prosecutors arrest Tamio Araki, former president of Pacific Consultants International, and three other company executives for allegedly misusing 120 million yen of government funds meant for the disposal of about 400,000 chemical weapons that Japanese troops left behind in China at the end of the war.东京检察官逮捕了太平洋顾问国际公司前总裁荒木民生（Tamio Araki）和其他三名公司高管，他们涉嫌滥用1.2亿日元的政府资金，用于处置战争结束后日军在中国留下的约40万化学武器。
2008 Poll by Yomiuri newspaper finds 43.1 percent of Japanese support keeping the 1947 pacifist constitution as is, against 42.5 percent who back revisions.读卖新闻社的民意测验发现，日本支持43.1％的1947年和平宪法保持原样，而支持修订的日本占42.5％。
2009 DPJ leader Hatoyama Yukio is elected prime minister at head of coalition with Social Democratic Party and People’s New Party.DPJ领导人鸠山由纪夫当选为首相，与社会民主党和人民新党组成联盟。
2010 Prime Minister Hatoyama says Japan may rethink US military bases after a city on Okinawa elects a mayor opposed to hosting a major air base.鸠山首相表示，在冲绳的一个城市选出反对建立主要空军基地的市长后，日本可能会重新考虑美国的军事基地。
2010 Japan’s economy grows by less than first estimated in the final quarter of 2009. On an annualized basis, economic growth is 3.8 percent, down from the initial estimate of 4.6 percent.日本经济增长率低于2009年最后一个季度的最初估计。按年化计算，经济增长率为3.8％，低于最初估计的4.6％。
2010 Prime Minister Hatoyama Yukio apologizes for not keeping an election promise to move the American Futenma military base from Okinawa.日本首相鸠山由纪夫对未履行将冲绳美国普天间军事基地迁至美国的承诺表示歉意。
2010 Prime Minister Hatoyama quits. Finance Minister Kan Naoto takes over after a vote in the party’s parliamentary caucus.鸠山首相辞职。 财长菅直人在该党议会核心会议中投票后接任。
2010 Ruling coalition loses majority in elections to the upper house of parliament.执政联盟在议会上议院的选举中失去多数席位。
2011 The Great Tōhoku Earthquake and tsunami hit Japan on March 11. Beginning of Fukushima Dai-ichi nuclear reactor crisis.日本东北大地震和海啸袭击了日本。福岛第一核电站核危机开始。
2011 Japan wins Women’s World Cup in soccer (July).日本赢得了女子世界杯足球赛（7月）。
2011 Prime Minister Kan Naoto is replaced by Noda Yoshihiko.首相菅直人被野田佳彦取代。







###Baseball Sport

Baseball, which had been popular since the later nineteenth century but condemned during the war as a decadent Western sport, made an instant comeback in postwar Japan. The annual high school national baseball championship that had driven the nation into a wild frenzy every August in the prewar years returned, and baseball mania revived. In June 1951 the state broadcasting network, NHK, aired a professional ball game for the first time; soon the baseball broadcasts began to rival the high school ball games in popularity. The popularity of baseball grew to such an extent that by the 1960s American professionals were lured to play in Japan. In 1964 Murakami Masanori (b.1944) briefly pitched in the United States, and beginning in 1995 with Nomo Hideo (b. 1968) a deluge of Japanese players defected from Japan to play in the United States.棒球自19世纪后期开始流行，但在战争期间被谴责为一种腐朽的西方运动，但在战后日本迅速卷土重来。 一年一度的高中全国棒球锦标赛在战前的每年八月都使国家陷入狂热，棒球狂热得以复兴。 1951年6月，国家广播电视台NHK首次播出了职业棒球比赛； 很快，棒球广播就开始与高中的球类比赛相媲美。 棒球的普及程度如此之高，以致于1960年代，美国专业人士被吸引到日本打球。 1964年，村上正则（b.1944年生）在美国短暂推销，从1995年开始，与野上英夫（b。1968年生）涌现了从日本投奔日本的大量日本玩家。

In September 1951 radio broadcasting rights were extended to private companies, and the government monopoly came to an end. Beginning in February 1953, NHK began regular television broadcasts. In August of that year a private television company began broadcasting. By 1958, there were 1 million TV sets in Japanese homes, and by 2004 there were over 80 million sets.1951年9月，无线电广播的权利扩展到了私人公司，政府的垄断也因此而终结。 从1953年2月开始，NHK开始定期播放电视节目。 那年的八月，一家私人电视公司开始播放。 到1958年，日本家庭中有100万台电视机，到2004年，超过8000万台。

After Nomo broke the vise-like death grip that Japanese baseball owners had over their players (he “retired” from Japanese baseball before signing with the Los Angeles Dodgers), a legion of top stars jumped to the American major leagues. Before long some twenty of Japan’s best were playing in North America. A few, like Suzuki Ichiro, Matsuzaka Daisuke, and Matsui Hideki, became genuine all-stars in the United States. Many Japanese fans feared that the best Japanese players would naturally gravitate to the high salaries and endorsement possibilities abroad and leave the Japanese leagues to be little more than minor league development stations for the American game.在Nomo打破了日本棒球所有者对球员的钳制般的死亡控制之后（他在与洛杉矶道奇队签约之前从日本棒球“退役”），一大批顶级球星跳入了美国大联盟。 不久，日本最好的二十支球队都在北美比赛。 铃木一郎（Suzuki Ichiro），松坂大辅（Matsuzaka Daisuke）和松井秀树（Matsui Hideki）等少数人成为美国真正的全明星。 许多日本球迷担心，最好的日本球员会自然而然地被国外高薪和认可的机会所吸引，而把日本联赛留给了美国比赛的小型联赛发展站。



###popular culture

In popular culture, pornography and comic books have become the rage. As one authority on Japanese literature and culture has observed, Japan presents a dichotomy of the puritanical and the sordid: cleanliness and dirtiness exist side by side. 51 In Shinto there is an abhorrence of pollution, but there is also the “muddy goddess of the village, the shamaness who is in touch with the dark mysteries of nature.” 52 From this perspective, women are seen as demonic forces that consume men by their passion. Japanese pornographic films and photographs, which are overwhelmingly sadistic, depict brutal abuses of women, thus reflecting, as some believe, a fear of masculine inadequacy. A British woman novelist surmised that Japan’s pornographic movies and comic books exhibit “a very real fear and hatred of women, maybe even of the female principle. The recurring themes [are] of bondage and mutilation. . . . Men must be very much afraid of women if they want to load them up with so many chains and cut off their breasts and I don’t know what.” 53 Traditionally, ghost stories entailed being haunted by terrible hags who come back to avenge the wrong done to them. Thus, the fear of the demonic power of women is not necessarily of recent vintage.

The popularity of comic books (manga) among adults may appear to be a reversion to childhood, but the contents are hardly suitable for children, given the emphasis on “violence, sex and scatology.” Comic book sales amount to hundreds of millions of copies each year. One popular weekly comics magazine had a circulation of six million in 1991. Their readership includes a wide spectrum of Japanese society, from students and salaried businesspeople to housewives who wish to escape into fantasyland. In the 1970s Paul Theroux came across a comic book left behind by a reader on a train and was appalled when he picked it up and glanced at it. “The comic strips showed decapitations, cannibalism, people bristling with arrows like Saint Sebastian . . . and, in general, mayhem.” 54

Boys’ comic books deal with stories about sports, adventure, and science fiction. Girls’ comic books contain stories of love and romance. Adult comic books deal with stories about warriors, gamblers, and gigolos, but problems at the workplace and human relationships are treated in a humorous and satirical fashion. A pompous office boss may be lampooned, and the henpecked husband ridiculed. Perhaps the pressure to conform, to preserve harmony, and to abide by social proprieties has prompted people to seek outlets in fantasies of violence and sadism. Some critics have charged that comic books are ruining  the minds of Japanese youth, but such cautions have had no effect on their ever-growing popularity. Some enterprising journalists have started a magazine in English, excerpting and translating select strips with the ostensible excuse as a means to help the American reader learn the Japanese language.

Japan is famous for being a place where the younger generation seems to constantly invent a new craze that captivates the youth of Japan, followed closely a few weeks later by the children in the rest of the world. The “Hello Kitty” phenomenon is familiar to the parents of young girls everywhere. The cartoon cat is emblazoned on virtually every conceivable piece of young girls’ paraphernalia: lunch boxes, clothes, notepaper, erasers, sunglasses, shoes, jewelry, stuffed toys, bed linen, curtains, watches, and even food. Styled in imitation of the ubiquitous “waving cat” good-luck symbol that graces every retail store, the cartoon logo is a multibillion dollar industry. A similar fad for young boys was the Mighty Morphin Power Rangers cartoon series, which also became a world industry in plastic toy action figures and Halloween costumes.




【1】[美浓部达吉_百度百科](https://baike.baidu.com/item/美浓部达吉)
【2】[日本历史年表_百度百科](https://baike.baidu.com/item/日本历史年表/20423951)


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
