

function updateBackgroundImage(event) {
  const image_root = document.getElementById('image_root_dir').value;

  const h = document.getElementById('select_hair').value;
  const b = document.getElementById('select_brow').value;
  const e = document.getElementById('select_eye').value;
  const n = document.getElementById('select_nose').value;
  const m = document.getElementById('select_mouth').value;
  const f = document.getElementById('select_face').value;
  console.log(h);
  console.log(b);
  console.log(e);
  console.log(n);
  console.log(m);
  console.log(f);

  let background = document.querySelector('#example');

  background.style.backgroundImage = [
    "url('"+image_root+"/hair/hair__"+h+".png')",
    "url('"+image_root+"/brow/brow__"+b+".png')",
    "url('"+image_root+"/eye/eye__"+e+".png')",
    "url('"+image_root+"/nose/nose__"+n+".png')",
    "url('"+image_root+"/mouth/mouth__"+m+".png')",
    "url('"+image_root+"/face/face__"+f+".png')"
  ];
}

const ui_sh = document.getElementById('select_hair');
ui_sh.addEventListener('change', updateBackgroundImage);

const ui_sb = document.getElementById('select_brow');
ui_sb.addEventListener('change', updateBackgroundImage);

const ui_se = document.getElementById('select_eye');
ui_se.addEventListener('change', updateBackgroundImage);

const ui_sn = document.getElementById('select_nose');
ui_sn.addEventListener('change', updateBackgroundImage);

const ui_sm = document.getElementById('select_mouth');
ui_sm.addEventListener('change', updateBackgroundImage);

const ui_sf = document.getElementById('select_face');
ui_sf.addEventListener('change', updateBackgroundImage);
